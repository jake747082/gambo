module GamePlatforms
  autoload :Base, 'game_platforms/base'
  autoload :Histories, 'game_platforms/histories'
  autoload :User, 'game_platforms/user'
  autoload :Games, 'game_platforms/games'
end

require "#{File.dirname(__FILE__)}/game_platforms/platforms/ag.rb"
Dir.glob("#{File.dirname(__FILE__)}/game_platforms/platforms/*.rb").each { |file| require file }