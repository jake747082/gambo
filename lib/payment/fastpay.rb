module Payment
  class Fastpay

    def initialize(settings)
      @url        = settings['url']
      @partner_id = settings['partner_id']
      @pkey       = settings['pkey']
      @return_url = settings['return_url']
      @notice_url = settings['notice_url']
    end

    def pay(withdraw)
      params = {
        serialID: withdraw.order_id,
        submitTime: Time.zone.now.strftime("%Y%m%d%H%M%S"),
        orderID: withdraw.order_id,
        orderAmount: (withdraw.credit_actual * 100).to_i, # 分
        payType: 1,
        returnUrl: @return_url,
        noticeUrl: @notice_url,
        partnerID: @partner_id,
        remark: 'sf',
      }
      params.merge(signMsg: sign_msg(params), url: @url)
    end

    def return_feedback(params)
      return false unless vaild_sign_msg(params)
      params
    end

    def notice_feedback(withdraw, params)
      return false unless vaild_sign_msg(params)
      return false unless (withdraw[:credit_actual] * 100).to_i == params['orderAmount'].to_i
      status = params['stateCode'] == 'SUCCESS' ? :transferred : :failed
      begin
        ActiveRecord::Base.transaction do
          # 更新訂單
          raise WithdrawUpdateError unless withdraw.update!(status: status, deposit_at: params['acquiringTime'], order_info: params.to_json)
          # 會員加值
          if status == :transferred
            raise UserUpdateError unless User::CreditControl.new(withdraw.user, withdraw[:credit_actual]).deposit!
          end
        end
        'SUCCESS'
      rescue => e
        withdraw.update!(status: :waiting_to_deposit)
        ExceptionNotifier.notify_exception(e)
        false
      end
    end

    private

    def sign_msg(args = {})
      params = args.map do |field, value|
        "#{field}=#{value}"
      end.join('&')
      Digest::MD5.hexdigest("#{params}&pkey=#{@pkey}")
    end
    
    def vaild_sign_msg(params)
      response_sign_msg = params['signMsg']
      params = {
        orderID: params['orderID'],
        stateCode: params['stateCode'],
        orderAmount: params['orderAmount'],
        acquiringTime: params['acquiringTime'],
        orderNo: params['orderNo'],
        partnerID: params['partnerID'],
        remark: params['remark'],
      }
      sign_msg(params) == response_sign_msg
    end
    
  end
end