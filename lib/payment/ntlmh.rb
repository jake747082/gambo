module Payment
  class Ntlmh

    def initialize(settings)
      @url        = settings['url']
      @partner = settings['partner']
      @key       = settings['key']
      @callbackurl = settings['callbackurl']
      @hrefbackurl = settings['hrefbackurl']
    end

    def pay(withdraw)
      sign_params = {
        partner: @partner,
        banktype: withdraw.bank_code,
        paymoney: withdraw.credit_actual.to_f.round(2), #人民幣, 小數後兩位
        ordernumber: withdraw.order_id,
        callbackurl: @callbackurl
      }
      other_params = {
        hrefbackurl: @hrefbackurl,
        attach: '',
        sign: generate_sign(sign_params)
      }
      
      params = sign_params.merge(other_params).map do |field, value|
        "#{field}=#{value}"
      end.join('&')

      URI.parse(@url+'?'+params)
    end

    def return_feedback(params)
      return false unless vaild_sign_msg(params)
      return false unless params['orderstatus'].to_i == 1
      {
        "orderID" => params['ordernumber'],
        "orderAmount" => params['paymoney']
      }
    end

    def notice_feedback(withdraw, params)
      return false if withdraw.nil?
      return false unless vaild_sign_msg(params)
      return false unless (withdraw[:credit_actual] * 100).to_i == (params['paymoney'].to_f * 100).to_i
      status = params['orderstatus'].to_i == 1 ? :transferred : :failed
      begin
        ActiveRecord::Base.transaction do
          # 更新訂單
          raise WithdrawUpdateError unless withdraw.update!(status: status, deposit_at: Time.now, order_info: params.to_json)
          # 會員加值
          if status == :transferred
            raise UserUpdateError unless User::CreditControl.new(withdraw.user, withdraw[:credit_actual]).deposit!
          end
        end
        'ok'
      rescue => e
        withdraw.update!(status: :waiting_to_deposit)
        ExceptionNotifier.notify_exception(e)
        false
      end
    end

    private

    def generate_sign(args = {})
      params = args.map do |field, value|
        "#{field}=#{value}"
      end.join('&')
      Digest::MD5.hexdigest("#{params}#{@key}")
    end

    def vaild_sign_msg(params)
      response_sign_msg = params['sign']
      params = {
        partner: params['partner'],
        ordernumber: params['ordernumber'],
        orderstatus: params['orderstatus'],
        paymoney: params['paymoney']
      }
      p params
      p generate_sign(params)
      generate_sign(params) == response_sign_msg
    end
  end
end