WithdrawUpdateError = Class.new(StandardError)
UserUpdateError = Class.new(StandardError)
module Payment
  class Pay
    attr_accessor :payment

    def initialize(pay_method)
      @payment = "Payment::#{pay_method.camelize}".constantize.new(Setting.pay[pay_method])
    end

    def pay(withdraw)
      @payment.pay(withdraw)
    end
  end
end