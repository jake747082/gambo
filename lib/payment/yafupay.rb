module Payment
  class Yafupay

    def initialize(settings)
      @url        = settings['url']
      @cust_id    = settings['cust_id']
      @key        = settings['key']
      @back_url   = settings['back_url']
      @front_url  = settings['front_url']
    end

    def pay(withdraw)
      url_query = 'pay.ac'
      params = {
        backUrl: @back_url,
        bankCode: withdraw.bank_code,
        # buyPhome: '',
        # buyIp: '',
        consumerNo: @cust_id,
        frontUrl: @front_url,
        goodsName: 'Points',
        merOrderNo: withdraw.order_id,
        # merRemark: '',
        payType: withdraw.pay_method,
        # shopName: '',
        transAmt: withdraw.credit_actual.to_f.round(2), # 元
        version: '3.0',
      }
      params.merge(sign: sign_msg(params), url: @url + url_query)
    end

    def return_feedback(params)
      # return false unless vaild_sign_msg(params)
      # params
      true
    end

    def notice_feedback(withdraw, params)
      return false unless vaild_sign_msg(params)
      return false unless withdraw[:credit_actual].to_i == params['transAmt'].to_i #元
      return false if withdraw.transferred?

      # 0：处理中 ; 1：成功 ; 2：失败 ; 3:已退款 ; 4:可疑
      status = if params['orderStatus'] == '1' 
        :transferred
      elsif params['orderStatus'] == '2' 
        :failed
      else
        # @todo 
        # 3:已退款 4:可疑
      end

      begin
        ActiveRecord::Base.transaction do
          # 更新訂單
          raise WithdrawUpdateError unless withdraw.update!(status: status, order_info: params.to_json)
          # 會員加值
          raise UserUpdateError unless User::CreditControl.new(withdraw.user, withdraw[:credit_actual]).deposit!
        end
        'SUCCESS'
      rescue => e
        withdraw.update!(status: :waiting_to_deposit)
        ExceptionNotifier.notify_exception(e)
        false
      end
    end

    private

    def sign_msg(args = {})
      params = args.map do |field, value|
        "#{field}=#{value}" if value.present?
      end.compact.join('&')
      Digest::MD5.hexdigest("#{params}&key=#{@key}")
    end
    
    def vaild_sign_msg(params)
      response_sign_msg = params['sign']
      params = {
        consumerNo: params['consumerNo'],
        merOrderNo: params['merOrderNo'],
        orderNo: params['orderNo'],
        orderStatus: params['orderStatus'],
        payType: params['payType'],
        transAmt: params['transAmt'],
        version: params['version'],
      }
      sign_msg(params).upcase == response_sign_msg
    end
    
  end
end