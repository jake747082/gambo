class Game::HistoryCalculator
  attr_accessor :machine, :history, :total_base_credit, :total_spent_credit

  def initialize(machine, player, total_base_credit, free_spin_mode: false)
    @player = player
    self.machine = machine
    self.history = player.shareholder
    self.total_base_credit = total_base_credit
    self.total_spent_credit = if free_spin_mode then 0 else total_base_credit end
    @downto_cycle_size = machine.class::CYCLE_SIZES.reverse
  end

  # 此局是使用者贏或機器贏?
  def user_win?
    return max_response_credit > total_spent_credit
  end

  # 亂數產生使用者贏的金額 (使用者最大贏的金額)
  def max_response_credit
    # HACK: 至多 3 次嘗試、避免 max_can_dispatched_credit 太小找不到
    # 做法不好，若常常取到太小值會效能很差
    (1..3).each do
      # a. 最大吐錢的倍數亂數
      random_win_multiple = rand(1..machine["max_mutiple_#{current_lose_cycle}"]).to_f.round(2)
      temp_credit_result = (total_base_credit * random_win_multiple).round(2)
      # b. 如果最後金額小於遊戲記錄可分配最大值則 pass
      if max_can_dispatched_credit >= temp_credit_result
        return (temp_credit_result + total_spent_credit)
      end
    end

    # 如果一直找不到，那也不要回傳 0 直接套用輸錢規則
    rand_lose_money_credit
  end

  # 更新user記錄
  def update_player_history!(user_win_money)
    # user_win_money | rewoard_amount | total_spent_credit
    #              0 =   1 - 1
    #              1 =   2 - 1
    #             99 = 100 - 1
    #             -1 =   0 - 1
    # user_win_money = (reward_amount - total_spent_credit)
    User.update_counters @player, play_total: user_win_money
    return save_and_increase_bet_count_for_history! if user_win_money == 0

    unless ignore_histories
      if user_win_money > 0
        deduct_from_dispaching_credit!(user_win_money)
      elsif user_win_money < 0
        increase_to_preparing_credit!(user_win_money.abs)
      end
    end

    save_and_increase_bet_count_for_history!
  end

  # 測試線不計算利潤(股東username = 'apitest1') - 暫時拿掉
  def ignore_histories
    # @is_user && @player.agent.root.username == 'apitest1'
    false
  end

  # 遞增 total_bet_count
  def save_and_increase_bet_count_for_history!
    history['total_bet_count'] += 1 unless ignore_histories
    history.save!
  end

  private

  # 使用者獲利狀況需要遞減 dispatching columns
  def deduct_from_dispaching_credit!(user_win_money)
    history['total_lose_amount'] += user_win_money
    deduct_credit = user_win_money
    # xl => lg => md => sm => xs
    machine.class::CYCLE_SIZES.reverse_each do |cycle_size|
      if history["dispatching_#{cycle_size}"] >= deduct_credit
        return history["dispatching_#{cycle_size}"] -= deduct_credit
      else
        deduct_credit -= history["dispatching_#{cycle_size}"]
        history["dispatching_#{cycle_size}"] = 0
      end
    end
  end

  # 莊家贏錢狀況需要遞增 preparing columns
  def increase_to_preparing_credit!(user_lose_money)
    history['total_win_amount'] += user_lose_money

    machine.class::CYCLE_SIZES.each do |cycle_size|
      history["preparing_#{cycle_size}"] += (user_lose_money * machine["lose_rate_#{cycle_size}"])

      # 若 preparing >= slot machine trigger dispatching 金額則開始啟用獲利 (把 preparing 加到 dispaching)
      if history["preparing_#{cycle_size}"] >= machine["lose_trigger_amount_#{cycle_size}"]
        history["dispatching_#{cycle_size}"] += history["preparing_#{cycle_size}"]
        history["preparing_#{cycle_size}"] = 0
      end
    end
  end

  # 亂數產生要輸的錢
  def rand_lose_money_credit
    (rand(0..total_spent_credit) * user_lose_speed_rate).round(2)
  end

  # 機器咬錢速率
  def slot_win_speed_rate
    machine.win_speed / 100.0
  end

  # 玩家輸錢速率
  def user_lose_speed_rate
    1 - slot_win_speed_rate
  end

  # 該贏多少錢 (從大到小依序往下找) 需控制金額
  def max_can_dispatched_credit
    @downto_cycle_size.map { |cycle_size| history["dispatching_#{cycle_size}"] }.sum
  end

  # 目前正在走的吐錢週期 (若相同則回應最小週期 xs)
  def current_lose_cycle
    @downto_cycle_size.max_by { |cycle_size| history["dispatching_#{cycle_size}"] }
  end
end

