# 計算每個階層獲利
module Game::ProfitableBetFormHandler
  extend ActiveSupport::Concern

  included do
    attr_accessor :own_parent_amount

    # bet_form 計算每個階層獲利
    def params_with_dispatched_credit_for_each_agent(player, default_bet_form_params, prepare_to_dispatched_credit)
      @own_parent_amount = prepare_to_dispatched_credit
      params = default_bet_form_params
      prev_agent = nil
      # 反向代理樹 (店家 => 總代 => 代理)
      player.agent.self_and_ancestors.reverse_each do |agent|
        # agent.lock!

        # 扣掉上一個代理
        casino_ratio = if prev_agent.present?
          agent.casino_ratio - prev_agent.casino_ratio
        else
          agent.casino_ratio
        end

        # 自己的 casino_ratio
        casino_ratio = (casino_ratio / 100.to_f).abs

        # 該 agent 分配到的 credit
        agent_win_amount = (((prepare_to_dispatched_credit * casino_ratio) * 100) / 100.to_f)

        # 欠上線的錢
        @own_parent_amount = @own_parent_amount - agent_win_amount

        # 預備寫入的資料
        params["#{agent.agent_level}_id"] = agent.id
        params["#{agent.agent_level}_win_amount"] = agent_win_amount
        params["#{agent.agent_level}_owe_parent"] = @own_parent_amount

        # 公司的 credit 需扣掉
        prev_agent = agent
      end

      params
    end
  end
  
end