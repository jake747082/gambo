require 'activerecord-import/base'
class Reports::AgentCommission
  ActiveRecord::Import.require_adapter('mysql2')
  
  def self.bet_limit
    20
  end

  # 佣金表規則 - 獲利規則
  # * 勿反著排：Admin代理佣金表顯示用
  def self.bet_level_limit
    {
      5 => {
        total: 1,
        users: 1
      },
      4 => {
        total: 5.1,
        users: 2
      },
      3 => {
        total: 50.1,
        users: 3
      },
      2 => {
        total: 80.1,
        users: 4
      },
      1 => {
        total: 120.1,
        users: 5
      }
    }
    # {
    #   5 => {
    #     total: 1,
    #     users: 5
    #   },
    #   4 => {
    #     total: 50001,
    #     users: 10
    #   },
    #   3 => {
    #     total: 500001,
    #     users: 30
    #   },
    #   2 => {
    #     total: 800001,
    #     users: 80
    #   },
    #   1 => {
    #     total: 1200001,
    #     users: 120
    #   }
    # }
  end

  # 佣金表規則 - 各類型佔比
  def self.game_rate
    {
      #BT
      1=> {
        5=> 25, 
        4=> 30,
        3=> 35,
        2=> 40,
        1=> 45
      },
      # kgame
      2=> {
        5=> 25, 
        4=> 30,
        3=> 35,
        2=> 40,
        1=> 45
      }
    }
  end

  def initialize(begin_date, end_date)
    @agent_commission = {}
    run_bet_forms(begin_date, end_date)
    run_withdraws(begin_date, end_date)
    begin
      ActiveRecord::Base.transaction do
        @agent_commission.each do |key, commission|
          # insert data
          date, agent_id, type_id = key.split("@")
          reports = AgentCommissionReport.find_or_initialize_by(date: date, agent_id: agent_id, type_cd: type_id)
          reports.assign_attributes(commission.merge(date: date, agent_id: agent_id, type_cd: type_id))
          reports.save
        end
      end
    rescue => e
      ExceptionNotifier.notify_exception(e)
      false
    end
  end

  # 平台分類
  def run_bet_forms(begin_date, end_date)
    # 格式說明:
    # agent_commission: 
    #   "{date}@{agent_id}@{type_id}":
    #      {count}
    #      {total}
    
    bet_forms = ThirdPartyBetForm.state(1).datetime_range("#{begin_date} 00:00", "#{end_date} 23:59:59")
    bet_forms.each do |bet_form|
      key = "#{bet_form.betting_at.to_date.to_s}@#{bet_form.agent_id}@#{type_id(bet_form.game_platform_id, bet_form.game_type_id, bet_form.from)}"
      # BT / KG
      if @agent_commission[key].nil?
        @agent_commission[key] = {count: 1, total: -bet_form.user_credit_diff}
      else
        @agent_commission[key][:count] += 1
        @agent_commission[key][:total] -= bet_form.user_credit_diff
      end
    end
    @agent_commission
  end

  # 行政費用
  def run_withdraws(begin_date, end_date)
    withdraws = Withdraw.list_administration.list_transferred.where("agent_id is NOT NULL").datetime_range("#{begin_date} 00:00", "#{end_date} 23:59:59")
    type = 99
    withdraws.each do |withdraw|
      key = "#{withdraw.created_at.to_date.to_s}@#{withdraw.agent_id}@#{type}"
      total = (withdraw.cash_type_cd == 0) ? -withdraw.credit_actual : withdraw.credit_actual
      if @agent_commission[key].nil?
        @agent_commission[key] = {count: 1, total: total}
      else
        @agent_commission[key][:count] += 1
        @agent_commission[key][:total] += total
      end
    end
    @agent_commission
  end

  private

  def type_id(game_platform_id, game_type_id, from)
    # 99 => 行政費用
    if from == "bt"
      1
    elsif from == "kgame"
      2
    end
  end
end