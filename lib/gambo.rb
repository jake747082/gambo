require 'gambo/version'
require 'active_support/all'
require 'active_record'
require 'public_activity'

module Gambo
  extend ActiveSupport::Autoload

  module Reform
    autoload :AtomicSave, 'gambo/reform/atomic_save'
  end

  eager_autoload do
    autoload :Platforms, 'gambo/platforms'
  end

  autoload :Channel, 'gambo/channel'
end

Gambo.eager_load!

module Game
  extend ActiveSupport::Autoload  
  autoload :ProfitableBetFormHandler, 'game/profitable_bet_form_handler'
  autoload :ClientTimeSync, 'game/client_time_sync'
end

require 'game_platforms'# game_platforms
require 'reports'       # reports

require 'payment/pay'
require 'payment/fastpay'
require 'payment/yafupay'
require 'payment/zsagepay'
require 'payment/dinpay'
require 'payment/ntlmh'

require 'pusher'
require 'game/history_calculator'
require 'gambo/app'

if defined?(Rails)
  require 'gambo/railtie'
  require 'gambo/engine'
end