class AgentCommissionLog < ActiveRecord::Base
  belongs_to :agent

  default_scope { order(id: :desc) }

  scope :search, -> (query) {
    if query.present?
      joins(:agent).where("#{self.table_name}.id LIKE ? OR agents.nickname LIKE ? OR agents.username LIKE ?", "%#{query}%", "%#{query}%", "%#{query}%")
    else
      all
    end
  }
end