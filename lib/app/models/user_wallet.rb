class UserWallet < ActiveRecord::Base
  belongs_to :user
  belongs_to :currency

  shared do
    # 錢包廠商 1: ACE, 2: QQ
    as_enum :type, ACE: 1, QQ: 2
  end
end