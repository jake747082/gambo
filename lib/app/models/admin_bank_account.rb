class AdminBankAccount < ActiveRecord::Base
  validates :limit_update_count, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :title, :account, :sub_bank_name, presence: true


  include SensitiveFilter
  scope :list_deposit, -> { where(open: true, deposit: true) }

  def self.bank_options
    ['CMBC', 'MSBC', 'PCBC', 'CP', 'COMM', 'BKCH', 'ABOC']
  end

  def human_bank_title
    I18n.t("bank_accounts.bank_title.#{bank_id}")
  end
end
