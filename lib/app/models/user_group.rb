class UserGroup < ActiveRecord::Base
  
  default_scope { order(id: :desc)}

  has_many :users

  validates :name, presence: true, uniqueness: true
end