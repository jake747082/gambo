module SensitiveFilter
  def sensitive_filter(column, ratio: 0.5, direction: :right)
    data = public_send(column)
    length = (data.length * ratio).to_i

    if direction == :right
      data[0, length].ljust(data.length, '*')
    elsif direction == :left
      start = (length - data.length) - 1
      data[start, data.length].rjust(data.length, '*')
    end
  end
end