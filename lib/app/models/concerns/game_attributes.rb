module GameAttributes
  extend ActiveSupport::Concern

  CYCLE_SIZES = %w(xs sm md lg xl)

  included do
    scope :open, -> { where(open: true) }
  end

  def game_model
    @game_model ||= if game_model_name.present?
      game_model_name.constantize
    end
  end
  
  def human_game_name
    I18n.t("#{game_type_name}.#{game_model_name.demodulize.underscore}")
  end

  def tw_human_game_name
    I18n.t("#{game_type_name}.#{game_model_name.demodulize.underscore}", locale: 'zh-TW')
  end

  def cn_human_game_name
    I18n.t("#{game_type_name}.#{game_model_name.demodulize.underscore}", locale: 'zh-CN')
  end

  def en_human_game_name
    I18n.t("#{game_type_name}.#{game_model_name.demodulize.underscore}", locale: :en)
  end

  # ex: Slot::NinjaMission => ninjamission
  def game_name
    game_model_name.demodulize.downcase
  end

  # ex: LittleMary::GameTrain => little_mary
  def game_type_name
    game_model_name.deconstantize.underscore
  end

  # 用於提供給廠商的api用
  # ex: Slot::NinjaMission => NinjaMission
  def api_game_name
    game_model_name.demodulize
  end
  

end