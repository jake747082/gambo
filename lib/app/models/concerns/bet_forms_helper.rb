module BetFormsHelper
  extend ActiveSupport::Concern

  included do

    belongs_to :user
    belongs_to :cashout_agent, class_name: :Agent

    belongs_to :agent
    belongs_to :director, class_name: :Agent
    belongs_to :shareholder, class_name: :Agent

    default_scope { order(id: :desc) }

    scope :datetime_range, -> (begin_datetime, end_datetime) {
      begin_datetime = Time.zone.parse(begin_datetime.to_s)
      end_datetime   = Time.zone.parse(end_datetime.to_s)
      where('created_at >= ? AND created_at < ?', begin_datetime, end_datetime)
    }

    scope :date_range, -> (begin_date, end_date) {
      begin_date = Time.zone.parse(begin_date.to_s)
      end_date   = Time.zone.parse(end_date.to_s) + 1.day
      where('created_at >= ? AND created_at < ?', begin_date, end_date)
    }

    scope :daily_cashout, -> (agent_level) {
      group('DATE(created_at)')
      .select("id, DATE(created_at) as date, COUNT(id) as bet_count,
              SUM(bet_total_credit - reward_amount) as profit_amount,
              0 as jackpot_amount,
              SUM(bet_total_credit) as bet_amount,
              SUM(valid_amount) as valid_amount,
              SUM(#{agent_level}_win_amount) as win_amount,
              SUM(#{agent_level}_owe_parent) as owe_parent_amount")
    }

    scope :children_cashout, -> (agent_level_next) {
      group(agent_level_next.foreign_key)
      .select("id, COUNT(id) as bet_count,
              #{agent_level_next.foreign_key} as cashout_agent_id,
              SUM(bet_total_credit - reward_amount) as profit_amount,
              SUM(bet_total_credit) as bet_amount,
              SUM(valid_amount) as valid_amount,
              0 as jackpot_amount,
              SUM(#{agent_level_next}_win_amount) as win_amount,
              SUM(#{agent_level_next}_owe_parent) as owe_parent_amount")
    }

    scope :player_cashout, -> {
      group('user_id')
      .select("id, COUNT(id) as bet_count, user_id,
              SUM(bet_total_credit - reward_amount) as profit_amount,
              0 as jackpot_amount,
              SUM(valid_amount) as valid_amount,
              SUM(bet_total_credit) as bet_amount")
    }

    # 依日期統計玩家下注資料
    scope :player_cashout_by_date, -> {
      group('DATE(created_at)')
      .select("id, DATE(created_at) as date, COUNT(id) as bet_count, user_id,
              SUM(bet_total_credit - reward_amount) as profit_amount,
              SUM(reward_amount) as reward_amount,
              SUM(bet_total_credit) as bet_amount")
    }
  end
end
