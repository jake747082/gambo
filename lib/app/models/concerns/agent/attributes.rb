module Agent::Attributes
  extend ActiveSupport::Concern

  def credit_left
    credit_max - credit_used
  end

  def credit_undispatched
    credit_max - credit_dispatched
  end

  def title
    "#{human_agent_level} - #{nickname}"
  end
  alias_method :system_id, :title

  def human_agent_level
    self.class.human_enum_name(:agent_levels, agent_level)
  end
end
