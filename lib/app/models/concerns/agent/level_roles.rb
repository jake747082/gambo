require 'activerecord-mysql-index-hint'
module Agent::LevelRoles
  extend ActiveSupport::Concern

  included do
    shared do
      # Nested settings (agent level)
      acts_as_nested_set counter_cache: true

      # Levels ENUM
      as_enum :agent_level, shareholder: 0, director: 1, agent: 2

      # 0: 現金代理; 1: 買分代理
      as_enum :type, cash: 0, points: 1

      # 所有下線代理
      alias_method :all_agent, :descendants

      # 是否為最底層代理 (網咖 or 代理)
      alias_method :in_final_level?, :agent?

      # 是否為最頂層代理 (股東)
      alias_method :in_top_level?, :shareholder?

      alias_method :is_cash_type?, :cash?
      alias_method :is_points_type?, :points?

      TREE_INDEX = :index_agents_on_lft_and_rgt

      def self_and_descendants(*)
        super.use_index(TREE_INDEX, :PRIMARY)
      end

      def descendants(*)
        super.use_index(TREE_INDEX, :PRIMARY)
      end

      def self_and_ancestors(*)
        super.use_index(TREE_INDEX, :PRIMARY)
      end

      def ancestors(*)
        super.use_index(TREE_INDEX, :PRIMARY)
      end
    end

    # Scopes
    scope :list_shareholder, -> { where(agent_level_cd: 0) }
    scope :list_director, -> { where(agent_level_cd: 1) }
    scope :list_agent, -> { where(agent_level_cd: 2) }
  end

  module ClassMethods
    def self.agent_level_exists?(agent_level)
      @@agent_level_keys ||= agent_levels.keys.map(&:to_s)
      @@agent_level_keys.include?(agent_level.to_s)
    end
  end

  def agent_level_next
    @agent_level_next ||= self.class.agent_levels.hash.invert[agent_level_cd + 1]
  end

  def agent_level_prev
    @agent_level_prev ||= self.class.agent_levels.hash.invert[agent_level_cd - 1]
  end
end