module User::Attributes
  extend ActiveSupport::Concern

  def credit_left
    credit - credit_used
  end

  def credit_empty?
    credit_left == 0
  end

  def can_withdraw?
    credit_left >= 10
  end

  def title
    "#{username} (#{lobby_id})"
    # username
  end

  # 給後端辨識用
  def system_id
    lobby_id
  end
end
