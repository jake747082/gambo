module Machine::Attributes
  extend ActiveSupport::Concern

  # 給後端辨識用
  def system_id
    "[電腦] #{mac_address}"
  end

  def title
    "#{nickname} (#{mac_address})"
  end

  def credit_left
    (credit_max - credit_used).to_f
  end

  def cashout?
    credit_left == 0
  end

  def shutdown?
    !boot?
  end

  def can_deposit?
    boot? || credit_left == 0
  end

  def private_channel
    "private-machine-#{mac_address}"
  end
end