module Loggable
  extend ActiveSupport::Concern

  included do
    include ::PublicActivity::Common
  end

  # @todo create_log & create_bank_account_log 併為一
  def create_log(action, owner=nil, remote_ip='', params={})
    params = { changed: changed_attributes.keys, remote_ip: remote_ip}.merge(params)
    default_log_data = { owner: owner, params: params}
    default_log_data[:status] = params[:status] if params[:status]
    create_activity(action, default_log_data)
  end

  def create_bank_account_log(action, owner=nil, remote_ip='', params={}, recipient)
    params = { changed: changed_attributes.keys, remote_ip: remote_ip}.merge(params)
    default_log_data = { owner: owner, params: params, recipient: recipient}
    create_activity(action, default_log_data)
  end

  def action_logs
    PublicActivity::Activity.where(owner: self)
  end

  def change_logs
    PublicActivity::Activity.where(trackable: self)
  end
end
