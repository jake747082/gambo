module DeviseAuthableHelper
  extend ActiveSupport::Concern

  included do
    cattr_accessor :auth_column

    # Hack DEVISE
    def email_required?
      false
    end

    def email_changed?
      false
    end

    scope :search, -> (query) {
      if query.present?
        where("#{self.table_name}.#{auth_column} LIKE ? OR #{self.table_name}.nickname LIKE ?", "%#{query}%", "%#{query}%")
      else
        all
      end
    }
  end

  def find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login_value = conditions.delete(auth_column)
      where(conditions).where(auth_column => login_value).first
    else
      where(conditions).first
    end
  end
end