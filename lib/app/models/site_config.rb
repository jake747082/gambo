class SiteConfig < ActiveRecord::Base

  before_save :update_jp
  
  def update_and_notice_each_platform!(params)
    assign_attributes(params)
    changed_columns = changed
    if save
      SiteConfig::Controller.call(self, changed_columns)
      true
    else
      false
    end
  end

  def rate_percent
    self.jp_rate * 0.01
  end

  def update_jp
    self.jp_primary = (self.bet_total_credit - self.jp_reset) * self.rate_percent
  end

end