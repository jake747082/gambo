class UserGametoken < ActiveRecord::Base
  belongs_to :user
  belongs_to :game_platform
  belongs_to :platform_game
  before_create :assign_gametoken

  # 亂數產生 gametoken
  def assign_gametoken
    gametoken = loop do
      random_game_token = SecureRandom.hex(10)
      break random_game_token unless UserGametoken.exists?(token: random_game_token)
    end
    self.assign_attributes(token: gametoken, expired_at: Time.now + 1.day)
  end
end