class ThirdPartyBetForm < ActiveRecord::Base

  include BetFormsHelper

  belongs_to :game_platform

  default_scope { reorder("betting_at DESC").where(state: 1) }

  scope :state, -> (state_value) { where(state: state_value) }
  scope :east, -> { where(game_platform_id: 2) }
  scope :bbin, -> { where(game_platform_id: 3) }
  scope :cagayan, -> { where(game_platform_id: 6) }
  scope :bt, -> { where(game_platform_id: 7) }
  scope :kgame, -> { where(game_platform_id: 8) }
  scope :cq9, -> { where(game_platform_id: 9) }
  scope :sf, -> { where(game_platform_id: 10) }
  scope :bts, -> { where(game_platform_id: 11) }
  scope :wm, -> { where(game_platform_id: 13) }

  scope :video, -> { where(game_type_id: 1) }
  scope :slot, -> { where(game_type_id: 2) }
  scope :table, -> { where(game_type_id: 6) }
  scope :from_ag, -> { unscoped.order(betting_at: :desc).where(from: 'ag') }
  platform :agent do
    default_scope { Registry.current_agent.all_third_party_bet_forms if Registry.current_agent.present? }
  end

  scope :today, -> {
    where('betting_at >= ? AND betting_at < ?', Time.zone.now.at_beginning_of_day, Time.zone.now.at_end_of_day)
  }

  scope :week, -> {
    where('betting_at >= ? AND betting_at < ?', Time.zone.now.at_beginning_of_week.strftime("%Y-%m-%d 00:00:00"), Time.zone.now.at_end_of_week.strftime("%Y-%m-%d 23:59:59"))
  }

  scope :month, -> {
    where('betting_at >= ? AND betting_at < ?', Time.zone.now.at_beginning_of_month.strftime("%Y-%m-%d 00:00:00"), Time.zone.now.at_end_of_month.strftime("%Y-%m-%d  23:59:59"))
  }

  scope :total_betform_sum, -> {
    select("SUM(bet_total_credit) as bet_total_credit", "SUM(-user_credit_diff) as user_credit_diff").first
  }

  scope :category, -> (game_platform_name, category_id) {
    query = game_platform_name == 'bbin' ? {extra: category_id.to_s} : {game_type_id: category_id}
    where(query)
  }

  scope :game_type, ->(game_type_id) {
    where('game_type_id = ?', game_type_id)
  }

  # 記錄每日每個會員在各平台下注打碼量
  # @todo where state = 1
  # SELECT user_id, game_platform_id, SUM(valid_amount),COUNT(valid_amount), updated_at
  # FROM `third_party_bet_forms`
  # GROUP BY user_id, CAST(updated_at AS DATE), game_platform_id
  scope :daily_users_bet_credits, -> {
    select('user_id, game_platform_id, SUM(valid_amount) as total, COUNT(valid_amount) as count, DATE(betting_at) as date')
    .group('user_id, CAST(betting_at AS DATE), game_platform_id')
  }

  scope :datetime_range, -> (begin_datetime, end_datetime) {
    begin_datetime = Time.zone.parse(begin_datetime.to_s)
    end_datetime   = Time.zone.parse(end_datetime.to_s)
    where('betting_at >= ? AND betting_at < ?', begin_datetime, end_datetime)
  }

  scope :date_range, -> (begin_date, end_date) {
    begin_date = Time.zone.parse(begin_date.to_s)
    end_date   = Time.zone.parse(end_date.to_s)
    where('betting_at >= ? AND betting_at < ?', begin_date, end_date)
  }

  scope :histories_sum, -> {
    select("user_id, agent_id, director_id, shareholder_id, SUM(shareholder_owe_parent) as shareholder_owe_parent,
            SUM(shareholder_win_amount) as shareholder_win_amount,
            SUM(director_win_amount) as director_win_amount,
            SUM(agent_win_amount) as agent_win_amount,
            SUM(bet_total_credit) as bet_total_credit,
            SUM(reward_amount) as reward_amount,
            SUM(valid_amount) as valid_amount,
            SUM(user_credit_diff) as user_credit_diff,
            COUNT(*) as count")
  }

  scope :user_histories_sum, -> {
    select("SUM(bet_total_credit) as bet_total_credit,
            SUM(reward_amount) as reward_amount,
            SUM(valid_amount) as valid_amount,
            COUNT(*) as count")
  }

  scope :user_valid_amount_sum, -> {
    select("SUM(valid_amount) as valid_amount")
  }

  scope :effective_bet_users, -> (agent_id, begin_datetime, end_datetime){
    begin_datetime = Time.zone.parse(begin_datetime.to_s)
    end_datetime   = Time.zone.parse(end_datetime.to_s)
    select("user_id , SUM(valid_amount) as vaild_total_amount").where('agent_id = ?', agent_id).where('betting_at >= ? AND betting_at < ?', begin_datetime, end_datetime).having('SUM(valid_amount) > ?', Reports::AgentCommission.bet_limit).group('user_id')
  }
end
