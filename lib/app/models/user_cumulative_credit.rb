class UserCumulativeCredit < ActiveRecord::Base
  belongs_to :users
  belongs_to :currency

  shared do
    # @notice 請查看 models/UserDailyCreditLog 備註
    as_enum :type, deposit: 0, cashout: 1
    scope :cal_sum, -> {
      select('type_cd, SUM(count) as count, SUM(total) as total, SUM(extra_total) as extra_total, SUM(dc_total) as dc_total')
      .group('type_cd')
    }
    scope :cal_dc_sum, -> {
      select('type_cd, currency_id, SUM(count) as count, SUM(total) as total, SUM(extra_total) as extra_total, SUM(dc_total) as dc_total')
      .where('currency_id != ?', 0)
      .group('type_cd, currency_id')
    }
  end

end