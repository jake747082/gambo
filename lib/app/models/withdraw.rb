require 'aasm'
class Withdraw < ActiveRecord::Base
  include SensitiveFilter
  belongs_to :user
  belongs_to :agent
  belongs_to :currency
  before_create :assign_order_id

  store :bank_account_info, accessors: [ :bank_id, :province, :subbranch, :city, :account, :title, :mobile ], coder: JSON
  store :admin_bank_account_info, accessors: [ :admin_bank_id, :admin_sub_bank_name, :admin_title, :admin_account ], coder: JSON
  cattr_accessor :mininal_amount
  cattr_accessor :min_deposit_credit
  self.mininal_amount = 0
  self.min_deposit_credit = 10

  shared do
    as_enum :cash_type, deposit: 0, cashout: 1
    # admin: 後台發放; agent: 代理發放, comission: 反點, event_offer: 優惠金額
    as_enum :type, deposit: 0, duplicate_deposit: 2, wrong_deposit: 3, cashout: 4, illegal_credit: 5, 
    abandon_comission: 6, other: 7, apply_cashout: 8, apply_deposit: 9, google_play: 10, fastpay: 11,
     yafupay: 12, zsagepay: 13, dinpay: 14, admin: 15, event_offer: 16, comission: 1, agent: 17, ntlmh: 18,
      ppp: 19, ace_pay: 20, qq_pay: 21, extra_deposit: 22
  end

  def trade_no
    # @trade_no ||= id.to_s.rjust(8, '0')
    order_id
  end

  default_scope { order(id: :desc)}
  default_scope { where.not(currency_id: [1, 4, 20])}

  scope :list_google_play, -> { where(type_cd: 9) }
  scope :list_apply, -> { where(type_cd: [8, 9]) }
  scope :list_deposit, -> { where(cash_type_cd: 0) }
  scope :list_cashout, -> { where(cash_type_cd: 1) }
  scope :list_by_agent, -> { where(type_cd: 17) }
  scope :list_by_digital_currency, -> { where(type_cd: [20, 21]) }
  scope :list_transferred, -> { where(status: 2) }
  # 行政費用
  scope :list_administration, -> { where(type_cd: [16, 1]) }

  scope :skip_ppp_type, -> { where('type_cd != ?' , 19) }
  scope :withdraw_type, ->(type) {
    if type == 'cashout'
      where(cash_type_cd: 1)
    elsif type == 'deposit'
      where(cash_type_cd: 0)
    end
  }

  scope :search_users, -> (users_id) {
    if users_id.kind_of?(String)
      where(user_id: users_id)
    else
      where("user_id IN (?)", users_id)
    end
  }

  scope :list_by_type, -> (type_name){
    case type_name
    when "deposit"
      # where(cash_type_cd: 0).where.not(type_cd: [1, 16])
      where(cash_type_cd: 0, type_cd: 15)
    when "cashout"
      # where(cash_type_cd: 1).where.not(type_cd: [1, 16])
      where(cash_type_cd: 1, type_cd: 15)
    when "dc_piece_deposit"
      where(cash_type_cd: 0, type_cd: [20, 21])
    when "dc_piece_cashout"
      where(cash_type_cd: 1, type_cd: [20, 21])
    when "dc_deposit"
      where(cash_type_cd: 0, type_cd: [20, 21])
    when "dc_cashout"
      where(cash_type_cd: 1, type_cd: [20, 21])
    when "comission"
      where(type_cd: 1)
    when "event_offer"
      where(type_cd: 16)
    when "administration"
      where(type_cd: [1, 16])
    when "dc_deposit_and_cashout"
      where(type_cd: [20, 21])
    when "extra_deposit"
      where(cash_type_cd: 0)
    else
      all
    end
  }

  scope :search, -> (query) {
    if query.present?
      joins(:user).where("#{self.table_name}.order_id LIKE ? OR users.nickname LIKE ? OR users.username LIKE ?", "%#{query}%", "%#{query}%", "%#{query}%")
    else
      all
    end
  }

  platform :agent do
    default_scope { Registry.current_agent.all_withdraws if Registry.current_agent.present? }
  end

  scope :today, -> {
    where('created_at >= ? AND created_at < ?', Time.zone.now.at_beginning_of_day, Time.zone.now.at_end_of_day)
  }

  scope :week, -> {
    where('created_at >= ? AND created_at < ?', Time.zone.now.at_beginning_of_week.strftime("%Y-%m-%d 00:00:00"), Time.zone.now.at_end_of_week.strftime("%Y-%m-%d 23:59:59"))
  }

  scope :month, -> {
    where('created_at >= ? AND created_at < ?', Time.zone.now.at_beginning_of_month.strftime("%Y-%m-%d 00:00:00"), Time.zone.now.at_end_of_month.strftime("%Y-%m-%d  23:59:59"))
  }

  scope :total_withdraw_sum, -> {
    select("SUM(credit_actual) as total_credit_actual, SUM(credit) as total_credit, SUM(credit_diff) as total_extra_deposit, cash_type_cd, type_cd").group('cash_type_cd').group('type_cd')
  }

  # 儲值/提款成功的單(計算出每人每日金額出入總額)
  # SELECT user_id, cash_type_cd, SUM(credit_actual),COUNT(credit_actual), updated_at
  # FROM `withdraws`
  # WHERE status = 2
  # GROUP BY user_id, DATE(updated_at), cash_type_cd
  scope :user_daily_credit_flows, -> {
    select('user_id, cash_type_cd, SUM(credit_actual) as total, COUNT(credit_actual) as count, DATE(updated_at) as date')
    .where(status: 2)
    .group('user_id, DATE(updated_at), cash_type_cd')
  }

  # 每個不同存提款類別總計
  scope :cash_reports, ->  (begin_datetime, end_datetime){
    select('cash_type_cd, type_cd, currency_id, SUM(credit_actual) as credit_actual, SUM(amount) as amount, SUM(credit) as credit, SUM(credit_diff) as credit_diff')
    .where(updated_at: begin_datetime..end_datetime)
    .where(status: 2)
    .group('cash_type_cd, type_cd, currency_id')
  }

  # 依照類型進行總計，並以日為單位
  scope :daily_sum_by_type, -> (begin_date, end_date, agent){

    begin_date = Time.zone.parse(begin_date.to_s)
    end_date   = Time.zone.parse(end_date.to_s)

    select('DATE(updated_at) as date, cash_type_cd, type_cd, currency_id, SUM(credit_actual) as credit_actual, SUM(amount) as amount, SUM(credit) as credit, SUM(credit_diff) as credit_diff')
    .where('updated_at >= ? AND updated_at < ?', begin_date, end_date)
    .where("#{agent.agent_level}_id = ?", agent.id)
    .where(status: 2)
    .group('cash_type_cd, type_cd, currency_id, DATE(updated_at)')
  }

  # 依照類型進行下層代理總計，以代理做group
  scope :children_sum_by_type, -> (begin_date, end_date, agent){
    if agent.nil?
      joins("INNER JOIN agents ON agents.id = withdraws.shareholder_id")
      .select("withdraws.cash_type_cd, withdraws.type_cd, currency_id, SUM(withdraws.credit_actual) as credit_actual, SUM(withdraws.amount) as amount, SUM(withdraws.credit) as credit, SUM(credit_diff) as credit_diff, withdraws.shareholder_id as report_agent, agents.nickname as report_agent_nickname")
      .where(updated_at: begin_date..end_date)
      .where(status: 2)
      .group("withdraws.cash_type_cd, withdraws.type_cd, currency_id, withdraws.shareholder_id")
    else
      joins("INNER JOIN agents ON agents.id = withdraws.#{agent.agent_level_next.foreign_key}")
      .select("withdraws.cash_type_cd, withdraws.type_cd, currency_id, SUM(withdraws.credit_actual) as credit_actual, SUM(withdraws.amount) as amount, SUM(withdraws.credit) as credit, SUM(credit_diff) as credit_diff, withdraws.#{agent.agent_level_next.foreign_key} as report_agent, agents.nickname as report_agent_nickname")
      .where(updated_at: begin_date..end_date)
      .where("withdraws.#{agent.agent_level}_id = ?", agent.id)
      .where(status: 2)
      .group("withdraws.cash_type_cd, withdraws.type_cd, currency_id, withdraws.#{agent.agent_level_next.foreign_key}")
    end
  }

  # 依照類型進行下層玩家總計，以玩家做group
  scope :player_sum_by_type, -> (begin_date, end_date, agent){
    select('cash_type_cd, type_cd, currency_id, SUM(credit_actual) as credit_actual, SUM(amount) as amount, SUM(credit) as credit, SUM(credit_diff) as credit_diff, withdraws.user_id')
    .where(updated_at: begin_date..end_date)
    .where("#{agent.agent_level}_id = ?", agent.id)
    .where(status: 2)
    .group('cash_type_cd, type_cd, currency_id, user_id')
  }

  # 查詢單一代理，依照類型進行總計
  scope :agent_sum_by_type, -> (begin_date, end_date, agent){
    select("cash_type_cd, type_cd, currency_id, SUM(credit_actual) as credit_actual, SUM(amount) as amount, SUM(credit) as credit, SUM(credit_diff) as credit_diff")
    .where(updated_at: begin_date..end_date)
    .where("#{agent.agent_level}_id = ?", agent.id)
    .where(status: 2)
    .group("cash_type_cd, type_cd, currency_id")
  }

  # 查詢單一玩家，依照類型進行總計
  scope :user_sum_by_type, -> (begin_date, end_date, user){
    select("cash_type_cd, type_cd, currency_id, SUM(credit_actual) as credit_actual, SUM(amount) as amount, SUM(credit) as credit, SUM(credit_diff) as credit_diff")
    .where(updated_at: begin_date..end_date)
    .where(user_id: user.id)
    .where(status: 2)
    .group("cash_type_cd, type_cd, currency_id")
  }

  scope :datetime_range, -> (begin_datetime, end_datetime) {
    begin_datetime = Time.zone.parse(begin_datetime.to_s)
    end_datetime   = Time.zone.parse(end_datetime.to_s)
    where('created_at >= ? AND created_at < ?', begin_datetime, end_datetime)
  }

  # UserDailyCreditLog::Import使用(統計充提紀錄總計)
  scope :sum_credit_by_user, -> {
    select('user_id, cash_type_cd, type_cd, currency_id, COUNT(*) as count, SUM(credit_actual) as total, SUM(credit_diff) as extra_total, SUM(amount) as dc_total')
    .where(status: 2)
    .group('user_id, cash_type_cd, type_cd, currency_id')
  }

  concerning :StateMachine do
    included do
      include AASM
      # enum status: { pending: 0, proccessing: 1, transferred: 2, rejected: 3, removed: 4, cancelled: 5, failed: 6, waiting_to_deposit: 7,  transaction_processing: 8}
      enum status: { pending: 0, proccessing: 1, transferred: 2, removed: 4, cancelled: 5, failed: 6, waiting_to_deposit: 7,  transaction_processing: 8}
      aasm column: :status, enum: true do
        # 提出審請等待中
        state :pending, initial: true
        # 處理帳號審核中
        state :proccessing
        # 已處理完畢匯款完成
        state :transferred
        # 拒絕 (提款申請不退款)
        state :rejected
        # 管理員移除 (提款申請退款)
        state :removed
        # 使用者自行取消 (提款申請退款)
        state :cancelled
        # 交易失敗 (提款申請退款)
        state :failed
        # 付款成功但等待匯款(ex: 第三方支付成功，等待第三方回傳)
        state :waiting_to_deposit
        # 交易已處理但等待交易成功(ex: 進行虛擬幣轉帳，等待虛擬幣方回傳處理成功)
        state :transaction_processing

        event :proccess do
          transitions from: :pending, to: :proccessing
        end

        event :finish do
          transitions from: [:waiting_to_deposit, :transaction_processing, :proccessing], to: :transferred, after: :finish_withdraw
        end
        
        event :reject do
          transitions from: :proccessing, to: :rejected
        end
        
        event :remove do
          transitions from: [:proccessing, :pending], to: :removed do
            after do
              User::CreditControl.new(user, credit).refund! if cash_type == :cashout
            end
          end
        end
        
        event :cancel do
          transitions from: :pending, to: :cancelled do
            after do
              User::CreditControl.new(user, credit).refund! if cash_type == :cashout
            end
          end
        end

        event :fail do
          transitions from: [:transaction_processing, :pending], to: :failed do
            after do
              User::CreditControl.new(user, credit).refund! if cash_type == :cashout
            end
          end
        end

        event :waiting_to_deposit do
          transitions from: :pending, to: :waiting_to_deposit
        end

        event :transaction_processing do
          transitions from: :proccessing, to: :transaction_processing
        end

      end
    end
  end

  concerning :Application do
    included do
      platform :www do
        cattr_accessor :security_code
        validates :agree_terms, acceptance: true, presence: true, on: :create
        validates :security_code, presence: true, length: { is: 6 }, on: :create
        validate :valid_security_code, :valid_credit, on: :create
        validate :verify_deposit_credit
        before_create do
          [:bank_id, :province, :subbranch, :city, :account, :security_code].each do |column|
            bank_account = user.bank_account.present? ? user.bank_account.public_send(column) : ''
            public_send("#{column}=", bank_account)
          end
          if cash_type == :cashout
            public_send("mobile=", user.mobile)
            public_send("title=", user.name)
          end
        end
      end

      shared do
        validates_numericality_of :credit, greater_than_or_equal_to: mininal_amount

        before_create do
          user.lock!
          credit_control = User::CreditControl.new(user, credit_actual)
          if cash_type == :cashout
            # 會員申請提款
            credit_control.cashout!
            if transferred?
              credit_control.save_cashout_logs!(UserCumulativeCredit.types[:cashout], currency_id, amount) 
            end
          else
            # 會員列表直接儲值
            if transferred?
              credit_control.deposit!(UserCumulativeCredit.types[:deposit], currency_id, amount, credit_diff)
            end
          end

          # 將代理層id帶入
          user.agent.self_and_ancestors.lock!.each do |update_agent|
            self.send("#{update_agent.agent_level}_id=", update_agent.id)
          end

          if admin_bank_account_info.present?
            admin_bank_account = AdminBankAccount.find(admin_bank_account_info.to_i)
            [:bank_id, :sub_bank_name, :title, :account].each do |column|
              public_send("admin_#{column}=", admin_bank_account.public_send(column))
            end
          end
        end
      end
    end

    def finish_withdraw(params = {})
      # 結帳
      if cash_type == :cashout
        if params.empty?
          self.update(deposit_at: Time.now)
        else
          self.update(params.merge(deposit_at: Time.now))
        end
      end
      # 儲值
      if cash_type == :deposit
        credit_control = User::CreditControl.new(user, credit_actual)
        credit_control.deposit!(UserCumulativeCredit.types[:deposit], currency_id, amount, credit_diff)
      end
      
      # 更新 note & admin_note
      # self.update(note: params[:note], admin_note: params[:admin_note])
    end

    # 若是會員申請儲值, 不可低於最少額度
    def verify_deposit_credit
      if credit.to_f < min_deposit_credit
        errors.add(:credit, :greater_than, count: min_deposit_credit)
      end
    end

    def cashout?
      cash_type == :cashout
    end

    def valid_security_code
      # 驗證 security_code
      if (!user.bank_account || !BankAccount.valid_security_code(user.bank_account.security_code, security_code))# && type == :apply_deposit
        errors.add(:security_code, :invalid)
      end
    end

    def valid_credit
      if credit_actual.to_f > user.credit_left.to_f && cash_type_cd == 1
        errors.add(:credit_actual, :less_than_or_equal_to, count: user.credit_left.to_f)
      end
    end
  end

  class << self
    def all_deposit_types
      types.map { |type, _|
        # 暫時先將優惠金額及反點拿掉
        # if ['admin', 'event_offer', 'comission'].include?(type)
        if ['admin', 'extra_deposit'].include?(type)
          [I18n.t("activerecord.enums.#{model_name.i18n_key}.types.#{type}"), type]
        else
          nil
        end
      }.compact
    end

    def all_cashout_types
      types.map { |type, _|
        # 暫時先將優惠金額及反點拿掉
        # if ['admin', 'event_offer', 'comission'].include?(type)
        if ['admin'].include?(type)
          [I18n.t("activerecord.enums.#{model_name.i18n_key}.types.#{type}"), type]
        else
          nil
        end
      }.compact
    end

    def cash_type_attributes_for_select
      cash_types.map do |cash_type, _|
        [I18n.t("activerecord.enums.#{model_name.i18n_key}.cash_types.#{cash_type}"), cash_type]
      end
    end

    def yafupay_pay_methods
      # @todo
      # {bank: '0101', wechat: '0201', alipay: '0301', qq: '0501'}.map do |pay_method, value|
      {bank: '0101', wechat: '0201'}.map do |pay_method, value|
        [I18n.t("activerecord.enums.#{model_name.i18n_key}.pay_methods.#{pay_method}"), value]
      end
    end

    def yafupay_bank_codes
      # @todo
      ['ICBC', 'CCB', 'ABC', 'CMB'].map do |bank_code, _|
        [I18n.t("activerecord.enums.#{model_name.i18n_key}.bank_codes.#{bank_code}"), bank_code]
      end
    end

    def zsagepay_pay_methods
      # @todo
      # {bank: '0', wechat: '21', alipay: '30', qq: '31'}.map do |pay_method, value|
      {bank: '0', wechat: '21', qq: '31'}.map do |pay_method, value|
        [I18n.t("activerecord.enums.#{model_name.i18n_key}.pay_methods.#{pay_method}"), value]
      end
    end

    # def zsagepay_bank_card_types
    #   {personal: '00', debit_card: '01', business: '03'}.map do |bank_card_type, value|
    #     [I18n.t("activerecord.enums.#{model_name.i18n_key}.bank_card_types.#{bank_card_type}"), value]
    #   end
    # end

    def zsagepay_bank_codes
      # @todo
      ['BOC', 'ABC', 'ICBC', 'CCB', 'BCM', 'CMB', 'CEB', 'SPDB', 'BCCB', 'BJRCB', 'BOS', 'CIB', 'CITIC', 'CMBC', 'GDB', 'HXB', 'PAB', 'PSBC'].map do |bank_code, _|
        [I18n.t("activerecord.enums.#{model_name.i18n_key}.bank_codes.#{bank_code}"), bank_code]
      end
    end

    def ntlmh_bank_codes
      # ['ICBC', 'CCB', 'ABC', 'CMB', 'BOCO', 'BOC', 'CEB', 'CMBC', 'CIB', 'CTTIC', 'GDB', 'SPDB', 'PINGANBANK', 'HXB', 'NBCB', 'HKBEA', 'SHB', 'PSBS', 'NJCB', 'SRCB', 'CBHB', 'BOCB', 'BCCB', 'HSBANK', 'TCCB', 'SDB', 'EGBANK', 'kjpay', 'wypay', 'QUICKPAY', 'WEIXIN', 'ALIPAY', 'ALIPAYWAP', 'QQ', 'QQWAP'].map do |bank_code, _|
      ['wypay'].map do |bank_code, _|
        [I18n.t("activerecord.enums.#{model_name.i18n_key}.bank_codes.#{bank_code}"), bank_code]
      end
    end

  end

  def assign_order_id
    self.order_id = "#{Time.zone.now.strftime("%Y%m%d%H%M%S")}"
  end

  def human_status
    I18n.t("activerecord.enums.withdraw.status.#{status}")
  end

  def human_type
    I18n.t("activerecord.enums.withdraw.types.#{type}")
  end
end
