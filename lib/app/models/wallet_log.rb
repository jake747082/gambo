class WalletLog < ActiveRecord::Base
  shared do
    as_enum :type, deposit: 1, withdraw: 2
  end
end