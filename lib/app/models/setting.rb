require 'settingslogic'

class Setting < Settingslogic
  if defined?(Rails)
    source "./config/setting.yml"
    namespace Rails.env
  end

  def self.shop_site?
    @@shop_site ||= begin site.shop? rescue false end
  end

  def self.cash_site?
    @@cash_site ||= begin site.cash? rescue false end
  end

  def self.api_site?
    @@api_site ||= begin site.api? rescue false end
  end

  def self.allow_iframe?
    @@allow_iframe ||= begin site.allow_iframe? rescue false end
  end
end