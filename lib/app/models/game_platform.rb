class GamePlatform < ActiveRecord::Base
  IDS = ['east', 'ag_bbin', 'ag', 'ag_mg', 'cagayan', 'bt', 'kgame']
  has_many :user_game_platformships
  has_many :platform_games
  has_many :users, through: :user_game_platformships
  shared do
    as_enum :status, close: 0, open: 1, maintain: 2
  end
  scope :video_list, -> {
    where(name: ['east', 'bbin', 'ag'])
  }
  scope :cagayan, -> { where(id: 6).first }
  scope :kgame, -> { where(id: 8).first }
  scope :cq9, -> { where(id: 9).first }

  def human_platform_name
    I18n.t("game_platforms.#{name}")
  end

  def has_result?
    # ag不可查詢結果
    # name == 'ag' || name == 'mg' || name == 'bbin' || name == 'cagayan' || name == 'bt' || name == 'kgame' || name == 'cq9' || name == 'sf' ? false : true
    false
  end
end
