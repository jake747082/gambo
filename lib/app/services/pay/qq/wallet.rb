# 王牌QQ錢包
module Pay
  module Qq
    class Wallet
      def initialize
        @url = Setting.pay.qq.url
        @api_key = Setting.pay.qq.api_key
        @subsystem_num = Setting.pay.qq.subsystem_num.to_s
      end

      def get_balance(address, currency)
        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_coin,
          address: address,
        }
        data = send_post_request('/v1/api/balance', params)
        unless data || data['status'].nil?
          return false
        end
        if data['status'] == 200
          data['balance'].to_f
        else
          false
        end
      end

      def create_addr(currency)
        token_type = currency.token_type

        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_type
        }
        data = send_post_request('/v1/api/addresses', params)
        return false unless data
        return false if data['status'].nil?

        if data['status'] == 200
          data['address']
        else
          false
        end
      end

      def transfer(currency, to_address, amount)
        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_coin,
          to_address: to_address,
          amount: amount
        }
        hash = Digest::SHA3.hexdigest(params.values.join(''), 256)
        data = send_post_request('/v1/api/withdraw', params.merge(hash: hash))

        unless data || data['status'].nil?
          return false
        end
        {'status' => data['status'], 'id' => data['order_id'] || ''}
      end

      # 查詢充值紀錄
      def deposit_history(currency, address)
        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_coin
        }
        data = send_post_request('/v1/api/deposit_history', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data['history'] : false
      end

      # 查詢提幣紀錄
      def withdraw_history(currency, address)
        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_coin
        }
        data = send_post_request('/v1/api/withdraw_history', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data['history'] : false
      end

      # 取得根地址
      def get_root_address(currency)
        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_coin
        }
        data = send_post_request('/v1/api/root_address', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data['root_address'] : false
      end

      # 取得回補地址
      # Note: 回補地址則是只有OMNI跟ERC20類別代幣
      def get_refund_address(currency)
        params = {
          subsystem_num: @subsystem_num,
          currency: currency.token_coin
        }
        data = send_post_request('/v1/api/refund_address', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data['root_address'] : false
      end

      private

      def send_post_request(path, params)
        begin
          Rails.logger.info "url: #{@url + path}"
          uri = URI.parse(@url + path)
          http = Net::HTTP.new(uri.host, uri.port)
          if uri.port == 443
            http.use_ssl     = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          end
          request = Net::HTTP::Post.new(uri.request_uri)

          request.body = params.to_json
          request["Content-Type"] = "application/json"
          request["X-CHECKSUM"] = Digest::SHA2.new(256).hexdigest(params.to_json + @api_key)
          Rails.logger.info "params: #{params.inspect}"

          response = http.request(request)
          Rails.logger.info "response status: #{response.code}"
          Rails.logger.info "response: #{response.body}"
          if response.code == "200"
            JSON.parse(response.body)
          else
            false
          end
        rescue Exception => e
          Rails.logger.info "Pay::Qq::Wallet Exception: #{e.message}"
          false
        end
      end
    end
  end
end