module Shared::Pushable
  extend ActiveSupport::Concern

  MAX_PUSHER_CHANNEL_PER_REQUEST = 10

  if defined?(Rails) && Rails.env.test?
    def push_event!(channels, event_name, params={})
      true
    end

    def push_event(channels, event_name, params={})
      true
    end
  else
    def push_event!(channels, event_name, params={})
      channels = [channels] if channels.is_a?(String)
      channels.in_groups_of(MAX_PUSHER_CHANNEL_PER_REQUEST, false).each do |group_channel|
        begin
          Pusher.trigger_async(group_channel, event_name, params)
        rescue Pusher::Error
          retry
        end
      end
    end

    def push_event(channels, event_name, params={})
      begin
        Pusher.trigger(channels, event_name, params)
      rescue Pusher::Error
        retry
      end
    end
  end
end
