class Agent::TreeLock < BaseService
  include Shared::Pushable

  def initialize(agent)
    @agent = agent
  end

  # 鎖定該用戶和其下線
  def lock(&block)
    begin
      ActiveRecord::Base.transaction do
        lock_machines_for!(@agent, &block)
        lock_children_for!(@agent, &block)
      end
      true
    rescue => e
      false
    end
  end

  # 僅解鎖該用戶
  def unlock(&block)
    begin
      ActiveRecord::Base.transaction do
        if @agent.in_final_level?
          unlock_machine_for!(@agent, &block)
        end
        @agent.update!(lock: false)
        yield(@agent) if block_given?
      end
      true
    rescue => e
      false
    end
  end

  # 解鎖該用戶及其下線
  def unlock_all(&block)
    begin
      ActiveRecord::Base.transaction do
        # 解鎖整個tree
        unlock_machine_for!(@agent, &block)
        unlock_children_for!(@agent, &block)
      end
      true
    rescue => e
      false
    end
  end

  private

  def unlock_children_for!(parent, &block)
    parent.self_and_descendants.each do |agent_locked|
      agent_locked.lock!
      agent_locked.update!(lock: false)
      agent_locked.accounts.update_all(lock: false)
      yield(agent_locked) if block_given?
    end
  end

  def lock_children_for!(parent, &block)
    parent.self_and_descendants.each do |agent_locked|
      agent_locked.lock!
      agent_locked.update!(lock: true)
      agent_locked.accounts.update_all(lock: true)
      yield(agent_locked) if block_given?
    end
  end

  def unlock_machine_for!(agent, &block)
    # sub accounts
    agent.accounts.each do |agent_locked|
      agent_locked.lock!
      agent_locked.update!(lock: false)
      yield(agent_locked) if block_given?
    end
    # machines
    machines = agent.all_machines
    machine_chanels = machines.map do |machine_locked|
      machine_locked.lock!
      machine_locked.update!(lock: false)

      yield(machine_locked) if block_given?

      machine_locked.private_channel
    end
    push_event!(machine_chanels, 'unlock')
    # users
    users = agent.all_users
    users.map do |user_locked|
      user_locked.lock!
      user_locked.update!(lock: false)
      yield(user_locked) if block_given?
    end
    true
  end

  def lock_machines_for!(agent, &block)
    # sub accounts
    agent.accounts.each do |agent_locked|
      agent_locked.lock!
      agent_locked.update!(lock: true)
      yield(agent_locked) if block_given?
    end
    # machines
    machines = agent.all_machines
    machine_chanels = machines.map do |machine_locked|
      machine_locked.lock!
      machine_locked.update!(lock: true, boot: false)

      yield(machine_locked) if block_given?

      machine_locked.private_channel
    end
    push_event!(machine_chanels, 'lock')
    # users
    users = agent.all_users
    users.map do |user_locked|
      user_locked.lock!
      user_locked.update!(lock: true)
      yield(user_locked) if block_given?
    end
    true
  end
end