class Agent::Apply < BaseService

  def initialize(agent)
    @agent = agent
  end

  # 重新設定推薦碼
  def apply!
    begin
      Agent.transaction do
        @agent.lock!
        @agent.update!(lock: 0, reviewing: 0)
        yield(@agent) if block_given?
      end
      true
    rescue
      false
    end
  end
end