class Agent::Cashout < BaseService

  attr_reader :cashout_credit

  def call(agent)
    begin
      Agent.transaction do
        agent.lock!
        @cashout_credit = agent.credit_used
        agent.update!(credit_used: 0)
        yield(agent, cashout_credit) if block_given?
      end
      true
    rescue
      false
    end
  end
end