class Agent::ControlUserPermission < BaseService

  def initialize(agent)
    @agent = agent
  end

  def open!(&block)
    begin
      ActiveRecord::Base.transaction do
        @agent.update!(user_permission: true)
        @agent.accounts.update_all(machine_write_permission: true)
        yield(@agent) if block_given?
      end
      true
    rescue => e
      false
    end
  end

  def close!(&block)
    begin
      ActiveRecord::Base.transaction do
        close_children_for!(@agent, &block)
      end
      true
    rescue => e
      false
    end
  end

  def open_all!(&block)
    begin
      ActiveRecord::Base.transaction do
        open_children_for!(@agent, &block)
      end
      true
    rescue => e
      false
    end
  end
  
  private

  def close_children_for!(parent, &block)
    parent.self_and_descendants.each do |agent_locked|
      agent_locked.lock!
      agent_locked.update!(user_permission: false)
      agent_locked.accounts.update_all(machine_write_permission: false)
      yield(agent_locked) if block_given?
    end
  end

  def open_children_for!(parent, &block)
    parent.self_and_descendants.each do |agent_locked|
      agent_locked.lock!
      agent_locked.update!(user_permission: true)
      agent_locked.accounts.update_all(machine_write_permission: true)
      yield(agent_locked) if block_given?
    end
  end
end