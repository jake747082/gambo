# 以股東為主，往下建立一整條代理線(新增總代理 & 代理)
class Agent::CreateDefaultLine < BaseService
  def initialize(target_agent)
    @target_agent = target_agent
  end

  def create!
    return false if @target_agent.agent?
    # 建立股東 -> 總代理 -> 代理
    default_agent = @target_agent.descendants.where(owner_id: @target_agent.id, agent_level_cd: 2).first
    if default_agent.nil?
      ActiveRecord::Base.transaction do
        # Create Director
        if @target_agent.shareholder?
          director = @target_agent.children.new
          form = AgentForm::Create.new(director)
          return false unless form.create(default_agent_params(@target_agent, 2))
        else
          director = @target_agent
        end
        # Create Agent
        agent = director.children.new
        form = AgentForm::Create.new(agent)
        if form.create(default_agent_params(@target_agent, 3))
          agent
        else
          p form.errors
          false
        end
      end
    else
      default_agent
    end
  end

  private

  def default_agent_params(target_agent, num)
    random_password = SecureRandom.hex(10)
    {
      username: "#{target_agent.username}#{num}_#{Time.zone.now.strftime('%Y%m%d%H%M')}",
      nickname: '_',
      type_cd: target_agent.type_cd,
      password: random_password,
      password_confirmation: random_password,
      credit_max: 0,
      casino_ratio: target_agent.casino_ratio.to_f,
      owner_id: target_agent.id
    }
  end
end