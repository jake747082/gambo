require 'action_view/helpers'

class SiteConfig::Controller < BaseService
  include Shared::Pushable
  include ActionView::Helpers::TextHelper

  SITE_UPDATE_EVENT = 'site_updated'

  def call(config, changed_columns)
    if changed_columns.include?('lobby_maintenance') || changed_columns.include?('lobby_marquee')
      push_event!(Gambo::Channel::LOBBY, SITE_UPDATE_EVENT, maintenance: config.lobby_maintenance?, marquee: config.lobby_marquee)
    end
    if changed_columns.include?('agent_maintenance') || changed_columns.include?('agent_marquee')
      push_event!(Gambo::Channel::AGENT, SITE_UPDATE_EVENT, maintenance: config.agent_maintenance?, marquee: auto_link(config.agent_marquee, html: { target: '_blank' }))
    end
    if changed_columns.include?('cash_maintenance') || changed_columns.include?('cash_marquee')
      push_event!(Gambo::Channel::CASH, SITE_UPDATE_EVENT, maintenance: config.cash_maintenance?, marquee: auto_link(config.cash_marquee, html: { target: '_blank' }))
    end
  end
end