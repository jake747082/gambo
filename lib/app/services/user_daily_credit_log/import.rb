require 'activerecord-import/base'
# 利用Withdraw及ThirdPartyBetForm計算出每日每人存取款及各平台打碼量＆總計
class UserDailyCreditLog::Import < BaseService
  ActiveRecord::Import.require_adapter('mysql2')
  def call
    begin
      ## 每日
      # 儲值/提款成功的單(計算出每人每日金額出入總額)
      # @withdraws = Withdraw.user_daily_credit_flows
      columns = [:count, :total, :dc_total]

      # 打碼量
      # @bet_forms = ThirdPartyBetForm.daily_users_bet_credits
      # UserDailyCreditLog.transaction do
      #   UserDailyCreditLog.import withdraw_logs, on_duplicate_key_update: columns, validate: true if withdraw_logs.present?
      #   UserDailyCreditLog.import bet_forms_logs, on_duplicate_key_update: columns, validate: true if bet_forms_logs.present?
      # end

      ## 累積
      # @sum_credit_logs = UserDailyCreditLog.sum_credit_by_user
      @withdraws = Withdraw.sum_credit_by_user
      UserCumulativeCredit.transaction do
        UserCumulativeCredit.import user_cumulative_credits, on_duplicate_key_update: columns, validate: true if user_cumulative_credits.present?
      end
      true
    rescue => e
      false
    end
  end

  def user_cumulative_credits
    credit_rows = []
    @withdraws.each do |withdraw|
      credit_rows << UserCumulativeCredit.new({
        user_id: withdraw.user_id,
        type_cd: withdraw.cash_type_cd,
        currency_id: withdraw.currency_id,
        total: withdraw.total,
        extra_total: withdraw.extra_total,
        dc_total: withdraw.dc_total,
        count: withdraw.count,
      })
    end
    credit_rows
  end

  def withdraw_logs
    logs = []
    @withdraws.each do |withdraw|
      logs << UserDailyCreditLog.new({
        user_id: withdraw.user_id,
        type_cd: withdraw.cash_type_cd,
        total: withdraw.total,
        count: withdraw.count,
        date: withdraw.date
      })
    end
    logs
  end

  def bet_forms_logs
    logs = []
    @bet_forms.each do |bet_form|
      logs << UserDailyCreditLog.new({
        user_id: bet_form.user_id,
        type_cd: bet_form.game_platform_id,
        total: bet_form.total,
        count: bet_form.count,
        date: bet_form.date
      })
    end
    logs
  end
end