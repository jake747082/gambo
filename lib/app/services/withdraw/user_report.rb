# 玩家充提報表
class Withdraw::UserReport < BaseService

  def call(cond)
    @user, @begin_datetime, @end_datetime = cond[:user], cond[:begin_date], cond[:end_date]
    @begin_datetime ||= "#{Date.today} 00:00:00"
    @end_datetime ||= "#{Date.today} 23:59:59"

    setup_self_report
  end

  private
  
  def setup_self_report
    return false if @user.nil?
    user_withdraws = Withdraw.user_sum_by_type(@begin_datetime, @end_datetime, @user)
    user_report = SumStatistics.new
    user_withdraws.each do |withdraw|
      format_report(user_report, withdraw)
    end
    user_report
  end

  def format_report(report, withdraw)
    return nil if withdraw.nil?
    report.user_id = @user.id
    if withdraw.cash_type == :deposit
      if withdraw.type == :ace_pay
        report.total_deposit += withdraw.credit_actual
        report.deposit += withdraw.credit
        report.extra_deposit += withdraw.credit_diff
        # 因可能幣別已被關閉，但仍有存提資料需顯示，故設定預設值
        report.dc_deposit[withdraw.currency_id] ||= {amount: 0, credit: 0}
        report.dc_deposit[withdraw.currency_id][:credit] += withdraw.credit
        report.dc_deposit[withdraw.currency_id][:amount] += withdraw.amount
      elsif withdraw.type == :admin
        report.total_deposit += withdraw.credit_actual
        report.deposit += withdraw.credit_actual
      elsif withdraw.type == :extra_deposit
        report.total_deposit += withdraw.credit
        report.extra_deposit += withdraw.credit
      end
    else
      report.total_cashout += withdraw.credit_actual
      if withdraw.type == :ace_pay
        report.dc_cashout[withdraw.currency_id] ||= {amount: 0, credit: 0}
        # 因可能幣別已被關閉，但仍有存提資料需顯示，故設定預設值
        report.dc_cashout[withdraw.currency_id][:credit] += withdraw.credit
        report.dc_cashout[withdraw.currency_id][:amount] += withdraw.amount
      elsif withdraw.type == :admin
        report.cashout += withdraw.credit_actual
      end
    end
    report
  end

  class SumStatistics
    attr_accessor :user_id, :deposit, :extra_deposit, :cashout, :dc_deposit, :dc_cashout, 
                  :total_deposit, :total_cashout, :total_extra_deposit, :currencies
                  # :dc_piece_deposit, :dc_piece_cashout

    def initialize
      @deposit = 0
      @extra_deposit = 0
      @cashout = 0

      @dc_deposit = {}
      @dc_cashout = {}

      @total_deposit = 0
      @total_cashout = 0
      @total_extra_deposit = 0
      
      currencies = Currency.all 
      @currencies = currencies.pluck(:id, :name).to_h
      currencies.each do |cur|
        # credit不含贈金
        @dc_deposit[cur.id] = {amount: 0, credit: 0} if cur.in_enable?
        @dc_cashout[cur.id] = {amount: 0, credit: 0} if cur.out_enable?
      end
    end
  end
end