require 'app/services/sms/base'
module Sms
  class China < Base

    def initialize
			@url = Setting.sms.china.url
			@status_url = Setting.sms.china.status_url
			@call_url = Setting.sms.china.call_url
			@userid = Setting.sms.china.userid
			@account = Setting.sms.china.account
			@password = Setting.sms.china.password
    end

    # 發送單筆訊息
    def send(mobile, content)
			params = {
				userid: @userid,
				account: @account,
        password: @password,
        mobile: mobile,
        content: content,
        sendTime: Time.zone.now.strftime("%Y-%m-%d %H:%M:%S"),
        action: 'send',
        extno: ''
      }
      xml = send_post_request(@url, params, 'xml')
      if xml.xpath("//returnstatus").text == "Sucess"
        {
          returnstatus: xml.xpath("//returnstatus").text,
          message: xml.xpath("//message").text,
          remainpoint: xml.xpath("//remainpoint").text,
          taskID: xml.xpath("//taskID").text,
          successCounts: xml.xpath("//successCounts").text,
        }
      else
        Rails.logger.info xml
        false
      end
    end

    # 余额及已发送量查询
    def overage
			params = {
				userid: @userid,
				account: @account,
        password: @password,
        action: 'overage',
      }
      xml = send_post_request(@url, params, 'xml')
      if xml.xpath("//returnstatus").text == "Sucess"
        {
          returnstatus: xml.xpath("//returnstatus").text,
          message: xml.xpath("//message").text,
          payinfo: xml.xpath("//payinfo").text,
          overage: xml.xpath("//overage").text,
          sendTotal: xml.xpath("//sendTotal").text,
        }
      else
        Rails.logger.info xml
        false
      end
    end

    # 非法关键词查询
    def checkkeyword(content)
			params = {
				userid: @userid,
				account: @account,
        password: @password,
        action: 'checkkeyword',
        content: content
      }
      xml = send_post_request(@url, params, 'xml')
      if xml.xpath("//returnstatus").text == "Success"
        { 
          message: xml.xpath("//message").text,
          checkCounts: xml.xpath("//checkCounts").text,
        }
      else
        Rails.logger.info xml
        false
      end
    end

    # 状态报告接口
    def status_query
			params = {
				userid: @userid,
				account: @account,
        password: @password,
        action: 'query',
      }
      xml = send_post_request(@status_url, params, 'xml')
      if xml.xpath("//returnsms").text == "" || xml.xpath("//error").text != ""
        false
      else
        query = {}
        xml.xpath("//statusbox").each_with_index do |statusbox, key|
          statusbox.elements.each do |box|
            query[key] ||= {}
            query[key][box.name] = box.text
          end
        end
        query
      end
    end

    # 上行接口
    def call_query
			params = {
				userid: @userid,
				account: @account,
        password: @password,
        action: 'query',
      }
      xml = send_post_request(@call_url, params, 'xml')
      if xml.xpath("//returnsms").text == "" || xml.xpath("//error").text != ""
        false
      else
        query = {}
        xml.xpath("//callbox").each_with_index do |callbox, key|
          callbox.elements.each do |box|
            query[key] ||= {}
            query[key][box.name] = box.text
          end
        end
        query
      end
    end
  end
end