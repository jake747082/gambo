require 'app/services/sms/base'
module Sms
  class Fivec < Base

    def initialize
			@url = Setting.sms.fivec.url
			@username = Setting.sms.fivec.username
			@password = Setting.sms.fivec.password
			@apikey = Setting.sms.fivec.apikey
    end

    # 發送單筆訊息
    def send(mobile, content)
			params = {
				username: @username,
				password_md5: Digest::MD5.hexdigest(@password),
        apikey: @apikey,
        mobile: mobile,
        content: URI.encode(content),
				encode: 'UTF-8',
      }
      send_url = request_url('', params)
			xml = send_request(send_url)
    end

  end
end
