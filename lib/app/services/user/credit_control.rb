class User::CreditControl
  def initialize(user, credit)
    @user = user
    @credit = credit
  end

  # type: 0 => 存款(包含人工存款, 虛擬幣存款..), 1: 提款
  # type: deposit: 0, cashout: 1
  def deposit!(type = 0, currency = 0, dc_amount = 0, extra_amount = 0)
    @user.increase_credit!(@credit)
    # 加上打碼量
    @user.update!(cashout_limit_at: Time.zone.now, cashout_limit: @user.cashout_limit + @credit.abs)
    # 新增user_cumulative_credits
    @cumulative_credit = UserCumulativeCredit.find_or_create_by(user_id: @user.id, type_cd: type, currency_id: currency)
    @cumulative_credit.lock!
    @cumulative_credit.update!(count: @cumulative_credit.count + 1, total: @cumulative_credit.total + @credit, dc_total: @cumulative_credit.dc_total + dc_amount, extra_total: @cumulative_credit.extra_total + extra_amount)
  end
  
  # 扣款
  def cashout!
    @user.use_credit!(@credit)
  end
  
  # 不扣款, 但做扣款統計
  # type :deposit: 0, cashout: 1
  def save_cashout_logs!(type = 1, currency = 0, dc_amount = 0)
    @user.lock!
    # 打碼量歸0
    @user.update!(cashout_limit: 0)
    # 新增user_cumulative_credits
    @cumulative_credit = UserCumulativeCredit.find_or_create_by(user_id: @user.id, type_cd: type, currency_id: currency)
    @cumulative_credit.lock!
    @cumulative_credit.update!(count: @cumulative_credit.count + 1, total: @cumulative_credit.total + @credit, dc_total: @cumulative_credit.dc_total + dc_amount)
  end

  # 退款
  def refund!
    @user.increase_credit!(@credit)
  end
  
  def transfer_in!
    @user.increase_credit!(@credit)
  end

  def transfer_out!
    @user.use_credit!(@credit)
  end
end