class User::GetWalletAddress
  def initialize(user, type, currency)
    @user = user
    @type = type
    @currency = currency
  end

  def get_addr
    in_currency = Currency.in_enable.find_by(name: @currency)
    if !UserWallet.types.include?(@type.to_sym) || in_currency.nil?
      return false
    end
    user = UserWallet.find_or_initialize_by(user: @user, type_cd: UserWallet.types[@type.to_sym], currency_id: in_currency.id)
    if user.address.nil? || user.address.empty?
      if @type == 'ACE'
        wallet = Pay::Ace::Wallet.new
      else @type == 'QQ'
        wallet = Pay::Qq::Wallet.new
      end
      addr = wallet.create_addr(in_currency)
      if addr
        user.address = addr
        user.save!
        addr
      else
        false
      end
    else
      user.address
    end
  end
end