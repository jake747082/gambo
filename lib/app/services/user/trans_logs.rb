# 儲值提款記錄
class User::TransLogs

  attr_reader :error
  attr_reader :total
  attr_reader :logs

  def initialize(user, cash_type)
    @user = user
    @cash_type = cash_type
  end

  def get(from_datetime, to_datetime, page, count)
    if page.to_i != 0 && count.to_i == 0
      @error = {count: ["invalid"]}
      return false
    end
    if page.to_i < 0 || count.to_i < 0
      @error = {count: ["invalid"]}
      return false
    end
    unless Withdraw.cash_types.include?(@cash_type)
      @error = {cash_type: ["invalid"]}
      return false
    end
    if page.to_i == 0
      @logs = @user.withdraws.datetime_range(from_datetime, to_datetime).where(type_cd: [15, 20, 22], cash_type_cd: @cash_type)
      @total = @logs.size
    else
      offset = (page.to_i - 1) * count.to_i
      @logs = @user.withdraws.datetime_range(from_datetime, to_datetime).where(type_cd: [15, 20, 22], cash_type_cd: @cash_type).limit(count.to_i).offset(offset)
      @total = @user.withdraws.datetime_range(from_datetime, to_datetime).where(type_cd: [15, 20, 22], cash_type_cd: @cash_type).size
    end
    true
  end
end