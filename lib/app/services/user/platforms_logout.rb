# 把會員從各平台登出
class User::PlatformsLogout
  def initialize(user)
    @user = user
  end

  def logout
    bt = GamePlatforms::Bt.new(@user)
    bt.logout
  end
end