# 發送會員驗證信
class User::SendSms

  # 886 台灣; 86 中國; ; 63: 菲律賓, 60: 馬來西亞; 852 香港; 84: 越南
  ENABLE_CODE = ['886', '86', '63', '60', '852', '84']
  
  def initialize(user)
    @user = user
  end

  def send_verification_code!
    content = "【王牌】Dear user, OTP: #{@user.verification_code}. OTP will be expire in 5 minutes."
    mobile = @user.mobile
    if mobile.start_with?('86') && Setting.sms.include?('china')
      # 中國
      mobile.slice! '86'
      Sms::China.new.send(mobile, content)
    else
      SmsMitake.send(mobile, content)
    end
  end

end