# 遊戲紀錄
class User::GameHistories

  attr_reader :error
  attr_reader :sum
  attr_reader :histories

  def initialize(user, game_platform, game_type)
    @user = user
    @game_platform = game_platform # ID
    @game_type = game_type # ID
  end

  def get(from_datetime, to_datetime, page, count)
    if page.to_i != 0 && count.to_i == 0
      @error = {count: ["invalid"]}
      return false
    end
    if page.to_i < 0 || count.to_i < 0
      @error = {count: ["invalid"]}
      return false
    end
    
    search_params = {}
    search_params.merge!({game_platform_id: @game_platform.to_i}) if @game_platform.to_i > 0
    search_params.merge!({game_type_id: @game_type.to_i}) if @game_type.to_i > 0

    if page.to_i == 0
      @histories = @user.third_party_bet_forms.datetime_range(from_datetime, to_datetime)
      @histories = @histories.where(search_params) unless search_params.empty?
      @sum = @histories.user_histories_sum.first
    else
      offset = (page.to_i - 1) * count.to_i
      @histories = @user.third_party_bet_forms.datetime_range(from_datetime, to_datetime).limit(count.to_i).offset(offset)
      @sum = @user.third_party_bet_forms.datetime_range(from_datetime, to_datetime)
      unless search_params.empty?
        @histories = @histories.where(search_params) 
        @sum = @sum.where(search_params)
      end
      @sum = @sum.user_histories_sum.first
    end
    true
  end
end