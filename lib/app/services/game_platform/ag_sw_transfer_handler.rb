# AG單錢包 - 視訊遊戲
class GamePlatforms::AgSwTransferHandler
  include Game::ProfitableBetFormHandler
  attr_reader :error, :user

  GAME_PLATFORM_ID = 12

  def initialize(params)
    @params = params
    username = params[:playname].split(Setting.game_platform.ag_sw.productid+Setting.game_platform.ag_sw.prefix)[1]
    user_id = User.lobby_id_to_id(username)
    @user = User.find_by_id(user_id) if user_id > 0
  end

  # transactionType : BET
  def bet!
    unless @params[:transactionType] == 'BET'
      @error = {'404'=> 'INVALID_TRANSACTION'}
      return false
    end
    return false unless valid_params?

    # 驗證是否有相同transactionID存在
    @log = AgSwTransferLog.find_by_transactionID(@params[:transactionID])
    unless @log.nil?
      # 檢查是否為同一筆資料
      if @log.playname == @params[:playname] && @log.sessionToken == @params[:sessionToken] && @log.transactionType == @params[:transactionType]
        # 已處理過, 直接回傳
        return true
      else
        @error = {'400'=> 'INVALID_DATA'}
        return false
      end
    end

    return false unless valid_user? # 在檢查transactionID是否存在之後
    return false unless valid_session_token?
    call_bet! 
  end

  # Win / Lose / Draw
  def end!
    return false unless valid_params?
    return false unless valid_user?

    @bet_params = @params.slice(:sessionToken, :currency, :netAmount, :validBetAmount, :playname, :agentCode, :settletime, :transactionID, :billNo, :gametype, :gameCode, :transactionType, :transactionCode, :ticketStatus, :gameResult, :finish, :playtype)

    # transactionID 通一格式
    transaction_ids = @bet_params[:transactionID].kind_of?(Array) ? @bet_params[:transactionID].map(&:to_s) : [@bet_params[:transactionID].to_s]

    # 驗證是否有相同transactionID存在
    @logs = if transaction_ids.length == 1
      AgSwTransferLog.where(transactionID: transaction_ids.first)
    else
      AgSwTransferLog.where(transactionID: transaction_ids)
    end

    if @logs.nil? || @logs.length != transaction_ids.length
      @error = {'400'=> 'INVALID_DATA'}
      return false
    end
    # 檢查是否為同一筆資料(檢查第一筆)
    if @logs.first.playname == @bet_params[:playname] && @logs.first.sessionToken == @bet_params[:sessionToken] && @logs.first.transactionType == @bet_params[:transactionType] && @logs.first.finish == @bet_params[:finish] && @logs.first.billNo.split(',').include?(@bet_params[:billNo])
      # 已處理過, 直接回傳
      return true
    end
    return false unless valid_session_token?

    # valid amount
    if (@bet_params[:transactionType] == 'WIN' && @bet_params[:netAmount].to_f < 0) || 
      (@bet_params[:transactionType] == 'LOSE' && @bet_params[:netAmount].to_f > 0)
      @error = {'400'=> 'INVALID_DATA'}
      return false
    end

    call_finish!
  end

  def refund!
    return false unless valid_params?
    return false unless valid_user?

    @refund_params = @params.slice(:ticketStatus, :sessionToken, :currency, :value, :playname, :agentCode, :betTime, :transactionID, :platformType, :round, :gametype, :gameCode, :tableCode, :transactionType, :transactionCode, :playtype)
    # 驗證是否有相同transactionID存在
    @log = AgSwTransferLog.find_by(transactionID: @refund_params[:transactionID])
    if @log.nil?
      @error = {'400'=> 'INVALID_DATA'}
      return false
    end
    # 檢查是否為同一筆資料(檢查第一筆)
    if @log.playname == @refund_params[:playname] && @log.sessionToken == @refund_params[:sessionToken] && @log.transactionType == @refund_params[:transactionType] && @log.ticketStatus == @params[:ticketStatus] && @log.value == @params[:value]
      # 已處理過, 直接回傳
      return true
    end
    return false unless valid_session_token?
    
    # valid amount
    if @refund_params[:transactionType] == 'REFUND' && @refund_params[:value].to_f < 0
      @error = {'400'=> 'INVALID_DATA'}
      return false
    end

    call_refund!
  end

  def build_undone_bet_form!(records)
    return false unless valid_user?
    return false unless valid_session_token?
      # 驗證是否有相同transactionID存在並且已處理
    @log = AgSwTransferLog.find_by(transactionID: @params[:transactionID])
    if @log.nil?
      @error = {'400'=> 'INVALID_DATA'}
      return false
    else
      # 檢查是否為同一筆資料
      # 已處理過, 直接回傳
      return true if @log.betResponse == @params[:betResponse] && @log.playname == @params[:playname] && @log.sessionToken == @params[:sessionToken]
    end
    if @params[:betResponse] == 'OK'
      call_bet_response!(records)
    else
      @error = {'404'=> 'INVALID_TRANSACTION'}
      false
    end
  end

  private

  # 視訊遊戲
  # ===============
  # 下注確認請求（BET)
  # 下注詳細信息回傳
  # 延后转账 羸 Post Transfer for Win
  # 延后转账 输 Post Transfer for Lose
  # 延后转账 和 Post Transfer for Draw (百家乐, 炸金花, 鬥牛, 21 点和局: WIN / 龙虎和局: LOSE)
  # 延后转账 回滚额度 Post Transfer for Refund

  def call_bet!
    bet_params = @params.slice(:sessionToken, :currency, :value, :playname, :agentCode, :betTime, :transactionID, :platformType, :round, :gametype, :gameCode, :tableCode, :transactionType, :transactionCode, :deviceType, :playtype)
    begin
      ActiveRecord::Base.transaction do
        if AgSwTransferLog.create(bet_params)
          # 進行玩家餘額扣除
          service = User::CreditControl.new(@user, bet_params[:value].to_f)
          service.transfer_out!
          true
        else
          @error = {'500'=> 'ERROR'}
          false
        end
      end
    rescue Exception => e
      @error = {'500'=> 'ERROR'}
      false
    end
  end
  
  # 下注詳細信息回傳
  def call_bet_response!(records)
    bet_params = @params.slice(:betResponse, :sessionToken, :playname, :transactionID)
    begin
      ActiveRecord::Base.transaction do
        if @log.update({betResponse: bet_params[:betResponse]})
          # 若 billNo已存在，則update
          if records.size == 1
            undone_bet_form = AgSwUndoneBetForm.find_by_vendor_id(records.first['billNo'])
            unless undone_bet_form.nil?
              # 下注額度加總
              bet_amount = BigDecimal(undone_bet_form.bet_total_credit) + BigDecimal(@log.value)
              response = [JSON.parse(undone_bet_form.response)].push(@log.as_json.merge!(records.first))
              undone_bet_form.update!(bet_total_credit: bet_amount, response: response.to_json, product_id: undone_bet_form.product_id + ",#{@log.transactionID}")
              return true
            end
          end
          
          # 新增 undone_bet_form
          undone_bet_forms = []
          records.each do |record|
            bet_form_params = default_bet_form_params(@user, @log.as_json.merge!(record), 1)
            undone_bet_forms << AgSwUndoneBetForm.new(bet_form_params)
          end

          unless undone_bet_forms.empty?
            # insert data
            AgSwUndoneBetForm.import undone_bet_forms
          end
          true
        else
          @error = {'500'=> 'ERROR'}
          false
        end
      end
    rescue Exception => e
      @error = {'500'=> 'ERROR'}
      false
    end
  end

  # WIN / LOSE
  def call_finish!
    begin
      ActiveRecord::Base.transaction do
        # 檢查BET
        # ag_sw = GamePlatforms::AgSw.new
        # transaction_ids = @bet_params[:transactionID].kind_of?(Array) ? @bet_params[:transactionID] : [@bet_params[:transactionID]]
        # AG說: 建議是 您先返回Win / Lost Request後，再調用CheckTicketStatus 確認訂單狀態
        # transaction_ids.each do |transaction_id|
        #   billnos = ag_sw.check_ticket_status(transaction_id)
        #   # status = 1 (確認下注成功) || status = 6 (注单结果传送成功)
        #   unless billnos[@bet_params[:billNo]] == '1' || billnos[@bet_params[:billNo]] == '6'
        #     Rails.logger.info billnos
        #     @error = {'404'=> 'INVALID_TRANSACTION'}
        #     return false
        #   end
        # end
        @logs.each do |log|
          # INSERT log(若transactionID相同則update)
          billNos = log[:billNo].split(',').push(@bet_params[:billNo])
          log.update({
            billNo: billNos.join(','),
            finish: @bet_params[:finish],
            ticketStatus: @bet_params[:ticketStatus],
            gameResult: @bet_params[:gameResult],
            netAmount: @bet_params[:netAmount],
            validBetAmount: @bet_params[:validBetAmount],
            settletime: @bet_params[:settletime],
            transactionType: @bet_params[:transactionType],
          })
        end

        # 實際出入金額度
        amount = BigDecimal(@bet_params[:netAmount]) + BigDecimal(@bet_params[:validBetAmount])

        undone_bet_form = AgSwUndoneBetForm.find_by_vendor_id(@bet_params[:billNo])

        # 代表可能BET Response無處理，故需建立undone_bet_form
        if undone_bet_form.nil?
          # 覆蓋billNo與value
          bet_form_params = default_bet_form_params(@user, @logs.first.as_json.merge!({"billNo" => @bet_params[:billNo], "value" => @bet_params[:validBetAmount], "playtype" => @bet_params[:playtype]}), 1)
          undone_bet_form = AgSwUndoneBetForm.new(bet_form_params)
          undone_bet_form.save!
        end

        # 計算
        bet_params = undone_bet_form.attributes.except!("id").merge!(finish_bet_form_params(@bet_params))
        # 計算代理分成
        final_params = params_with_dispatched_credit_for_each_agent(@user, bet_params, - bet_params[:user_credit_diff].to_f)
            
        # INSERT bet_form
        ThirdPartyBetForm.create(final_params)
        # Remove Undone_bet_form
        undone_bet_form.destroy!
        # 玩家入金
        service = User::CreditControl.new(@user, amount)
        if amount > 0
          service.transfer_in!
        elsif amount < 0
          service.transfer_out!
        end
      end
      true
    rescue Exception => e
      Rails.logger.info e.message
      @error = {'500'=> 'ERROR'}
      false
    end
  end

  def call_refund!
    begin
      ActiveRecord::Base.transaction do
        @log.update({
          ticketStatus: @refund_params[:ticketStatus],
          value: @refund_params[:value],
          transactionType: @refund_params[:transactionType]
        })
        # 玩家入金
        service = User::CreditControl.new(@user, @refund_params[:value].to_f)
        service.transfer_in!
      end
      true
    rescue Exception => e
      Rails.logger.info e.message
      @error = {'500'=> 'ERROR'}
      false
    end
  end

  def valid_params?
    unless @params[:agentCode] == Setting.game_platform.ag_sw.productid
      @error = {'400'=> 'INVALID_DATA'}
      false
    end
    true
  end

  def valid_user?
    if @user.nil?
      @error = {'400'=> 'INVALID_DATA'}
      return false
    end
    # 額度是否足夠
    if @user.credit_left < @params[:value].to_f
      @error = {'409'=> 'INSUFFICIENT_FUNDS'}
      return false
    end
    if @user.lock?
      @error = {'409'=> 'INSUFFICIENT_CLEARED_FUNDS'}
      return false
    end
    true
  end

  # 驗證session_token
  def valid_session_token?
    @user_game_platformships = @user.user_game_platformships.find_by_session_id(@params[:sessionToken])
    if @user_game_platformships.nil?
      @error = {'404'=> 'INVALID_SESSION'}
      false
    else
      true
    end
  end

  private

  def default_bet_form_params(user, data, game_type_id)
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   data['value'],
      game_type_id:       game_type_id,

      order_number:       "AGSW_#{data['billNo']}",

      game_name_id:       data['gametype'],
      game_betting_kind:  data['playtype'], # 玩家下注的玩法
      game_betting_content: data['val'].nil? ? '' : data['val'],
      betting_at:         DateTime.strptime(data['betTime'], "%m/%d/%Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S"),
      vendor_id:          data['billNo'],
      
      round:              data['round'], # 平台游戏类型
      product_id:         data['transactionID'],
      stage:              data['gameCode'],
      table_id:           data['tableCode'],
      game_kind:          data['gametype'],
      currency:           data['currency'],
      response:           data.to_json,
      from:               'ag_sw',
    }
  end

  def finish_bet_form_params(data)
    result = case data[:transactionType]
    when 'WIN'
      'W'
    when 'LOSE'
      'L'
    else
      data[:transactionType]
    end

    # 有效投注
    valid_bet = if data[:gametype] == 'DT' && BigDecimal(data[:netAmount]).abs == BigDecimal(data[:validBetAmount])/2
      # 龍虎(輸贏金額為下注额的一半)
      BigDecimal(data[:netAmount]).abs
    elsif data[:gametype] == 'BAC' && BigDecimal(data[:netAmount]) == 0
      # 百家樂(輸贏金額為0)
      0
    else
      data[:validBetAmount]
    end

    {
      reward_amount:      BigDecimal(data[:netAmount]) + BigDecimal(data[:validBetAmount]),
      user_credit_diff:   data[:netAmount],
      end_at:             DateTime.strptime(data[:settletime], "%m/%d/%Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S"),
      valid_amount:       valid_bet,
      result:             result
    }
  end
end