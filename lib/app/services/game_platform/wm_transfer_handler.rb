
class GamePlatforms::WmTransferHandler

	User_prefix  ||=  Setting.game_platform.wm.user_prefix

	def initialize(params)
		@params = params
		if @params["user"].present?
			userId = @params["user"].gsub(User_prefix,"").to_i - Setting.beginning_lobby_id.to_i
			@user  = User.find_by(id: userId)
		end
	end

	def call_balance
		userBalance  = @user.credit_left
		responseDate = get_responseDate
		Rails.logger.info "Return user balance."
		reply        = {errorCode: 0, errorMessage: "Success.", result: {"user" => @params["user"] , "money" => userBalance, "responseDate" => responseDate} }
	end

	def exchange_point
		@user.lock!
		# 新增log
		log = WmTransferLog.new({
				account_name: @params["user"],
				money:        @params["money"],
				request_date: convert_timeZone_to_EST(@params["requestDate"]),
				dealid:       @params["dealid"],
				gtype:        @params["gtype"],
				bet_type:     @params["type"],
				betdetail:    @params["betdetail"],
				gameno:       @params["gameno"],
				code:         @params["code"],
				category:     @params["category"],
				result_status: "failed",
				user_id:      @user.id
		})
		log.save!

		begin
			ActiveRecord::Base.transaction do
				# 判斷加扣款
				if @params["code"] == "1" || @params["code"] == "3" || @params["code"] == "5"
					# 加點
					money        = @params["money"].to_f.abs
					dealid       = @params["dealid"]
					responseDate = get_responseDate
					service = User::CreditControl.new(@user, money)
					service.transfer_in!
					log.update(result_status: "success")
					Rails.logger.info "Points transfer in success."
					cash   = User.find_by(id: @user.id).credit_left
					reply  = {errorCode: 0, errorMessage: "Success.", result: {"money" => "+#{money}", "responseDate" => responseDate, "dealid" => dealid, "cash" => cash}}
				elsif @params["code"] == "2" || @params["code"] == "4"
					# 轉帳失敗判定？
					if @params["money"].to_f.abs > User.find_by(id: @user.id).credit_left
						Rails.logger.info "Points exchange failed."
						reply  = {errorCode: 10805, errorMessage: "Insufficient credit.", result: {} }
					else
						# 扣點
						money        = @params["money"].to_f.abs
						dealid       = @params["dealid"]
						responseDate = get_responseDate
						service = User::CreditControl.new(@user, money)
						service.transfer_out!
						log.update(result_status: "success")
						Rails.logger.info "Points transfer out success."
						cash   = User.find_by(id: @user.id).credit_left
						reply  = {errorCode: 0, errorMessage: "Success.", result: {"money" => "-#{money}", "responseDate" => responseDate, "dealid" => dealid, "cash" => cash}}
					end
				else
					Rails.logger.info "Points exchange failed."
					reply  = {errorCode: 102, errorMessage: "Exchange failed.", result: {} }
				end
			end 
		rescue => exception
			Rails.logger.info exception.message
			log.update(result_status: "failed")
			reply  = {errorCode: 101, errorMessage: exception.message, result: {} }
		end
	end

	def return_bet
		begin
			ActiveRecord::Base.transaction do
				# 新增log
				@user.lock!
				log = WmBetReturnLog.new({
					account_name: @params["user"],
					money:        @params["money"],
					request_date: convert_timeZone_to_EST(@params["requestDate"]),
					dealid:       @params["dealid"],
					bet_type:     @params["type"],
					betdetail:    @params["betdetail"],
					gameno:       @params["gameno"],
					code:         @params["code"],
					category:     @params["category"],
					user_id:      @user.id
				})
				log.save!
				# 判斷加扣款
				if @params["code"] == "2"
					# 加點?
					money        = @params["money"].to_f.abs
					dealid       = @params["dealid"]
					responseDate = get_responseDate
					service = User::CreditControl.new(@user, money)
					service.transfer_in!
					Rails.logger.info "Points transfer return in success."
					cash   = User.find_by(id: @user.id).credit_left
					reply  = {errorCode: 0, errorMessage: "Success.", result: {"money" => "-#{money}", "responseDate" => responseDate, "dealid" => dealid, "cash" => cash}}
				elsif @params["code"] == "1"                    
					# 扣點?
					money        = @params["money"].to_f.abs
					dealid       = @params["dealid"]
					responseDate = get_responseDate
					service = User::CreditControl.new(@user, money)
					service.transfer_out!
					Rails.logger.info "Points transfer return out success."
					cash   = User.find_by(id: @user.id).credit_left
					reply  = {errorCode: 0, errorMessage: "Success.", result: {"money" => "+#{money}", "responseDate" => responseDate, "dealid" => dealid, "cash" => cash}}
				else  
					Rails.logger.info "Points exchange failed."
					reply  = {errorCode: 102, errorMessage: "Exchange failed.", result: {} }
				end
			end 
		rescue => exception
			Rails.logger.info exception.message
			reply  = {errorCode: 101, errorMessage: exception.message, result: {} }
			return reply 
		end
	end

	def send_report
	end

	private 

	def get_responseDate
			Time.zone.now.in_time_zone("Beijing").strftime("%Y-%m-%d %H:%M:%S")
	end

	def convert_timeZone_to_EST(vendorTime)
		vendorTime_anchor = Time.zone.now.in_time_zone("Beijing").strftime("%Y-%m-%d %H:%M")
		estTime_anchor    = Time.zone.now.in_time_zone("America/New_York").strftime("%Y-%m-%d %H:%M")
		minute_diffence   = (DateTime.parse(estTime_anchor) - DateTime.parse(vendorTime_anchor))/1.minutes
		estTime           = (DateTime.parse(vendorTime)+minute_diffence.minutes).strftime("%Y-%m-%d %H:%M:%S")
	end

end