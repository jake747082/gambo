# 分割為每個月的佣金報表
module Finance
  class AgentCommissionView < BaseService

    # [important] 自動計算報表不需傳入時間區間
    def call(agent, cond)
      log_begin_date, log_end_date = cond[:begin_date], cond[:end_date]
      @agent = agent
      @commission_reports = []
      if log_begin_date.nil? && log_end_date.nil?
        # 自動計算報表（用於觀看尚未結清的報表）
        if !@agent.commission_at.nil? && Date.today.at_beginning_of_month == @agent.commission_at.to_date.at_beginning_of_month
          # 計算本月的佣金
          @begin_date = Date.today.at_beginning_of_month
          @end_date = Date.today.at_end_of_month
          get_commission_reports
          @commission_reports.push(reports: @commission, user_count_level: @user_count_level, begin_date: @begin_date, end_date: @end_date, bet_users_count: @bet_users_count, total_sum: @total_sum, commission_sum: @commission_sum)
        else
          if @agent.commission_at.nil?
            # 計算註冊月到上個月的佣金
            @begin_date = @agent.created_at.to_date.at_beginning_of_month
            @end_date = @agent.created_at.to_date.at_end_of_month
          else
            # 計算最後一次結算月到上個月的佣金
            @begin_date = @agent.commission_at.to_date.at_beginning_of_month
            @end_date = @agent.commission_at.to_date.at_end_of_month
          end
  
          until Date.today.at_beginning_of_month == @begin_date
            get_commission_reports
            @commission_reports.push(reports: @commission, user_count_level: @user_count_level, begin_date: @begin_date, end_date: @end_date, bet_users_count: @bet_users_count, total_sum: @total_sum, commission_sum: @commission_sum)
            @begin_date = @begin_date.to_date.next_month.at_beginning_of_month
            @end_date = @end_date.to_date.next_month.at_end_of_month
          end
        end
      else
        # 用於查詢佣金紀錄
        # 起始日期
        @begin_date = log_begin_date.at_beginning_of_month
        @end_date = log_begin_date.at_end_of_month

        until log_end_date.next_month.at_beginning_of_month == @begin_date
          get_commission_reports
          @commission_reports.push(reports: @commission, user_count_level: @user_count_level, begin_date: @begin_date, end_date: @end_date, bet_users_count: @bet_users_count, total_sum: @total_sum, commission_sum: @commission_sum)
          @begin_date = @begin_date.to_date.next_month.at_beginning_of_month
          @end_date = @end_date.to_date.next_month.at_end_of_month
        end
      end
      @commission_reports
    end

    private

    def get_commission_reports
      effective_bet_users = ThirdPartyBetForm.effective_bet_users(@agent.id, "#{@begin_date} 00:00:00", "#{@end_date} 23:59:59")
      @bet_users_count = effective_bet_users.length
      @user_count_level, @commission, @total_sum, @commission_sum = Finance::AgentCommission.call(@agent, @bet_users_count, {begin_date: @begin_date, end_date: @end_date})
    end    

  end
end