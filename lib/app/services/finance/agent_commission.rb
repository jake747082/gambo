# 查詢時間區間內某代理各個佣金分類額度及有效會員
module Finance
  class AgentCommission < BaseService

    attr_accessor :begin_date
    # agent : 代理
    # user_count: 當月最低有效會員
    # cond: begin_date / end_date 或其他參數
    def call(agent, user_count, cond)
      cond = {begin_date: Date.today.last_month.at_beginning_of_month, end_date: Date.today.last_month.at_end_of_month }.merge(cond)
      @begin_date, @end_date = cond[:begin_date], cond[:end_date]
      @agent = agent
      @user_count = user_count
      commission_reports = commission
      max_rate = 0
      total_sum = 0 # 
      commission_sum = 0 # 佣金總值
      @commission.each do |type_cd, com|
        if type_cd != 99
          # 非行政
          commission_reports[type_cd][:rate] = calculate(com[:total], type_cd)
          commission_reports[type_cd][:commission] = (com[:total].to_f * commission_reports[type_cd][:rate].to_f / 100).round(2)
          if (max_rate.to_f < commission_reports[type_cd][:rate].to_f)
            max_rate = commission_reports[type_cd][:rate]
          end
          total_sum += com[:total].to_f
          commission_sum += commission_reports[type_cd][:commission].to_f
        end
      end
      commission_reports[99] ||= {}
      a_commission = (commission_reports[99][:total].to_f * max_rate.to_f / 100).round(2)
      if max_rate > 0
        # 行政; 補上rate跟commission
        commission_reports[99][:rate] = max_rate
        commission_reports[99][:commission] = a_commission.to_f
      end
      # 總額加上行政費用
      total_sum += commission_reports[99][:total].to_f
      commission_sum += a_commission.to_f

      [user_count_level, commission_reports, total_sum, commission_sum]
    end

    def commission
      @commission = {}
      reports = AgentCommissionReport.where(agent_id: @agent.id).date_range(@begin_date, @end_date)
      reports.each do |report|
        # set defualt value
        @commission[report.type_cd] ||= default_commission
        @commission[report.type_cd][:count] += report.count.to_i
        @commission[report.type_cd][:total] += report.total.to_f
      end
      @commission
    end

    private

    def default_commission
      {
        count: 0,
        total: 0,
        rate: 0,
        commission: 0
      }
    end

    def user_count_level
      bet_level_limit = Reports::AgentCommission.bet_level_limit
      if @user_count >= bet_level_limit[1][:users]
        1
      elsif @user_count >= bet_level_limit[2][:users]
        2
      elsif @user_count >= bet_level_limit[3][:users]
        3
      elsif @user_count >= bet_level_limit[4][:users]
        4
      elsif @user_count >= bet_level_limit[5][:users]
        5
      else
        0
      end
    end

    def calculate(total, type_cd)
      bet_level_limit = Reports::AgentCommission.bet_level_limit
      game_rate = Reports::AgentCommission.game_rate
      if total >= bet_level_limit[1][:total] && @user_count >= bet_level_limit[1][:users]
        game_rate[type_cd][1]
      elsif total >= bet_level_limit[2][:total] && @user_count >= bet_level_limit[2][:users]
        game_rate[type_cd][2]
      elsif total >= bet_level_limit[3][:total] && @user_count >= bet_level_limit[3][:users]
        game_rate[type_cd][3]
      elsif total >= bet_level_limit[4][:total] && @user_count >= bet_level_limit[4][:users]
        game_rate[type_cd][4]
      elsif total >= bet_level_limit[5][:total] && @user_count >= bet_level_limit[5][:users]
        game_rate[type_cd][5]
      else
        0
      end
    end
  end
end