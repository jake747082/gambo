module Finance
  class DailyCashout < BaseService
    cattr_accessor :cashout_list

    def call(cond)
      cond = { agent: nil, begin_date: Date.yesterday, end_date: Date.today }.merge(cond)
      @agent, @begin_date, @end_date = cond[:agent], cond[:begin_date], cond[:end_date]
      @agent_level, @game_total_bet_forms = cashout_query_information

      cashout_list
    end

    # Slot + Racing + LittleMary
    def cashout_list
      @cashout_list = {}
      total_bet_forms.each do |cashout|
        # set defualt value
        @cashout_list[cashout.date] ||= default_cashout_list

        @cashout_list[cashout.date][:date]              = cashout.date
        @cashout_list[cashout.date][:jackpot_amount]    = cashout.jackpot_amount
        @cashout_list[cashout.date][:bet_count]         += cashout.bet_count.to_i
        @cashout_list[cashout.date][:profit_amount]     += cashout.profit_amount.to_f
        @cashout_list[cashout.date][:bet_amount]        += cashout.bet_amount.to_f
        @cashout_list[cashout.date][:win_amount]        += cashout.win_amount.to_f
        @cashout_list[cashout.date][:owe_parent_amount] += cashout.owe_parent_amount.to_f
        @cashout_list[cashout.date][:valid_amount]      += cashout.valid_amount.to_f
      end
      @cashout_list = @cashout_list.values.sort_by { |cashout| cashout[:date] }.reverse
    end

    private

    # 所有game bet forms
    def total_bet_forms
      total_bet_forms = []
      @game_total_bet_forms.each do |bet_forms|
        total_bet_forms += game_cashout_list(bet_forms)
      end
      total_bet_forms
    end

    def game_cashout_list(bet_forms)
      # Date Range Query, Groupping
      bet_forms.date_range(@begin_date, @end_date).daily_cashout(@agent_level)
    end

    # cashout_list default values
    def default_cashout_list
      {
        bet_count: 0,
        profit_amount: 0,
        bet_amount: 0,
        win_amount: 0,
        owe_parent_amount: 0,
        jackpot_amount: 0,
        valid_amount: 0,
      }
    end

    # Agent Level & BetForms Collection
    def cashout_query_information
      if @agent.present?
        # [ @agent.agent_level.to_s, [ @agent.all_slot_bet_forms, @agent.all_racing_bet_forms, @agent.all_little_mary_bet_forms, @agent.all_poker_bet_forms, @agent.all_fishing_joy_bet_forms, @agent.all_roulette_bet_forms, @agent.all_third_party_bet_forms ] ]
        [ @agent.agent_level.to_s, [  @agent.all_third_party_bet_forms ] ]
      else
        # [ 'shareholder', [ SlotBetForm, RacingBetForm, LittleMaryBetForm, PokerBetForm, FishingJoyBetForm, RouletteBetForm, ThirdPartyBetForm ] ]
        [ 'shareholder', [ ThirdPartyBetForm ] ]
      end
    end
  end
end
