module LangSwitcher
  extend ActiveSupport::Concern

  included do
    before_action :set_locale
  end

  private

  def set_locale
    session[:locale] = params[:locale]  if params[:locale].present?

    I18n.locale = session[:locale] || I18n.default_locale

    redirect_to :back, notice: t('flash.lang.changed') if params[:locale].present?
  end
end