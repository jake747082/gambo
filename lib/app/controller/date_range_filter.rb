module DateRangeFilter
  extend ActiveSupport::Concern

  def set_default_date_range
    # @begin_date = (params[:begin_date] || DateTime.current.change(hour:12)).to_datetime.strftime('%F %T')
    # @end_date = (params[:end_date] || DateTime.current.tomorrow.change(hour:11, min: 59, sec: 59)).to_datetime.strftime('%F %T')
    @begin_date = (params[:begin_date] || DateTime.current.change(hour:0)).to_datetime.strftime('%F %T')
    @end_date = (params[:end_date] || DateTime.current.change(hour:23, min: 59, sec: 59)).to_datetime.strftime('%F %T')
  end

  def set_default_today_range
    @begin_date = (params[:begin_date] || DateTime.current).to_datetime.strftime('%F')
    @end_date = (params[:end_date] || DateTime.current).to_datetime.strftime('%F')
  end
end
