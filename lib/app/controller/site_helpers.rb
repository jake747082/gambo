module SiteHelper
  extend ActiveSupport::Concern

  included do
    cattr_accessor :current_site

    helper_method :remote_ip, :site_config, :last_admin_news, :last_agent_news, :last_www_news
  end

  module ClassMethods
    protected
    def define_site(site_name)
      self.current_site = site_name.to_sym
      before_action :check_site_maintenance!
    end
  end

  # Flatten remote ip method to controller & views (for unicorn + nginx)
  def remote_ip
    @remote_ip = request.env['HTTP_X_FORWARDED_FOR'].nil? ? request.remote_ip : request.env['HTTP_X_FORWARDED_FOR'].split(',')[0]
  end

  def site_config
    @site_config ||= SiteConfig.first!
  end

  def last_admin_news
    @last_admin_news ||=  New.admin_list.present? ? New.admin_list.first! : nil
  end

  def last_agent_news
    @last_agent_news ||= New.agent_list.present? ? New.agent_list.first! : nil
  end

  def last_www_news
    @last_www_news ||= New.www_list.present? ? New.www_list.first! : nil
  end

  # 檢查是否正在維護中
  def check_site_maintenance!
    if self.class.current_site.present? && site_config.send("#{self.class.current_site}_maintenance?")
      respond_to do |format|
        format.html {
          set_page_title 'Site Maintenance'
          render 'common/maintenance', layout: 'maintenance', status: 200
        }
        format.json { render json: { status: false, messages: 'service unavaliable' }, status: 503 }
      end
      return false
    end
  end
end