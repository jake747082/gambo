module TransferCreditHelper
  def transfer_credit_from_and_to_and_action(log)
    case log.key
    when 'user.transfer_credit'
      [t("game_platforms.#{log.parameters[:from_platform]}"), t("game_platforms.#{log.parameters[:to_platform]}"), t("transfer_credit.action.transfer_credit")]
    when 'user.apply_cashout'
      [ t("game_platforms.mdragon"), '-', t("transfer_credit.action.apply_cashout")]
    when 'user.deposit'
      if log.owner_type == 'User'
        # 虛擬幣存款
        [ '-', t("game_platforms.mdragon"),t("transfer_credit.action.dc_deposit")]
      else
        [ '-', t("game_platforms.mdragon"), t("transfer_credit.action.deposit")]
      end
    when 'user.cashout'
      if log.owner_type == 'User'
        if log.status == 'failed'
          # 線上支付退回
          ['-', t('game_platforms.mdragon'), t("transfer_credit.action.dc_cashout_failed")]
        else
          # 虛擬幣下分
          [t("game_platforms.mdragon"), '-', t('transfer_credit.action.dc_cashout')]
        end
      else
        if log.status == 'removed'
          # 申請退回
          ['-', t('game_platforms.mdragon'), t("transfer_credit.action.dc_cashout_removed")]
        else
          [t("game_platforms.mdragon"), '-', t('transfer_credit.action.cashout')]
        end
      end
    end
  end
end