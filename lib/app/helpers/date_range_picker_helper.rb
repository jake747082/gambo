module DateRangePickerHelper
  def render_daterange_picker(url_helper, options={})
    list = (options[:list] || {})
    resource = options[:resource]
    params = options[:params]

    date_range = date_picker_map.collect do |option|
      link_name, date_params = option.first, option.last
      content_tag(:li) do
        if resource
          link_to link_name, send(url_helper, resource, params.merge(begin_date: date_params[:begin_date], end_date: date_params[:end_date]))
          # content_tag(:a, link_name, {href: "/users/#{resource.id}/finances?begin_date=#{date_params[:begin_date]}&end_date=#{date_params[:end_date]}"})
        else
          link_to link_name, send(url_helper, params.merge(begin_date: date_params[:begin_date], end_date: date_params[:end_date]))
          # content_tag(:a, link_name, {href: "/finances?begin_date=#{date_params[:begin_date]}&end_date=#{date_params[:end_date]}"})
        end
      end
    end.join.html_safe

    return date_range

  end

  private

  def date_picker_map
    @date_picker_map ||= [
      [t('date.today'), format_report_date_time(Date.today, Date.today)],
      [t('date.yesterday'), format_report_date_time(Date.yesterday, Date.yesterday)],
      [t('date.this_week'), format_report_date_time(Date.today.at_beginning_of_week, Date.today.at_end_of_week)],
      [t('date.last_week'), format_report_date_time(Date.today.last_week.at_beginning_of_week, Date.today.last_week.at_end_of_week)],
      [t('date.this_month'), format_report_date_time(Date.today.at_beginning_of_month, Date.today.at_end_of_month)],
      [t('date.last_month'), format_report_date_time(Date.today.last_month.at_beginning_of_month, Date.today.last_month.at_end_of_month)]
    ]
  end

  def format_report_date_time(begin_date, end_date)
    # begin_date = begin_date.to_datetime.change(hour: 12).strftime('%F %T')
    # end_date = end_date.to_datetime.change(hour:11, min: 59, sec: 59).strftime('%F %T')
    begin_date = begin_date.to_datetime.change(hour: 0).strftime('%F %T')
    end_date = end_date.to_datetime.change(hour:23, min: 59, sec: 59).strftime('%F %T')

    hash = {begin_date: begin_date, end_date: end_date}
    return hash
  end

  def begin_end_date_match(begin_date, end_date)
    return @begin_date == begin_date && @end_date == end_date
  end
end
