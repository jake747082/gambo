module FormHelper
  def search_form(name, value, to_path, opts={})
    opts = { placeholder: t('forms.search_placeholder'), class: 'form-inline', method: :get }.merge(opts)
    placerholder = opts.delete(:placeholder)
    form_tag(to_path, opts) do
      text_field_tag name, value, class: 'form-control', placeholder: placerholder, autofocus: true
    end
  end

  def search_form_type(name, value, to_path, type, opts={})
    opts = { placeholder: t('forms.search_placeholder'), class: 'form-inline', method: :get }.merge(opts)
    placerholder = opts.delete(:placeholder)
    form_tag(to_path, opts) do
      query = text_field_tag name, value, class: 'form-control', placeholder: placerholder, autofocus: true
      type = hidden_field_tag 'type', type
      query << type
    end
  end
end
