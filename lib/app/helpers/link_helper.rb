module LinkHelper
  # 空 link href
  def void_link
    "javascript:void(true);"
  end

  def agent_promo_url(agent)
    "#{Setting.site.cash.scheme}://#{Setting.site.cash.domain}/register.html?code=#{agent.recommend_code}"
  end

  def static_page_url(page)
    "#{Setting.site.cash.scheme}://#{Setting.site.cash.domain}/docs/#{page.page_id}"
  end

  def copy_button(text, opts={})
    default_options = {
      "data-clipboard-target" => "fe_text",
      "data-clipboard-text" => text,
      "data-done" => t('btn.copy_done')
    }

    opts = default_options.merge(opts)

    opts[:class] ||= ""
    opts[:class] += " copy_me"

    title = (opts.delete(:title) || t('btn.copy'))

    link_to(title, void_link, opts)
  end
end