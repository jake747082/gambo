module AgentsHelper
  def render_lock_buttom(agent)
    if agent.lock?
      if agent.parent.present? && agent.parent.lock?
        link_to(t('btn.unlock'), parent_lock_notice_agent_locks_path(agent, format: :js), method: :get, remote: true, class: 'btn btn-danger')
      else
        link_to(t('btn.unlock'), destroy_confirm_agent_locks_path(agent, format: :js), method: :get, remote: true, class: 'btn btn-danger')
      end
    else
      link_to(t('btn.lock'), agent_locks_path(agent), method: :post, confirm: t('confirm.lock_tree', total_agents_count: @agent.total_agents_count, total_users_count: @agent.total_users_count), class: 'btn btn-default')
    end
  end

  def render_api_open_buttom(agent)
    if agent.open_api?
      link_to(t('btn.close_api'), agent_open_apis_path(agent), method: :delete, class: 'btn btn-danger')
    else
      link_to(t('btn.open_api'), agent_open_apis_path(agent), method: :post, class: 'btn btn-danger')
    end
  end

  def render_user_permission_button(agent, opt = {})
    opt[:close_title] ||= t('btn.close_user_permission')
    opt[:open_title] ||= t('btn.open_user_permission')
    if agent.user_permission?
      confirm = @agent.in_final_level? ? t('confirm.close_user_permission') : t('confirm.close_user_permission_tree', total_agents_count: @agent.total_agents_count, total_users_count: @agent.total_users_count)
      link_to(opt[:close_title], agent_user_permission_path(agent), method: :delete, data: {confirm: confirm}, class: "btn btn-danger #{opt[:class]}")
    else
      if agent.parent.present? && !agent.parent.user_permission?
        link_to(opt[:open_title], notice_agent_user_permission_path(agent, format: :js), method: :get, remote: true, class: "btn btn-default #{opt[:class]}")
      else
        link_to(opt[:open_title], open_confirm_agent_user_permission_path(agent, format: :js), method: :get, remote: true, class: "btn btn-default #{opt[:class]}")
      end
    end
  end
end