module HistoriesHelper

  # 視訊平台選項
  def platform_video_opions
    GamePlatform.video_list.map do |game_platform|
      [game_platform.human_platform_name, game_platform.id]
    end.unshift([t('shared.option.default'), ''])
  end

  # 該視訊平台內的所有遊戲
  def platform_video_games_opions(game_platform_id)
    if game_platform_id.present?
      game_list_kind(game_platform_id)
    else
      [t('shared.option.default'), '']
    end
  end

  # 各遊戲歷程連結（第三方遊戲分類）
  def all_history_games_links(search_params = {}, role_search_params = {}, opts = {})
    content_tag(:ul, {class: 'nav justify-content-start'}) do
      history_games_type.collect do |game_type|
        opts[:class] = if search_params[:type] == game_type
          'nav-item active'
        else
          'nav-item'
        end
        content_tag(:li, opts) do
          path = game_type == 'main' ? 'histories_main_index_path' : 'histories_game_index_path'
          link_to(t("game_platforms.#{game_type}"), send(path, type: game_type, begin_date: search_params[:begin_date], end_date: search_params[:end_date], role: role_search_params), class: 'nav-link p_transfer')
        end
      end.join('').html_safe
    end +
    content_tag(:ul, {class: 'nav justify-content-start'}) do
      history_games_categories(search_params[:type]).collect do |name, category_id|
        opts[:class] = if search_params[:category].to_i == category_id.to_i
          'nav-item active'
        else
          'nav-item'
        end
        content_tag(:li, opts) do
          path = search_params[:type] == 'main' ? 'histories_main_index_path' : 'histories_game_index_path'
          link_to(t("game_platforms.categories.#{name}"), send(path, type: search_params[:type], category: category_id, begin_date: search_params[:begin_date], end_date: search_params[:end_date], role: role_search_params), class: 'nav-link p_transfer')
        end
      end.join('').html_safe
    end
  end

  # def all_history_games_links(search_params = {}, role_search_params = {}, opts = {})
  #   content_tag(:ul, {class: 'nav nav-pills'}) do
  #     history_games_type.collect do |game_type|
  #       opts[:class] = if search_params[:type] == game_type
  #         'pull-left active'
  #       else
  #         'pull-left'
  #       end
  #       content_tag(:li, opts) do
  #         path = game_type == 'main' ? 'histories_main_index_path' : 'histories_game_index_path'
  #         link_to(t("game_platforms.#{game_type}"), send(path, type: game_type, begin_date: search_params[:begin_date], end_date: search_params[:end_date], role: role_search_params))
  #       end
  #     end.join('').html_safe
  #   end +
  #   content_tag(:ul, {class: 'nav nav-pills push-xs'}) do
  #     history_games_categories(search_params[:type]).collect do |name, category_id|
  #       opts[:class] = if search_params[:category].to_i == category_id.to_i
  #         'pull-left active'
  #       else
  #         'pull-left'
  #       end
  #       content_tag(:li, opts) do
  #         path = search_params[:type] == 'main' ? 'histories_main_index_path' : 'histories_game_index_path'
  #         link_to(t("game_platforms.categories.#{name}"), send(path, type: search_params[:type], category: category_id, begin_date: search_params[:begin_date], end_date: search_params[:end_date], role: role_search_params))
  #       end
  #     end.join('').html_safe
  #   end
  # end

  # 各遊戲歷程連結（本館遊戲分類）
  def main_history_games_links(type, begin_date, end_date, role_search_params = {}, opts = {})
    main_history_games_type.collect do |game_type|
      opts[:class] = if type == game_type
        'pull-left active'
      else
        'pull-left'
      end
      content_tag(:li, opts) do
        link_to(t("breadcrumbs.histories.#{game_type}"), send("histories_main_index_path", type: game_type, begin_date: begin_date, end_date: end_date, role: role_search_params))
      end
    end.join('').html_safe
  end

  # 各遊戲匯出連結（第三方遊戲分類）
  def all_history_games_export_links(search_params, role_search_params)
    if history_games_type.include?(search_params[:type])
      path = search_params[:type] == 'main' ? 'export_histories_main_index_path' : 'export_histories_game_index_path'
      link_to(t('btn.export'), send(path, format: "xlsx", type: search_params[:type], begin_date: search_params[:begin_date], end_date: search_params[:end_date], category: search_params[:category], role: role_search_params), class: 'btn btn-default')
    end
  end


  # 各遊戲匯出連結（本館遊戲分類）
  def main_history_games_export_links(type, begin_date, end_date, role_search_params)
    if main_history_games_type.include?(type)
      link_to(t('btn.export'), send("export_histories_#{type}_index_path", format: "xlsx", begin_date: begin_date, end_date: end_date, role: role_search_params), class: 'btn btn-default')
    end
  end

  private

  # 遊戲歷程裡的第三方遊戲分類
  def history_games_type
    # ['main', 'ag', 'mg', 'east', 'bbin']
    # ['east', 'cagayan', 'bt', 'kgame']
    # ['bt', 'east', 'cq9', 'sf']
    ['all', 'bt', 'bts', 'ag_sw', 'east', 'wm']
  end

  def history_games_categories(game_platform_name)
    GamePlatforms::Games.categories(game_platform_name)
  end

  # 遊戲歷程裡的本館遊戲分類
  def main_history_games_type
    ['slot', 'little_mary', 'poker', 'roulette', 'fishing_joy', 'video']
  end

  def greater_agent_level(agent)
    agent.agent_level_cd <= 2
  end
  
  def greater_director_level(agent)
    agent.agent_level_cd <= 1
  end

  def greater_shareholder_level(agent)
    agent.agent_level_cd <= 0
  end
end
