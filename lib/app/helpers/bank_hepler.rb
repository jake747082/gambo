module BankHelper
  def select_bank_options
    BankAccount.bank_options.map do |option|
      [I18n.t("bank_accounts.bank_title.#{option}"), option]
    end
  end

  def select_admin_bank_options
    AdminBankAccount.bank_options.map do |option|
      [I18n.t("bank_accounts.bank_title.#{option}"), option]
    end
  end

  def select_admin_deposit_bank_account_options
    AdminBankAccount.list_deposit.map do |bank|
      ["#{bank.human_bank_title} - #{bank.sub_bank_name} ( #{bank.title} - #{bank.sensitive_filter(:account, ratio: 0.5)} )", bank.id]
    end
  end
end