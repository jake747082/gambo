class UserMailer < ActionMailer::Base
  default from: Setting.mail.from
  layout false

  def verification_code_email(user, content)
    mail(to: user.email, subject: "[#{Setting.mail.title}] 帐户安全性验证码", body: content)
  end

  def send_email(to_email, subject, content)
    mail(to: to_email, subject: subject, body: content)
  end
end