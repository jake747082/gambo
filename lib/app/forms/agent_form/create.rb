class AgentForm::Create < AgentForm::Base

  property :parent_id, type: Integer
  property :username
  property :level, virtual: true
  property :type_cd
  property :owner_id

  validates :level, inclusion: { in: Agent.agent_levels.hash.values, message: :invalid }

  def create(params)
    Agent.unscoped do
      # type必須同上層
      params["type_cd"] = parent.type_cd if parent.present?
      save_with_transaction(params) do
        model.agent_level_cd = model.level
        model.recommend_code = model.generate_recommend_code if model.in_final_level?
        if parent.present?
          # P.S 為了要 update agents updated_at 所以不用 increment_counter
          parent.self_and_ancestors.lock!.each do |update_agent|
            # 往上遞增所有代理樹 total_machines_count
            update_agent.increment!(:total_agents_count)
          end
        end
      end
    end
  end
end