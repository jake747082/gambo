class AgentForm::Apply < AgentForm::Base

  property :parent_id, type: Integer
  property :username
  property :level, virtual: true
  property :type_cd
  property :name
  property :mobile
  property :qq
  property :email
  property :lock
  property :reviewing
  property :bank_account, virtual: true
  property :agree_terms, virtual: true

  validates :level, inclusion: { in: Agent.agent_levels.hash.values, message: :invalid }
  validates :name, presence: true, length: { in: 2..50 }
  validates :mobile, presence: true, numericality: true, length: {in: 10..15}
  validates :agree_terms, acceptance: true, presence: true
  validate :valid_bank_account

  def create(params)
    Agent.unscoped do
      # type必須同上層
      params["type_cd"] = parent.type_cd if parent.present?
      @bank_account = params.delete("bank_account")
      save_with_transaction(params) do
        model.agent_level_cd = model.level
        model.recommend_code = model.generate_recommend_code if model.in_final_level?
        # valid_bank_account(bank_account)
        unless @bank_account.empty?
          model.build_bank_account(@bank_account)
        end
        if parent.present?
          # P.S 為了要 update agents updated_at 所以不用 increment_counter
          parent.self_and_ancestors.lock!.each do |update_agent|
            # 往上遞增所有代理樹 total_machines_count
            update_agent.increment!(:total_agents_count)
          end
        end
      end
    end
  end

  def valid_bank_account
    if @bank_account.nil? || @bank_account.empty?
      errors.add(:bank_account, :invalid)
    end
    if @bank_account["bank_id"].empty?
      errors.add(:bank_account, :bank_id)
    end
    if @bank_account["city"].empty?
      errors.add(:bank_account, :city)
    end
    if @bank_account["province"].empty?
      errors.add(:bank_account, :province)
    end
    if @bank_account["subbranch"].empty?
      errors.add(:bank_account, :subbranch)
    end
    if @bank_account["account"].empty?
      errors.add(:bank_account, :account)
    end
    if @bank_account["security_code"].empty? || @bank_account["security_code"].length != 6 || @bank_account["security_code"].match(/^[0-9]*$/).nil?
      errors.add(:bank_account, :security_code)
    end
    if @bank_account["security_code_confirmation"].empty?
      errors.add(:bank_account, :security_code_confirmation)
    end
    if @bank_account["security_code_confirmation"] != @bank_account["security_code"]
      errors.add(:bank_account, :security_code_confirmation_invalid)
    end
  end
end