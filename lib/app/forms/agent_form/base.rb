class AgentForm::Base < SharedForm::Authable
  model :agent

  property :parent, virtual: true
  property :credit_max, type: Float
  property :casino_ratio, type: Float

  before_transaction :do_lock_agents
  before_commit :do_decrement_parent_agent_credit

  # 暱稱
  validates :nickname, presence: true
  validate :valid_the_agent_of_parent
  validates_numericality_of :credit_max, :casino_ratio, greater_than_or_equal_to: 0
  validates_numericality_of :casino_ratio, less_than_or_equal_to: 100

  def human_agent_level
    agent_level =  (parent.present? ? parent.agent_level_next : :shareholder )
    I18n.t("activerecord.enums.agent.agent_levels.#{agent_level}")
  end

  protected
  # 信用額度的差異 (拿 form 去跟 model 內資料去扣掉)
  # 這裡需要 cache 的原因是:
  # => 一旦儲存了 form 之後 model 的值則被寫入，所以必須先 cache 起來
  # => 後續 callback 要把 parent agent 的值扣掉要用的
  def credit_max_diff
    @credit_max_diff ||= (credit_max - model.credit_max)
  end

  def max_casino_ratio
    @max_casino_ratio ||= parent.casino_ratio
  end

  def max_credit
    @max_credit ||= (model.credit_max + parent.credit_undispatched)
  end

  private

  # 檢驗上層是否還有 agent (有可能是 admin create shareholder 不會有上層)
  def valid_the_agent_of_parent
    return if parent.blank?

    errors.add(:parent, :invalid) if parent.in_final_level?

    valid_credit_dispatching
    valid_ratios_dispatching
  end

  # 檢驗 credit 是否載允許值內 (僅有 parent 時才檢查)
  def valid_credit_dispatching
    if credit_max_diff != 0 && (credit_max_diff > parent.credit_undispatched)
      errors.add(:credit_max, :less_than, max_credit: max_credit)
    end
  end

  # 檢驗 ratios 是否載允許值內 (僅有 parent 時才檢查)
  def valid_ratios_dispatching
    if casino_ratio > parent.casino_ratio
      errors.add(:casino_ratio, :less_than, parent_casino_ratio: parent.casino_ratio)
    end
  end

  # transaction 開始前先 lock agent, parent agent
  def do_lock_agents
    parent.lock!(true) if parent.present?
    model.lock! if model.persisted?
  end

  # 儲存完成後遞增父親必須扣掉 credit_used
  def do_decrement_parent_agent_credit
    parent.increment!(:credit_dispatched, credit_max_diff) if parent.present?
  end
end