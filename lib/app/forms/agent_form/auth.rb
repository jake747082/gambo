class AgentForm::Auth < BaseForm::Basic

  form_name :auth

  attribute :username
  attribute :password

  def initialize(username)
    self.username = username
  end

  def verify(password)
    [ Agent, AgentSubAccount ].each do |model|
      account = model.find_by_username(username)
      if account && account.valid_password?(password)
        yield if block_given?
        return true
      end
    end
    errors.add(:password, :invalid)
    false
  end
end