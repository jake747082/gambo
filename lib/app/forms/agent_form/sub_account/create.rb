class AgentForm::SubAccount::Create < AgentForm::SubAccount::Base
  property :username

  alias_method :create, :save_with_transaction
end