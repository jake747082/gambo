class AgentForm::Update < AgentForm::Base

  property :parent_id, virtual: true, type: Integer
  property :username, virtual: true
  property :name
  property :mobile
  property :qq
  property :email

  validate :valid_credit_max_can_not_over_credit_dispatched

  def update(params)
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation)
    end
    save_with_transaction(params)
  end

  private

  def valid_credit_max_can_not_over_credit_dispatched
    if model.credit_dispatched > credit_max
      errors.add(:credit_max, :greater_than_or_equal_to, credit_dispatched: model.credit_dispatched)
    end
  end
end