class UserForm::TransferCredit < BaseForm::Basic
  form_name :transfer_credit

  attr_reader :user

  attribute :credit, Float
  attribute :from_platform
  attribute :to_platform

  validates :credit, presence: true
  # validates_numericality_of :credit, greater_than_or_equal_to: 10, only_integer: true
  validate :verify_platform
  validate :verify_credit

  def initialize(user, params={})
    @user = user
    super(params)
  end

  def transfer_credit
    User.transaction do
      user.lock!
      return false unless valid?

      # 扣款
      from_platform_name = (from_platform == 'mg' || from_platform == 'bbin') ? "ag_#{from_platform}" : from_platform
      cashout = GamePlatforms::User.new(from_platform_name, user)

      cashout_status = cashout.transfer_credit(credit, 'OUT')
      Rails.logger.info cashout_status
      unless cashout_status
        errors.add(:from_platform, :cashout_error)
        return false
      end

      if cashout_status == 3003
        errors.add(:from_platform, :playing)
        return false
      end

      # 存款
      to_platform_name = (to_platform == 'mg' || to_platform == 'bbin') ? "ag_#{to_platform}" : to_platform
      deposit = GamePlatforms::User.new(to_platform_name, user)
      deposit_status = deposit.transfer_credit(credit, 'IN')

      unless deposit_status
        # 將款項退回
        deposit = GamePlatforms::User.new(from_platform_name, user)
        deposit_status = deposit.transfer_credit(credit, 'IN')
        errors.add(:to_platform, :deposit_error)
        return false
      end

    end
    true
  end

  private

  def verify_credit
    if from_platform == 'mdragon' && user.credit_left.to_f < credit.to_f
      errors.add(:credit, :invaild)
    end
  end

  def verify_platform
    if from_platform.nil? || to_platform.nil? || from_platform == to_platform || !GamePlatform.exists?(name: from_platform) || !GamePlatform.exists?(name: to_platform)
      errors.add(:from_platform, :platform_error)
    end
  end

end
