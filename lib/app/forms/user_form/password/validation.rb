class UserForm::Password::Validation < BaseForm::Basic

  attribute :auth_type_cd
  attribute :email
  attribute :mobile
  attribute :checkcode

  validates :auth_type_cd, numericality: {message: 'invalid'}, allow_nil: true
  validates :email, format: {with: Devise::email_regexp, message: 'invalid'}, length: {maximum: 255, message: 'invalid'}, allow_blank: true
  validates :mobile, numericality: {message: 'invalid'}, length: {minimum: 11, maximum: 13, message: 'invalid'}, allow_blank: true
  validates :checkcode, numericality: {message: 'invalid'}, length: { is: 6, message: 'invalid' }
  validate :verify_checkcode

  attr_reader :user

  def initialize(params={})
    super(params)
  end

  def run
    begin
      return false unless valid?
      @user.verification_code = ""
      @user.verification_token = generate_confirmation_token
      @user.verification_at = Time.zone.now
      @user.save!
    rescue => e
      p e.message
      false
    end
  end

  private

  def verify_mobile_prefix
    unless mobile.start_with?(*User::SendSms::ENABLE_CODE)
      errors.add(:mobile, 'invalid')
    end
  end

  def verify_checkcode
    if auth_type_cd == 0
      if mobile.nil? || mobile.empty? || verify_mobile_prefix
        errors.add(:mobile, 'invalid') 
        return
      end
      @user = User.find_by(mobile: mobile)
      if @user.nil?
        errors.add(:mobile, 'not_exist')
        return
      end
    else
      if email.nil? || email.empty?
        errors.add(:email, 'invalid')
        return
      end
      @user = User.find_by(email: email)
      if @user.nil?
        errors.add(:email, 'not_exist')
        return
      end
    end
    if @user.verification_code.nil? || @user.verification_code.empty?
      errors.add(:checkcode, 'invalid')
      return
    end
    if Time.zone.now > @user.verification_send_at + 5.minutes
      errors.add(:confirmation_token, 'invalid')
      return
    end
    unless @user.verification_code == checkcode
      errors.add(:checkcode, 'invalid')
    end
  end

  def generate_confirmation_token
    raw, enc = Devise.token_generator.generate(User, :verification_token)
    enc
  end
end