class UserForm::Password::SendValidation < BaseForm::Basic
  
  attribute :auth_type_cd
  attribute :email
  attribute :mobile

  validates :auth_type_cd, numericality: {message: 'invalid'}, allow_nil: true
  validates :email, format: {with: Devise::email_regexp, message: 'invalid'}, length: {maximum: 255, message: 'invalid'}, allow_blank: true
  validates :mobile, numericality: {message: 'invalid'}, length: {minimum: 11, maximum: 13, message: 'invalid'}, allow_blank: true
  validate :verify_user_exist

  def initialize(params={})
    super(params)
  end

  def run
    begin
      return false unless valid?
      user_params = if auth_type_cd == 0
        {auth_type_cd: auth_type_cd, mobile: mobile}
      else
        {auth_type_cd: auth_type_cd, email: email}
      end
      user = User.find_or_initialize_by(user_params)
      if !user.verification_send_at.nil? && Time.zone.now <= user.verification_send_at + 1.minutes
        field = auth_type_cd == 0 ? :mobile : :email
        errors.add(field, 'too_often')
        return false
      end
      user.verification_code = new_verification_code
      user.verification_send_at = Time.zone.now
      user.increment(:verification_send_count) 
      user.save!
      # send verification_code
      if auth_type_cd == 0
        # 發送簡訊
        sms = User::SendSms.new(user)
        sms.send_verification_code!
        true
      else
        # 發送email
        content = "欢迎！您的验证码为#{user.verification_code}，请及时完成注册程序。"
        Thread.new { UserMailer.verification_code_email(user, content).deliver! }
        true
      end
    rescue => e
      p e.message
      false
    end
  end

  private
  
  def verify_user_exist
    if auth_type_cd == 0
      if mobile.nil? || mobile.empty? || verify_mobile_prefix
        errors.add(:mobile, 'invalid') 
        return
      end
      errors.add(:mobile, 'not_exist') unless User.exists?(mobile: mobile)
    else
      if email.nil? || email.empty?
        errors.add(:email, 'invalid')
        return
      end
      errors.add(:email, 'not_exist') unless User.exists?(email: email)
    end
  end

  def verify_mobile_prefix
    unless mobile.start_with?(*User::SendSms::ENABLE_CODE)
      errors.add(:mobile, 'invalid')
    end
  end

  def new_verification_code
    chr = (0..9).to_a
    (0..5).map { chr.sample }.join
  end
end