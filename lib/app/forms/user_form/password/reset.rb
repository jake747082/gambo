class UserForm::Password::Reset < BaseForm::Basic

  attribute :confirmation_token
  attribute :password
  attribute :password_confirmation

  validates :password, presence: true, length: { in: 8..24, message: 'wrong_format' }, confirmation: { message: 'confirmation' }
  validates :password_confirmation, presence: {message: 'confirmation'}
  validate :verify_confirmation_token

  def initialize(params={})
    super(params)
  end

  def reset
    begin
      return false unless valid?
      ActiveRecord::Base.transaction do
        @user.lock!
        @user.verification_token = ""
        @user.password = password
        @user.save!
      end
    rescue => e
      p e.message
      false
    end
  end

  private

  def verify_confirmation_token
    if confirmation_token.nil? || confirmation_token.empty?
      errors.add(:confirmation_token, 'invalid')
      return false
    end
    @user = User.find_by(verification_token: confirmation_token)
    if @user.nil?
      errors.add(:confirmation_token, 'invalid')
      return false
    end
    if Time.zone.now > @user.verification_at + 5.minutes
      errors.add(:confirmation_token, 'invalid')
      return 
    end
  end
end