class UserForm::Auth < BaseForm::Basic

  form_name :auth

  attribute :username
  attribute :password

  attribute :user

  def initialize(username)
    self.username = username
    self.user = User.find_by_username(username)
  end

  def verify(password)
    if user && user.valid_password?(password)
      yield if block_given?
      return true
    end
    errors.add(:password, :invalid)
    false
  end
end