class UserForm::Base < SharedForm::Authable
  model :user

  property :agent, virtual: true
  property :credit, type: Integer
  property :name
  property :mobile
  property :email
  property :qq

  validates_numericality_of :credit, greater_than_or_equal_to: 0
  validate :valid_agent_and_credit

  # Workflow
  before_transaction :do_lock_agent
  before_commit :do_decrement_agent_credit

  protected
    def credit_diff
      @credit_diff ||= (credit.to_i - model.credit.to_i)
    end

    def max_credit
      @max_credit ||= (model.credit + agent.credit_undispatched)
    end

  private
    def do_lock_agent
      agent.lock!(true)
    end

    def do_decrement_agent_credit
      agent.increment!(:credit_used, credit_diff)
    end

    def valid_agent_and_credit
      # 檢驗 agent 是否為 <最底層代理>
      unless agent.in_final_level?
        errors.add(:agent, I18n.t('forms.errors.user_form.base.agent.in_final_level'))
      end
      # 檢驗 agent's credit 是否足夠
      if credit_diff != 0 && (credit_diff > agent.credit_undispatched)
        errors.add(:credit, I18n.t('forms.errors.user_form.base.credit.less_than'))
      end
    end
end