class UserForm::Create < UserForm::Base
  property :username
  property :user_group_id

  def create(params)
    User.unscoped do
      save_with_transaction(params) do
        # P.S 為了要 update agents updated_at 所以不用 increment_counter
        agent.self_and_ancestors.lock!.each do |update_agent|
          # 1. 往上遞增所有代理樹 total_users_count
          update_agent.increment!(:total_users_count)

          # 2. 把 shareholder, director 依序往上填 id
          model.send("#{update_agent.agent_level}_id=", update_agent.id)
        end
      end
    end
  end
end