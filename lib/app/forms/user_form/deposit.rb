class UserForm::Deposit < BaseForm::Basic
  form_name :deposit

  attr_reader :user, :credit_left, :withdraw, :cashout_model

  attribute :deposit_credit, Float
  attribute :trans_id, String
  attribute :type, Integer
  attribute :order_info

  validates :deposit_credit, presence: true
  validate :verify_agent_credit_left

  def initialize(user, params={})
    @user = user
    @credit_left = user.credit_left
    super(params)
  end

  def deposit
    unless Withdraw.exists?(trans_id: trans_id)
      User.transaction do
        user.lock!
        user.agent.lock!
        return false unless valid?
        # 新增withdraw
        @withdraw = Withdraw.new({
          cash_type: 0,
          user: user,
          trans_id: trans_id,
          credit: deposit_credit,
          credit_actual: deposit_credit,
          status: 2, # transferred
          type: type || 0,
          order_info: order_info
        })
        withdraw.save!
        if type == Withdraw.types[:agent]
          @cashout_model = AgentWithdraw.new({
            cash_type_cd: 3,
            agent: user.agent,
            points: deposit_credit,
          })
          cashout_model.save!
          user.agent.use_points!(deposit_credit)
        end
      end
    end
    true
  end

  private

  def verify_agent_credit_left
    # 若為ppp tranfer，deposit_credit可為0 並 不被agent限制
    if type != Withdraw.types[:ppp]
      if !user.agent.is_points_type? && deposit_credit.to_f > user.agent.credit_left
        errors.add(:deposit_credit, :less_than, credit_left: user.agent.credit_left)
      end
      if deposit_credit.to_f <= 0
        errors.add(:deposit_credit, :greater_than_or_equal_to, count: 0)
      end
      if user.agent.is_points_type? && deposit_credit.to_f > user.agent.points
        errors.add(:deposit_credit, :less_than, credit_left: user.agent.points)
      end
    end
  end
end