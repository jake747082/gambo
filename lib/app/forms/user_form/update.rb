class UserForm::Update < UserForm::Base
  property :username, virtual: true
  property :note

  property :user_group_id

  def update(params)
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation)
    end
    save_with_transaction(params) do
      # user_game_platformship = params[:user_game_platformship]
      # if user_game_platformship[:video_limit].present?
      #   model.user_game_platformships.east.update!({
      #     video_limit: user_game_platformship[:video_limit],
      #     roulette_limit: user_game_platformship[:roulette_limit],
      #     game_limit: user_game_platformship[:game_limit]
      #   })
      # end
    end
  end
end