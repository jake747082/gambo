require 'reform'
require 'reform/form/active_record'
require 'reform/form/coercion'

class BaseForm::Reform < Reform::Form
  include Gambo::Reform::AtomicSave
  include Reform::Form::ActiveRecord
  include Reform::Form::Coercion

  # 定義 i18n scope
  def self.i18n_prefix(i18n_scope)
    define_singleton_method :i18n_scope do
      i18n_scope.to_sym
    end
  end
end