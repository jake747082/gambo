class SharedForm::Authable < BaseForm::Reform
  i18n_prefix :forms
  property :nickname
  property :password
  property :password_confirmation

  # 帳號
  validates :username,
    presence: true,
    length: { in: 6..128 },
    uniqueness: { message: :unique },
    format: { with: /\A[a-zA-Z0-9_]+\z/ }
  # TODO: 帳號如果允許 .，必須在現金版 subdomain 做特別處理

  # 密碼
  validates :password, confirmation: { message: :confirmation }
  validates :password, presence: true, length: { in: 8..128 }, if: :require_password?

  private

  def require_password?
    model.new_record? || password.present? || password_confirmation.present?
  end
end