class MessageForm::Update < BaseForm::Reform
  i18n_prefix :forms
  model :message

  property :title
  property :content

  validates :title, :content, presence: true

  def update(params)
    save_with_transaction(params)
  end
end
