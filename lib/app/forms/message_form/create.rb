class MessageForm::Create < BaseForm::Reform
  i18n_prefix :forms
  model :message

  property :title
  property :content
  property :groups
  property :group_names
  # property :agent_port
  # property :www_port

  attr_accessor :username
  # attr_accessor :agent_username
  attr_accessor :groups

  attr_reader :user_id
  # attr_reader :agent_id

  attr_reader :user
  # attr_reader :agent

  validates :title, :content, presence: true
  validate :validate_username, :validate_target_member

  def create(params)
    self.username = params[:username]
    # self.agent_username = params[:agent_username]
    self.groups = params[:groups]
    
    # agent
    # if agent_username.present?
    #   save_with_transaction(params) do
    #     model.assign_attributes(agent_id: @agent.id)
    #   end
    # end
    save_with_transaction(params) do
      model.assign_attributes(user_id: @user.id) if username.present?

      user_ids = username.present? ? [@user.id] : []
      group_names = []
      if params[:groups].present? && !params[:groups].empty?
        params[:groups].each do |group|
          if group.to_i > 0
            user_group = UserGroup.find(group)
            if user_group.present?
              user_ids += user_group.users.ids 
              group_names.push(user_group.name)
            end
          end
        end
      end
      model.assign_attributes(group_names: group_names.join(',')) unless group_names.empty?
      # threads = []
      user_ids.uniq.each do |user_id|
        # threads << Thread.new do
          model.read_messages.new(user_id: user_id).save!
        # end
        # threads.each(&:join)
      end
    end
  end

  private

  def validate_username
    @user = User.find_by_username(username)
    # @agent = Agent.find_by_username(agent_username)
    errors.add(:username, :invalid) if @user.blank? && username.present?
    # errors.add(:agent_username, :invalid) if @agent.blank? && agent_username.present?
  end

  # 不可沒有發送目標
  def validate_target_member
    # errors.add(:username, :invalid) if @user.blank? && @agent.blank? && groups.empty?
    errors.add(:username, :invalid) if @user.blank? && (groups.nil? || groups.reject(&:empty?).empty?)
  end
end
