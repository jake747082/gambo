class GamePlatforms::User

  attr_accessor :game_platform

  def initialize(game_platform_name, user = nil)
    exception_handle { 
      self.game_platform = "GamePlatforms::#{game_platform_name.camelize}".constantize.new(user)
    }
  end

  # 取得餘額
  def credit_left
    exception_handle { 
      game_platform.get_balance
    }
  end

  # 進入遊戲
  def transfer_game_url
    exception_handle { 
      game_platform.transfer_game
    }
  end

  # 進入demo遊戲
  def transfer_demo_game_url
    exception_handle { 
      game_platform.transfer_credit(2000, 'IN')
      game_platform.transfer_game
    }
  end

  def update_limit(limit, limittype)
    exception_handle { 
      game_platform.update_limit(limit, limittype)
    }
  end

  # 轉點數
  def transfer_credit(credit, type)
    exception_handle { 
      game_platform.transfer_credit(credit, type)
    }
  end

  # 遊戲結果
  def game_result(bet_form)
    exception_handle { 
      game_platform.game_result(bet_form)
    }
  end

  private

  def exception_handle
    begin
      yield
    rescue Exception => e
      Rails.logger.info "Caught #{e}."
      false
    end
  end
end
