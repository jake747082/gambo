require 'abstract_interface'
class GamePlatforms::Base::Interface
  include AbstractInterface

  # 取得遊戲歷程
  needs_implementation :betting_records
  # 取得餘額
  needs_implementation :get_balance
  # 進入遊戲
  needs_implementation :transfer_game
  # 建立會員
  needs_implementation :check_and_create_account
  # 轉點
  needs_implementation :transfer_credit
end