module GamePlatforms::Wm::BetFormHandler
  GAME_PLATFORM_ID = 13

  def bet_forms_format(result, game_type_id)
    bet_forms         = []
    user_prefix       = Setting.game_platform.wm.user_prefix
    user_lobbyId_base = Setting.beginning_lobby_id

    if result.present?
      result.map do |data|
        user_id = data["user"].gsub(user_prefix,"").to_i - user_lobbyId_base.to_i
        user    = User.find(user_id)

        if User.exists?(id:user_id) && !ThirdPartyBetForm.wm.exists?(vendor_id: data["betId"])
          default_bet_form = default_bet_form_params(user, data, game_type_id)
          # 計算代理分成
          final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff].to_f)
          bet_forms << ThirdPartyBetForm.new(final_params.merge!(order_number: final_params[:order_number]))
        end

      end.compact
    end

    unless bet_forms.empty?
      Rails.logger.info bet_forms
      # insert data
      ActiveRecord::Base.transaction do
        ThirdPartyBetForm.import bet_forms, on_duplicate_key_update: [:bet_total_credit, :reward_amount, :user_credit_diff, :valid_amount, :result]
      end
    end
  end

  private

  def default_bet_form_params(user, data, game_type_id)
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   data["bet"].to_f,
      reward_amount:      data["winLoss"].to_f + data["bet"].to_f,
      user_credit_diff:   data["winLoss"].to_f,
      game_type_id:       game_type_id,
      order_number:       "WM_#{data['betId']}",
      game_name_id:       data["gname"],
      # @TODO be better : 原北京時區切換至美東時區
      betting_at:         convert_timeZone_to_EST(data["betTime"].to_s),
      vendor_id:          data["betId"],
      valid_amount:       data["validbet"].to_f,
      round:              data["betId"],
      game_kind:          data["gname"],
      result:             (data["winLoss"].to_f) > 0 ? "W": "L",
      response:           data.to_json,
      from:               "wm",
    }
  
  end

  def convert_timeZone_to_EST(vendorTime)
		vendorTime_anchor = Time.zone.now.in_time_zone("Beijing").strftime("%Y-%m-%d %H:%M")
		estTime_anchor    = Time.zone.now.in_time_zone("America/New_York").strftime("%Y-%m-%d %H:%M")
		minute_diffence   = (DateTime.parse(estTime_anchor).to_time - DateTime.parse(vendorTime_anchor).to_time)/1.minutes
		estTime           = (DateTime.parse(vendorTime) + minute_diffence.minutes).strftime("%Y-%m-%d %H:%M:%S")
	end

end