module GamePlatforms::East::BetFormHandler

  GAME_PLATFORM_ID = 2

  # xml資料整理
  def bet_forms_format(xml, game_type_id)
    bet_forms = []
    invalid_bet_forms = {} # 需重新計算有效投注的注單
    if xml.xpath("//Data").present?
      xml.xpath("//Data").map do |data|
        # @todo refactor : remove loop query
        username = if Setting.game_platform.east.prefix.empty?
          data.xpath("properties[@name='UserName']").text
        else
          data.xpath("properties[@name='UserName']").text.split(Setting.game_platform.east.prefix)[1]
        end
        unless username.nil?
          user_id = User.lobby_id_to_id(username)
          user = User.find_by_id(user_id) if user_id > 0
          if !user.nil? && !ThirdPartyBetForm.east.exists?(vendor_id: data.xpath("properties[@name='VendorId']").text.to_i)
            default_bet_form = default_bet_form_params(user, data, game_type_id)
            # 計算代理分成
            final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
            # insert data
            bet_form = ThirdPartyBetForm.new(final_params.merge!(order_number: final_params[:order_number]))
            bet_forms << bet_form

            # 處理有效投注
            # 處理結果為輸贏，ResultType = 1 (輸) & ResultType = 2 (贏); 不處理結果為和
            if bet_form.result_type == 1 || bet_form.result_type == 2
              # 預設
              # 依照 TableID , Stage , Inning 相同為同一場
              invalid_bet_form_key = "#{bet_form.table_id}_#{bet_form.stage}_#{bet_form.inning}"
              invalid_bet_forms[invalid_bet_form_key] ||= {}
              invalid_bet_forms[invalid_bet_form_key]['bank'] ||= 0 # 庄
              invalid_bet_forms[invalid_bet_form_key]['play'] ||= 0 # 閒
              invalid_bet_forms[invalid_bet_form_key]['other'] ||= 0
              # 影響遊戲類型為
              #  GameNameID : 11 (百家樂) ,12 (龍虎)
              #  GameBettingKind : 
              #   百家樂: 101 (閒); 102 (庄)
              #   龍虎: 201 (龍); 202 (虎)
              if bet_form.game_betting_kind == 101 || bet_form.game_betting_kind == 201
                # 閒
                invalid_bet_forms[invalid_bet_form_key]['play'] += bet_form.bet_total_credit
              elsif bet_form.game_betting_kind == 102 || bet_form.game_betting_kind == 202
                # 庄
                invalid_bet_forms[invalid_bet_form_key]['bank'] += bet_form.bet_total_credit
              else
                invalid_bet_forms[invalid_bet_form_key]['other'] += bet_form.bet_total_credit
              end
              invalid_bet_forms[invalid_bet_form_key]['vendor_id'] = bet_form.vendor_id
            end
          end
        end
      end.compact

      # 重新整理bet_forms, 更新有效投注
      bet_forms.each do |bet_form|
        key = "#{bet_form.table_id}_#{bet_form.stage}_#{bet_form.inning}"
        if !invalid_bet_forms[key].nil? && invalid_bet_forms[key]['bank'] > 0 && invalid_bet_forms[key]['play'] > 0
          if invalid_bet_forms[key]['vendor_id'] == bet_form.vendor_id
            # 最後一筆紀錄實際有效投注
            bet_form.valid_amount = invalid_bet_forms[key]['other'] + (invalid_bet_forms[key]['bank'] - invalid_bet_forms[key]['play']).abs
          else
            # 有效投注為0
            bet_form.valid_amount = 0
          end
        end
      end
    end

    unless bet_forms.empty?
      # insert data
      ActiveRecord::Base.transaction do
        ThirdPartyBetForm.import bet_forms, on_duplicate_key_update: [:bet_total_credit, :reward_amount, :user_credit_diff, :valid_amount, :result]
      end
    end
  end
  
  private
  
  def default_bet_form_params(user, data, game_type_id)
    {
      user_id: user.id,
      game_platform_id: GAME_PLATFORM_ID,
      bet_total_credit: data.xpath("properties[@name='BettingAmount']").text,
      reward_amount: data.xpath("properties[@name='BettingAmount']").text.to_f + data.xpath("properties[@name='WinLoseAmount']").text.to_f,
      user_credit_diff: data.xpath("properties[@name='WinLoseAmount']").text.to_f,
      game_type_id: game_type_id,

      product_id: data.xpath("properties[@name='ProductID']").text,
      game_record_id: data.xpath("properties[@name='GameRecordID']").text,
      order_number: "east_" + data.xpath("properties[@name='OrderNumber']").text,
      table_id: data.xpath("properties[@name='TableID']").text,
      stage: data.xpath("properties[@name='Stage']").text,
      inning: data.xpath("properties[@name='Inning']").text,
      game_name_id: data.xpath("properties[@name='GameNameID']").text,
      game_betting_kind: data.xpath("properties[@name='GameBettingKind']").text,
      game_betting_content: data.xpath("properties[@name='GameBettingContent']").text,
      result_type: data.xpath("properties[@name='ResultType']").text,
      compensate_rate: data.xpath("properties[@name='CompensateRate']").text,
      balance: data.xpath("properties[@name='Balance']").text,
      # @TODO be better : 原北京時區切換至美東時區
      betting_at: (DateTime.strptime(data.xpath("properties[@name='AddTime']").text, '%Y/%m/%d %H:%M:%S') - 12.hours).strftime("%Y-%m-%d %H:%M:%S"),
      vendor_id: data.xpath("properties[@name='VendorId']").text,
      valid_amount: data.xpath("properties[@name='ValidAmount']").text,
      result: (data.xpath("properties[@name='WinLoseAmount']").text.to_f) > 0 ? 'W': 'L',
      game_kind: data.xpath("properties[@name='GameKind']").text,
      response:  data.to_s,
      from: 'east',
    }
  end

end
