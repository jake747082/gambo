module GamePlatforms::East::GameResultHandler

  # xml資料整理
  def game_result_format(xml, record_id, game_kind)
    data = xml.xpath("//Data[properties[@name='ProductID'][text() = '#{record_id}']]")
    if data.present?
      game_result_params(data, game_kind)
    else
      # @todo 看是否做通知處理
      Rails.logger.info "[East - Game Result Import] Someting Wrong. (result: #{xml})" unless xml.xpath("//result") == 'No_Data'
      false
    end
  end

  # note:
  # 闲1@闲2@闲3@庄1@庄2@庄3
  # 个位是花色,十位是点数
  # 花色(1方,2梅,3红,4黑桃)
  def game_result_params(data, game_kind)
    game_information = data.xpath("properties[@name='GameInformation']").text

    case game_kind
    when "111", "112", "113", "114", "115" #百家樂
      baccarat_data_format(game_information, game_kind)
    when "211" #龍虎
      dratir_data_format(game_information, game_kind)
    when "311"
      rotary_data_format(game_information, game_kind)
    when "411" #骰寶
      dice_data_format(game_information, game_kind)
    when "611" #番摊
      fantan_data_format(game_information, game_kind)
    end
  end

  def format_point(arr)
    arr.map do |value|
      value = value[0..value.length-2].to_i
      value = value >= 10 ? 0 : value
    end
  end

  def baccarat_data_format(game_information, game_kind)
    pokers = game_information.split("@")
    pokers.insert(5, "") if pokers.length == 5
    banker_pokers = [pokers[0], pokers[1], pokers[2]]
    player_pokers = [pokers[3], pokers[4], pokers[5]]

    banker_point = format_point(banker_pokers).reduce {|memo, num| memo + num}
    banker_point = banker_point >= 10 ? banker_point % 10 : banker_point
    player_point = format_point(player_pokers).reduce {|memo, num| memo + num}
    player_point = player_point >= 10 ? player_point % 10 : player_point

    {
      game_kind: game_kind,
      banker_pokers: banker_pokers,
      player_pokers: player_pokers,
      banker_point: banker_point,
      player_point: player_point
    }
  end

  def dratir_data_format(game_information, game_kind)
    pokers = game_information.split("@")
    dragon_poker = pokers[0]
    tiger_poker = pokers[1]
    dragon_point = dragon_poker[0..dragon_poker.length-2].to_i
    tiger_point = tiger_poker[0..tiger_poker.length-2].to_i

    {
      game_kind: game_kind,
      dragon_poker: dragon_poker,
      tiger_poker: tiger_poker,
      dragon_point: dragon_point,
      tiger_point: tiger_point
    }
  end

  def dice_data_format(game_information, game_kind)
    dices = game_information.split("")
    {
      game_kind: game_kind,
      dices: dices
    }
  end

  def fantan_data_format(game_information, game_kind)
    {
      game_kind: game_kind,
      point: game_information
    }
  end

  def rotary_data_format(game_information, game_kind)
    {
      game_kind: game_kind,
      point: game_information
    }
  end
end
