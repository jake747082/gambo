module GamePlatforms::Kgame::BetFormHandler

  GAME_PLATFORM_ID = 8

 def bet_forms_format(record)
    Rails.logger.info "===========GamePlatforms::Kgame::BetFormHandler========="
    if record.present?
      users = {}
      exist_bet_forms = {}
      bet_forms = record.map do |data|
        # @todo refactor : remove loop query
        username = data['username'].split(Setting.game_platform.kgame.prefix)[1]
        unless username.nil?
          user = users.key?(username) ? users[username] : User.find_by_username(username)
          users[username] = user
Rails.logger.info '---------------'
Rails.logger.info username
Rails.logger.info 'round id========='
Rails.logger.info data['round_id']
is_exist_bet_forms = exist_bet_forms.key?(data['round_id']) ? exist_bet_forms[data['round_id']] : ThirdPartyBetForm.kgame.exists?(round: data['round_id'])
Rails.logger.info '判斷========='
Rails.logger.info exist_bet_forms.key?(data['round_id'])
Rails.logger.info '已存在========='
Rails.logger.info exist_bet_forms[data['round_id']]
Rails.logger.info '找db========='
Rails.logger.info ThirdPartyBetForm.kgame.exists?(round: data['round_id'])
Rails.logger.info '結果========='
Rails.logger.info is_exist_bet_forms
Rails.logger.info '========='
          if user.present? && !is_exist_bet_forms
            exist_bet_forms[data['round_id']] = true
            default_bet_form = default_bet_form_params(user, data)
            # User.update_counters user, play_total: default_bet_form[:user_credit_diff]
            # 計算代理分成
            final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
            # insert data
            bet_form = ThirdPartyBetForm.find_or_initialize_by(round: final_params[:round])
            bet_form.assign_attributes(final_params)
            bet_form.save
          end
        end
      end.compact
    end
  end

  private

  def default_bet_form_params(user, data)
    # 2: slot, 1: 電玩棋盤
    if data['app_id'] == '568b7499a34294cafabd01e9e0532f24'
      game_type_id = 2
      prefix = 'SLOT'
    else
      game_type_id = 1
      prefix = 'CHESS'
    end
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   (data['bet'].to_f / 100).to_f,
      reward_amount:      (data['win'].to_f / 100).to_f,
      user_credit_diff:   (data['user_win'].to_f / 100).to_f,
      game_type_id:       game_type_id,

      order_number:       "KGAME_#{data['id']}",

      game_name_id:       data['app_id'],
      betting_at:         Time.zone.at(data['timestamp'].to_i),
      vendor_id:          "#{prefix}_#{data['id']}",
      valid_amount:       (data['miss'].to_f / 100).to_f,
      
      game_kind:          data['app_id'],
      round:              data['round_id'],
      result:             (data['win'].to_f) > 0 ? 'W' : 'L',
      response:           data.to_json,
      from:               'kgame',
    }
  end

end
