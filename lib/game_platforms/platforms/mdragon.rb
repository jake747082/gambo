class GamePlatforms::Mdragon
  attr_reader :username

  def initialize(user)
    @user = user

    if user.present?
      game_platform_user = user.user_game_platformships.mdragon
      @username = user.username

    end
  end

  def transfer_credit(credit, type)
    if type == "IN"
      # 儲值
      User::CreditControl.new(@user, credit.to_f).transfer_in!
    else
      # 扣款
      raise ApiError.new("error"), "餘額不足！！" if @user.credit_left < credit.to_f
      User::CreditControl.new(@user, credit.to_f).transfer_out!
    end
  rescue ApiError => e
    Rails.logger.info e.message
    false
  end

end
