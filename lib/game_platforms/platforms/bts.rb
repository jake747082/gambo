# BTS棋牌
class GamePlatforms::Bts < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  autoload :BetFormHandler, 'game_platforms/platforms/bts/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  attr_reader :url, :appkey, :platid, :prefix, :backurl
  attr_accessor :line

  def initialize(user = nil, remote_ip = nil)
    @url           ||= Setting.game_platform.bts.url
    @appkey        ||= Setting.game_platform.bts.appkey
    @platid        ||= Setting.game_platform.bts.platid
    @backurl       ||= Setting.game_platform.bts.backurl
    @cn_backurl    ||= Setting.game_platform.bts.cn_backurl
    @prefix        ||= Setting.game_platform.bts.prefix
    @agent        ||= Setting.game_platform.bts.agent
    @remote_ip       = remote_ip

    if user.present?
      game_platform_user = user.user_game_platformships.bts
      @username = "#{@prefix}#{user.lobby_id}"
      game_platform_user.update!(username: @username) if game_platform_user.username.blank?
      @password = password_encrypt(game_platform_user.password, @username)
      check_and_create_account
    end
  end  

  def check_and_create_account
    params = {
      account: @username,
      superiorAccount: @agent
    }
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/CheckExists', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      true
    else
      create_account
    end
  end

  def create_account
    params = {
      account: @username,
      password: @password,
      userip: @remote_ip,
      superiorAccount: @agent
    }
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/Create', request_params(params))
    !response['code'].nil? && response['code'].to_i == 200
  end

  def transfer_game
    params = {
      account: @username,
      password: @password,
      userip: @remote_ip,
      gameid: -1,
      backurl: line == 'cn' ? @cn_backurl : @backurl
    }
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/Login', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      response_data(response, 'gameurl')
    else
      false
    end
  end

  # type : IN / OUT
  # return true / false / 3003
  def transfer_credit(credit, type)
    Rails.logger.info credit
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    order_number = "#{time_now}#{Random.rand(1000)}"
    params = {
      account: @username,
      amount: type == 'IN' ? credit.abs : -credit.abs,
      orderNumber: order_number
    }
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/PerformTransfer', request_params(params))
    return false if response['code'].nil?
    
    Rails.logger.info response['code']
    if response['code'].to_i == 200
      confirm_transfer_credit(order_number)
    elsif response['code'].to_i == 511
      # 在游戏中暂时不可转出
      3003
    else
      false
    end
  end

  def get_balance
    params = {
      account: @username
    }
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/GetBalance', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      amount = response_data(response, 'amount')
      !amount ? 0 : amount.to_f
    else
      if response['code'].to_i == 503
        # 用戶不存在
        0
      else
        false
      end
    end
  end

  def games_list
    params = {}
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/GetGameInfos', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      response_data(response)
    else
      false
    end
  end
  
  def betting_records(page = 1, page_count = 1000)
    # 遊戲紀錄
    bts_last_bet_form = ThirdPartyBetForm.bts.first
    bts_updated_time = GamePlatform.find_by_name('bts').updated_at.in_time_zone('Beijing').strftime("%Y/%m/%d %H:%M:%S")
    bts_default_start_time = bts_updated_time.nil? ? '2020/04/06 00:00:00' : bts_updated_time
    date_from  ||= bts_last_bet_form.nil? ? bts_default_start_time : (bts_last_bet_form.betting_at).in_time_zone('Beijing').strftime("%Y/%m/%d %H:%M:%S")
    date_to = (Time.zone.now-1.seconds).in_time_zone('Beijing').strftime("%Y/%m/%d %H:%M:%S")

    current_page = page.to_i
    Rails.logger.info '-------------bts start-----------'
    loop do
      response = get_records(date_from, date_to, page, page_count)
      if !response['code'].nil? && response['code'].to_i == 200
        record = response_data(response)
        total_page = record['totalpage'].to_i
        bet_forms_format(record['RecordModels'], 6) # 牌桌 game_type_id : 6
      else
        break
      end
      current_page += 1
      break if total_page < current_page
    end
    Rails.logger.info '---------------bts end-----------'
  end

  # 踢出在線玩家
  def kick_out_user
    params = {
      account: @username
    }
    params_string = params.to_json
    response = post_request('/api/PANDAHandle/KickOutUser', request_params(params))
    !response['code'].nil? && response['code'].to_i == 200
  end

  private
  
  # type : 1 游戏记录 2 转账记录 3 扣房费 4 在线派彩金额
  def get_records(date_from, date_to, page = 1, page_count = 1000, type = 1)
    params = {
      starttime: date_from,
      endtime: date_to,
      curpage: page,
      perpage: page_count,
      Type: type,
      superiorAccount: @agent 
    }
    params_string = params.to_json
    response = post_request('/api/PANDARecord/Record', request_params(params))
  end

  def confirm_transfer_credit(order_number)
    params = {
      orderNumber: order_number
    }
    params_string = params.to_json

    response = post_request('/api/PANDAHandle/TransferConfirm', request_params(params))
    !response['code'].nil? && response['code'].to_i == 200
  end

  def request_params(params)
    params_string = params.to_json
    data = URI::encode(Base64.strict_encode64(params_string))
    sign = Digest::MD5.hexdigest(Base64.strict_encode64(params_string).downcase+appkey)
    {
      data: data,
      sign: sign,
      platid: @platid
    }
  end

  def post_request(path, params)
    begin
      uri              = URI.parse(url+path)
      http             = Net::HTTP.new(uri.host, uri.port)
      if uri.port == 443
        http.use_ssl     = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request          = Net::HTTP::Post.new(uri.request_uri)
      request.set_form_data(params)
      JSON.parse(http.request(request).body)
    rescue
      false
    end
  end

  def response_data(response, key = nil)
    unless response['data'].empty?
      begin
        if response['data'].is_a?(String)
          data = JSON.parse(response['data'])
        else
          data = response['data']
        end
      rescue JSON::ParserError
        false
      end
    end
    key.nil? ? data : data[key]
  end
  
  def password_encrypt(password, salt)
    Digest::SHA1.hexdigest("#{salt}-#{password}--")
  end


  # 每日压缩数据(現無使用)
  def daily_compression_records(date_from, date_to, page = 1, page_count = 1000)
    params = {
      # account: @username, #非必填
      starttime: date_from, #非必填
      endtime: date_to, #非必填
      curpage: page,
      perpage: page_count,
      superiorAccount: @agent
    }
    Rails.logger.info params
    params_string = params.to_json
    response = post_request('/api/PANDARecord/DailyCompressionRecord', request_params(params))
  end

  # 创建代理(現無使用)
  def create_agent(username, password)
    params = {
      agentAccount: username,
      password: password
    }
    Rails.logger.info params
    params_string = params.to_json
    response = post_request('/api/PANDAHandle/CreateAgent', request_params(params))
    !response['code'].nil? && response['code'].to_i == 200
  end

  # 获取下级代理信息(現無使用)
  def get_all_agent_info(agent_username, is_me = 0)
    params = {
      agentAccount: agent_username,
      isMe: is_me #是否查询的是自己(0 当前代理信息 1 所有下级)
    }
    Rails.logger.info params
    params_string = params.to_json
    response = post_request('/api/PANDAHandle/GetAllAgentInfo', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      response_data(response, 'AgentModels')
    else
      false
    end
  end

  # 积分账务查询(現無使用)
  def score_records(date_from, date_to, page = 1, page_count = 1000)
    params = {
      account: @username, #非必須
      starttime: date_from,
      endtime: date_to,
      curpage: page,
      perpage: page_count
    }
    Rails.logger.info params
    params_string = params.to_json
    response = post_request('/api/PANDARecord/ScoreRecord', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      response_data(response)
    else
      false
    end
  end

  # 积分账务详情查询(現無使用)
  def score_detail_record(game_num)
    params = {
      account: @username, #非必須
      gameNum: game_num
    }
    Rails.logger.info params
    params_string = params.to_json
    response = post_request('/api/PANDARecord/ScoreDetailRecord', request_params(params))
    if !response['code'].nil? && response['code'].to_i == 200
      response_data(response)
    else
      false
    end
  end
end
