class GamePlatforms::Mg < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  # 處理遊戲歷程
  autoload :BetFormHandler, 'game_platforms/platforms/mg/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  attr_reader :getdata_url, :dobusiness_url, :website, :agent, :agent_password, :prefix
  attr_reader :username, :password

  def initialize(user)
    @getdata_url    = Setting.game_platform.mg.getdata_url
    @dobusiness_url = Setting.game_platform.mg.dobusiness_url
    @website        = "http://#{Setting.game_platform.domain}/mg_electronic_games/retry"
    @agent          = Setting.game_platform.mg.agent
    @agent_password = Setting.game_platform.mg.agent_password
    @prefix         = Setting.game_platform.mg.prefix

    authenticate = is_authenticate

    @header ||= {
      "AgentSession" => {
        "SessionGUID" => authenticate[:session_guid],
        "ErrorCode" => authenticate[:error_code],
        "IPAddress" => authenticate[:ip_address],
        "IsExtendSession" => "true"
      }
    }

    if user.present?
      game_platform_user = user.user_game_platformships.mg
      @username = "#{@prefix}#{user.username}"
      @password = game_platform_user.password
      # 檢查帳號是否存在對方平台, 若無則建立帳號
      check_and_create_account if is_account_available?
    end
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'mg/config.yml')
    YAML::load(File.open(abs_path))
  end

  # 取得餘額
  def get_balance
    response = send_request(getdata_url, :get_account_balance, delimitedAccountNumbers: username)
    response.body[:get_account_balance_response][:get_account_balance_result][:balance_result][:balance].to_f
  end

  # 轉點數
  def transfer_credit(credit, type)
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    billno = "#{time_now}#{Random.rand(1000)}"
    transaction_id = if type == 'IN'
      deposit_credit(credit, billno)
    else
      withdrawal_credit(credit, billno)
    end
    if transaction_id != 0
      status = get_transaction_detail(transaction_id)
    else
      false
    end
  end

  def transfer_single_game_url(game_id)
    "https://mobile22.gameassists.co.uk/mobilewebservices/casino/game/launch/GoldenTree/#{game_id}/en/?lobbyURL=#{website}&bankingURL=#{website}&username=#{username}&password=#{password}&currencyFormat=%23%2C%23%23%23.%23%23&logintype=fullUPE&xmanEndPoints=https://xplay22.gameassists.co.uk/xman/x.x"
  end

  # 投注紀錄
  def betting_records
    private_resource = RestClient::Resource.new("#{dobusiness_url}/GetSpinBySpinData", agent, agent_password)
    response = private_resource.post(LastRowId: ThirdPartyBetForm.last.nil? ? '29070534480' : ThirdPartyBetForm.last.row_id[3..-1])
    bet_form_format(JSON.parse(response.body)["Result"])
  end

  private

  # 確認轉帳狀態
  def get_transaction_detail(transaction_id)
    response = send_request(getdata_url, :get_transaction_detail, transactionId: transaction_id)
    response.body[:get_transaction_detail_response][:get_transaction_detail_result][:is_succeed]
  end

  # 儲值
  def deposit_credit(credit, billno)
    params = {
      accountNumber: username,
      amount: credit,
      currency: 8,
      transactionReferenceNumber: billno
    }
    response = send_request(getdata_url, :deposit, params)
    response.body[:deposit_response][:deposit_result][:transaction_id]
  end

  # 取款
  def withdrawal_credit(credit, billno)
    params = {
      accountNumber: username,
      amount: credit,
      transactionReferenceNumber: billno
    }
    response = send_request(getdata_url, :withdrawal, params)
    response.body[:withdrawal_response][:withdrawal_result][:transaction_id]
  end

  def is_account_available?
    response = send_request(getdata_url, :is_account_available, accountNumber: username)
    result = response.body[:is_account_available_response][:is_account_available_result]
    if result[:is_succeed]
      result[:is_account_available]
    else
      false
    end
  end

  def check_and_create_account
    params = {
      accountNumber: username,
      firstName: username,
      lastName: username,
      password: password,
      currency: 8,
      BettingProfileId: 178,
      isSendGame: true,
      email: 'test@gmail.com',
      moblieGameLanguageId: 3,
      isProgressive: true
    }
    response = send_request(getdata_url, :add_account, params)
    response.body[:add_account_response][:add_account_result][:is_succeed]
  end

  # Agent 認證
  def is_authenticate
    params = {
      loginName: agent,
      pinCode: agent_password
    }
    response = request_url(getdata_url, :is_authenticate, params)
    response.body[:is_authenticate_response][:is_authenticate_result]
  end

  def request_url(url, method_name, args, header = {})
    client = Savon.client(wsdl: url, soap_header: header, namespaces: {xmlns: "https://entservices.totalegame.net"})
    client.call(method_name, message: args)
  end

  def send_request(url, method_name, args = {})
    request_url(url, method_name, args, @header)
  end
end
