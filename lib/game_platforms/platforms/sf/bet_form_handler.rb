module GamePlatforms::Sf::BetFormHandler
  GAME_PLATFORM_ID = 10

  # xml資料整理
  def bet_forms_format(data)
    username = data[:AccountID]
    unless username.nil?
      user = User.find_by_username(username)
      if user.present? && !ThirdPartyBetForm.sf.exists?(vendor_id: "SF_#{data[:TransactionID]}")
        default_bet_form = default_bet_form_params(user, data)
        User.update_counters user, play_total: default_bet_form[:user_credit_diff]
        # 計算代理分成
        final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
        # insert data
        ThirdPartyBetForm.create(final_params)
        return true
      end
    end
    false
  end

  private

  def default_bet_form_params(user, data)
    {
      user_id: user.id,
      game_platform_id: GAME_PLATFORM_ID,
      bet_total_credit: data[:BetAmount].to_f,
      reward_amount: data[:WinAmount].to_f,
      user_credit_diff: data[:WinAmount].to_f - data[:BetAmount].to_f,
      game_type_id: 2,

      order_number: "SF_#{data[:TransactionID]}",

      game_name_id: data[:GameName],
      game_kind: data[:GameName],
      betting_at: Time.at(data[:Timestamp].to_i / 1000.0).strftime("%Y-%m-%d %H:%M:%S"),
      vendor_id:          "SF_#{data[:TransactionID]}",
      valid_amount:       data[:BetAmount].to_f,
      
      round:              data[:RoundID],
      result:             data[:WinAmount].to_f > 0 ? 'W' : 'L',
      ip:                 data[:PlayerIP] || nil,
      response:           data.to_json,
      from:               'sf',
    }
  end

end
