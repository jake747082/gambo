module GamePlatforms::Bbin::BetFormHandler
  GAME_PLATFORM_ID = 3

  # xml資料整理
  def bet_forms_format(data)
    if data.present?
      bet_forms = data.map do |bet_form|
        # @todo refactor : remove loop query
        user = User.find_by_username(bet_form['UserName'][Setting.game_platform.bbin.prefix.size..-1])
        if user.present? && !ThirdPartyBetForm.bbin.exists?(vendor_id: bet_form['WagersID'].to_i)
          default_bet_form = default_bet_form_params(user, bet_form)
          User.update_counters user, play_total: default_bet_form[:user_credit_diff]
          # 計算代理分成
          final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
          ThirdPartyBetForm.new(final_params)
        end
      end.compact
    else
      # @todo 看是否做通知處理
      Rails.logger.info "[BBIN - Histories Import] Someting Wrong. (result: #{data})" unless data.blank?
    end
  end

  private

  def default_bet_form_params(user, data)
    {
      user_id: user.id,
      game_platform_id: GAME_PLATFORM_ID,
      bet_total_credit: data['BetAmount'],
      reward_amount: data['Payoff'].to_f + data['BetAmount'].to_f,
      user_credit_diff: data['Payoff'].to_f,

      game_record_id: data['RoundNo'],
      order_number: "bbin_" + data['WagersID'],
      table_id: data['GameCode'],
      stage: data['SerialID'],
      game_name_id: data['GameType'],
      game_betting_content: data['WagerDetail'],
      result_type: data['ResultType'],
      betting_at: Time.zone.parse(data['WagersDate']).to_datetime + 12.hours,
      valid_amount: data['Commissionable'],
      game_kind: data['GameType'],
      result: data['Result'],
      card: data['Card'],
    }
  end

end
