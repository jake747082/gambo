module GamePlatforms::Bbin::GameResultHandler

  def game_result_format(bet_form)
    game_kind = bet_form[:game_kind]

    case game_kind
    when "3001" #百家樂
      baccarat_data_format(bet_form, game_kind)
    when "3003" #龍虎
      dratir_data_format(bet_form, game_kind)
    when "3007" #轮盘
      rotary_data_format(bet_form, game_kind)
    when "3008" #骰寶
      dice_data_format(bet_form, game_kind)
    when "3015" #番摊
      fantan_data_format(bet_form, game_kind)
    when "3014" #21點
      unlimited_twenty_one(bet_form, game_kind)
    when "3006" #温州牌九
      wenzhou_pai_gow(bet_form, game_kind)
    when "3011" #色碟
      color_singles(bet_form, game_kind)
    when "3005", "3012" #三公, 牛牛
      excellencies(bet_form, game_kind)
    when "3010" #德州扑克
      texas_hold(bet_form, game_kind)
    when "3002" #二八杠
      mahjong(bet_form, game_kind)
    end
  end

  def mahjong(bet_form, game_kind)
    game_card = bet_form[:card]
    mahjongs = game_card.split("*")

    banker_mahjongs  = mahjongs[0].split(",").map { |m| m.split(".")[1] }
    up_mahjongs      = mahjongs[1].split(",").map { |m| m.split(".")[1] }
    mid_mahjongs     = mahjongs[2].split(",").map { |m| m.split(".")[1] }
    bottom_mahjongs  = mahjongs[3].split(",").map { |m| m.split(".")[1] }

    banker_point = bet_form[:result].split(",")[0]
    up_point     = bet_form[:result].split(",")[1]
    mid_point    = bet_form[:result].split(",")[2]
    bottom_point = bet_form[:result].split(",")[3]

    {
      game_kind: game_kind,
      banker_mahjongs: banker_mahjongs,
      up_mahjongs: up_mahjongs,
      mid_mahjongs: mid_mahjongs,
      bottom_mahjongs: bottom_mahjongs,
      banker_point: banker_point,
      mid_point: mid_point,
      up_point: up_point,
      bottom_point: bottom_point
    }
  end

  def texas_hold(bet_form, game_kind)
    game_card = bet_form[:card]
    game_result = bet_form[:result][0..bet_form[:result].split("").index("(")-1]
    pokers = game_card.split("*")

    banker_pokers = pokers[0].split(",").map { |poker| format_porker_name(poker) }
    player_pokers = pokers[1].split(",").map { |poker| format_porker_name(poker) }
    open_pokers   = pokers[2].split(",").map { |poker| format_porker_name(poker) }
    best_pokers   = game_result.split(",").map { |poker| format_porker_name(poker) }

    {
      game_kind: game_kind,
      banker_pokers: banker_pokers,
      player_pokers: player_pokers,
      open_pokers: open_pokers,
      best_pokers: best_pokers
    }
  end

  def excellencies(bet_form, game_kind)
    game_card = bet_form[:card]
    pokers = game_card.split("*")

    banker_pokers = pokers[0].split(",").map { |poker| format_porker_name(poker) }
    p1_pokers     = pokers[1].split(",").map { |poker| format_porker_name(poker) }
    p2_pokers     = pokers[2].split(",").map { |poker| format_porker_name(poker) }
    p3_pokers     = pokers[3].split(",").map { |poker| format_porker_name(poker) }

    banker_point = bet_form[:result].split(",")[0]
    p1_point = bet_form[:result].split(",")[1]
    p2_point = bet_form[:result].split(",")[2]
    p3_point = bet_form[:result].split(",")[3]

    {
      game_kind: game_kind,
      banker_pokers: banker_pokers,
      p1_pokers: p1_pokers,
      p2_pokers: p2_pokers,
      p3_pokers: p3_pokers,
      banker_point: banker_point,
      p1_point: p1_point,
      p2_point: p2_point,
      p3_point: p3_point
    }
  end

  def color_singles(bet_form, game_kind)
    game_result = bet_form[:result]
    {
      game_kind: game_kind,
      result: game_result.split(" ").map { |e| e.downcase }
    }
  end

  def wenzhou_pai_gow(bet_form, game_kind)
    game_card = bet_form[:card]
    pokers = game_card.split("*")

    banker_pokers = pokers[0].split(",").map { |poker| format_porker_name(poker) }
    alone_pokers  = pokers[1].split(",").map { |poker| format_porker_name(poker) }
    out_pokers    = pokers[2].split(",").map { |poker| format_porker_name(poker) }
    to_pokers     = pokers[3].split(",").map { |poker| format_porker_name(poker) }

    players_result = bet_form[:result].split(",").map do |e|
      e = (e.split("").include? "W") ? "2" : "1"
    end

    {
      game_kind: game_kind,
      banker_pokers: banker_pokers,
      alone_pokers: alone_pokers,
      out_pokers: out_pokers,
      to_pokers: to_pokers,
      players_result: players_result
    }
  end

  def unlimited_twenty_one(bet_form, game_kind)
    game_result = bet_form[:result]
    banker_point = game_result.split(",")[0]
    player_point = game_result.split(",")[1]
    {
      game_kind: game_kind,
      banker_point: banker_point,
      player_point: player_point
    }
  end


  # note:
  # "C.10,D.9*D.11,C.4" 庄门牌*閒家牌
  # S(Spade),H(Heart),C(Club),D(Diamond)

  def baccarat_data_format(bet_form, game_kind)
    game_card = bet_form[:card]
    pokers = game_card.split("*")

    banker_pokers = pokers[0].split(",").map { |poker| format_porker_name(poker) }
    player_pokers = pokers[1].split(",").map { |poker| format_porker_name(poker) }

    banker_point = bet_form[:result].split(",")[0]
    player_point = bet_form[:result].split(",")[1]

    {
      game_kind: game_kind,
      banker_pokers: banker_pokers,
      player_pokers: player_pokers,
      banker_point: banker_point,
      player_point: player_point
    }
  end

  def format_porker_name(poker)
    # 花色(1方,2梅,3红,4黑桃)
    rule_before = ["D", "C", "H", "S"]
    rule_after = ["1", "2", "3", "4"]
    color = poker.split(".")[0]
    color = rule_after[rule_before.index(color)]
    number = poker.split(".")[1]
    number.to_s + color #131 方塊K
  end

  def dratir_data_format(bet_form, game_kind)
    game_card = bet_form[:card]
    game_result = bet_form[:result]
    pokers = game_card.split("*")
    points = game_result.split(",")

    dragon_poker = format_porker_name(pokers[0])
    tiger_poker = format_porker_name(pokers[1])
    dragon_point = points[0].to_i
    tiger_point = points[1].to_i

    {
      game_kind: game_kind,
      dragon_poker: dragon_poker,
      tiger_poker: tiger_poker,
      dragon_point: dragon_point,
      tiger_point: tiger_point
    }
  end

  def dice_data_format(bet_form, game_kind)
    game_result = bet_form[:result]
    dices = game_result.split(",")
    {
      game_kind: game_kind,
      dices: dices
    }
  end

  def fantan_data_format(bet_form, game_kind)
    game_result = bet_form[:result]
    {
      game_kind: game_kind,
      point: game_result
    }
  end

  def rotary_data_format(bet_form, game_kind)
    game_result = bet_form[:result]
    {
      game_kind: game_kind,
      point: game_result
    }
  end
end
