class GamePlatforms::Ag < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler
  require 'net/ftp'
  # 處理遊戲歷程
  autoload :BetFormHandler, 'game_platforms/platforms/ag/bet_form_handler'

  [ BetFormHandler ].each { |m| include m }

  attr_reader :dobusiness_url, :forward_game_url, :md5_key, :des_key, :cagent
  attr_reader :username, :password
  attr_accessor :game_type

  def initialize(user = nil)
    @dobusiness_url   ||= Setting.game_platform.ag.do_business_url
    @forward_game_url ||= Setting.game_platform.ag.forward_game_url
    @md5_key          ||= Setting.game_platform.ag.md5_key
    @des_key          ||= Setting.game_platform.ag.des_key
    @cagent           ||= Setting.game_platform.ag.cagent

    if user.present?
      # actype: 1 => 正式帳號 , 0 => 測試帳號
      # @actype = 1
      @actype = 0
      game_platform_user = user.user_game_platformships.ag
      @username = "#{@prefix}ka_#{user.username}"
      @password = game_platform_user.password
    else
      @actype = 0
      @username = "demo_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{SecureRandom.hex(3)}"
      @password = SecureRandom.hex(6)
    end
    # 檢查帳號是否存在對方平台, 若無則建立帳號
    check_and_create_account
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'ag/config.yml')
    YAML::load(File.open(abs_path))
  end

  def betting_records
    # @todo 抓出ftp第一個檔案時間?
    get_bet_forms
    # get_bet_forms("20170519".to_datetime)
  end

  # 只讀取xml檔案
  def lost_betting_records(lost_folder_path = nil)
    get_lost_bet_forms(lost_folder_path)
  end

  def check_and_create_account
    params = {
      method: 'lg',
      loginname: username,
      password: password,
      actype: @actype,
      oddtype: "A",
      cur: "CNY"
    }
    xml = send_request(request_url(dobusiness_url, params))
    xml.xpath("//@info").text == '0' ? true : false
  end

  def get_balance
    params = {
      method: 'gb',
      loginname: username,
      password: password,
      actype: @actype,
      cur: "CNY"
    }

    xml = send_request(request_url(dobusiness_url, params))
Rails.logger.info xml
    value = xml.xpath("//@info").text
    xml.xpath("//@msg").text == "" ? value.to_f : false
  end

  def transfer_credit(credit, type)
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    billno = "#{time_now}#{Random.rand(1000)}"
    params = {
      method: 'tc',
      loginname: username,
      password: password,
      billno: billno,
      credit: credit,
      type: type,
      actype: @actype,
      cur: "CNY"
    }
    xml = send_request(request_url(dobusiness_url, params))
    transfer_credit_confirm(billno, credit, type) if  xml.xpath("//@info").text == '0'
  end

  def query_order_status(billno)
    params = {
      method: 'qos',
      billno: billno,
      actype: @actype,
      cur: "CNY"
    }

    xml = send_request(request_url(dobusiness_url, params))
    xml.xpath("//@info").text == '0' ? true : false
  end

  def transfer_game
    @game_type ||= '0'
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    sid = "#{@cagent}#{time_now}#{Random.rand(10)}"
    params = {
      loginname: username,
      password: password,
      dm: Setting.game_platform.domain,
      sid: sid,
      lang: "1",
      actype: @actype,
      gameType: @game_type,
      oddtype: "A",
      session_token: DateTime.now.strftime('%Q'),
      cur: "CNY"
    }

    request_url(forward_game_url, params)
  end

  def transfer_credit_confirm(billno, credit, type)
    params = {
      method: 'tcc',
      loginname: username,
      password: password,
      billno: billno,
      credit: credit,
      type: type,
      actype: @actype,
      flag: "1",
      cur: "CNY"
    }
    xml = send_request(request_url(dobusiness_url, params))
    xml.xpath("//@info").text == '0' ? true : false
  end

  def send_request(url)
    response = Net::HTTP.get_response(URI.parse(url))
    @code = response.code
    # response.body
    # "<?xml version=\"1.0\" encoding=\"utf-8\"?><result info=\"0\" msg=\"\"/>"
    Nokogiri::XML(response.body)
  end

  def request_url(url, args = {})
    args = args.merge({cagent: cagent}).map do |field, value|
      "#{field}=#{value}"
    end.join('/\\\\/')
    params = encode(args)
    key = Digest::MD5.hexdigest(params + md5_key)
    url = "#{url}?params=#{params}&key=#{key}"
  end

  def encode(str)
    cipher = ::OpenSSL::Cipher.new('des-ede3')
    cipher.encrypt
    if @des_key.length < cipher.key_len
      @des_key = @des_key.ljust(cipher.key_len, "\0")
    end
    cipher.key = @des_key

    cipher.padding = 1
    update_value = cipher.update(str)
    up_final = update_value + cipher.final

    Base64.encode64(up_final).gsub(/\n/, "")
  end
end
