# AG (單錢包)
class GamePlatforms::AgSw < GamePlatforms::Base::Interface

  attr_reader :dobusiness_url, :forward_game_url, :md5_key, :des_key, :cagent, :prefix
  attr_reader :username, :password
  attr_accessor :game_type, :lang

  def initialize(user = nil)
    @dobusiness_url   ||= Setting.game_platform.ag_sw.do_business_url
    @forward_game_url ||= Setting.game_platform.ag_sw.forward_game_url
    @uatapi_url       ||= Setting.game_platform.ag_sw.uatapi_url
    @md5_key     ||= Setting.game_platform.ag_sw.md5_key
    @des_key     ||= Setting.game_platform.ag_sw.des_key
    @cagent      ||= Setting.game_platform.ag_sw.cagent
    @productid   ||= Setting.game_platform.ag_sw.productid
    @prefix      ||= Setting.game_platform.ag_sw.prefix

    if user.present?
      @actype = 1
      @game_platform_user = user.user_game_platformships.ag_sw
      @username = "#{@prefix}#{user.lobby_id}"
      @game_platform_user.update!(username: @username) if @game_platform_user.username.blank?
      @password = password_encrypt(@game_platform_user.password, @username)
      @user = user
      # 檢查帳號是否存在對方平台, 若無則建立帳號
      check_and_create_account
    end
  end

  def check_and_create_account
    params = {
      loginname: @username,
      method: 'lg',
      actype: @actype,
      password: @password,
      cur: "CNY"
    }
    xml = send_request(request_url(@dobusiness_url, params))
    xml.xpath("//@info").text == '0' ? true : false
  end
  
  def transfer_game
    create_game_session
    
    @game_type ||= '0'
    @lang ||= 'zh-cn'
    lang_id = case @lang
    when 'zh-tw'
      2
    when 'en'
      3
    else
      1
    end
    time_now = Time.zone.now.strftime("%y%m%d%H%M%S")
    sid = "#{cagent}#{time_now}"
    params = {
      loginname: @username,
      password: @password,
      dm: Setting.game_platform.domain,
      sid: sid,
      actype: @actype,
      lang: lang_id,
      gameType: @game_type,
      cur: "CNY"
    }
    request_url(@forward_game_url, params)
  end

  # 查詢注單的下注情況(視讯游戏)
  # return object
  # example:
  # {
  #  "15083110589175" => "6",
  #  "15083110589176" => "6"
  # }
  def check_ticket_status(transaction_id)
    xml = send_request("#{@uatapi_url}/resource/CheckTicketStatus.ucs?transactionID=#{transaction_id}")
    result = {}
    xml.xpath("//row").each_with_index do |row, key|
      result[xml.xpath('//row')[key]['billNo']] = xml.xpath('//row')[key]['status']
    end
    result
  end
  
  # @NOTE: 尚無使用
  # 查詢注單的下注情況(電子游戏)
  # 
  # return object
  # example:
  # {
  #  "15083110589175" => {
  #    :status => "Success",
  #    :type => "Withdraw"
  #  }
  # }
  def check_ticket_status_by_egame(transaction_id)
    xml = send_request("#{@uatapi_url}/resource/CheckTicketStatus.ucs?transactionID=#{transaction_id}&type=EGame")
    result = {}
    xml.xpath("//row").each_with_index do |row, key|
      result[xml.xpath('//row')[key]['billNo']] = {
        status: xml.xpath('//row')[key]['status'],
        type:  xml.xpath('//row')[key]['type'],
      }
    end
    result
  end

  # @NOTE: 尚無使用
  # 查询注单的派彩状况(視讯游戏)
  # return object
  # example:
  # {
  #  "#{transactionID}" => {
  #     :currency => "CNY",
  #     :netAmount => "20",
  #     :validBetAmount => "20",
  #     ...
  # }
  def check_orders_status(bill_no)
    xml = send_request("#{@uatapi_url}/resource/CheckOrdersStatus.ucs?billNo=#{transaction_id}")
    result = {}
    xml.xpath("//Record").each_with_index do |record, key|
      result[record.xpath("//transactionID")[key].text] = {
        currency: record.xpath("//currency")[key].text,
        netAmount:  record.xpath("//netAmount")[key].text,
        validBetAmount:  record.xpath("//validBetAmount")[key].text,
        playname:  record.xpath("//playname")[key].text,
        agentCode:  record.xpath("//agentCode")[key].text,
        settletime:  record.xpath("//settletime")[key].text,
        transactionID:  record.xpath("//transactionID")[key].text,
        billNo:  record.xpath("//billNo")[key].text,
        gametype:  record.xpath("//gametype")[key].text,
        playtype:  record.xpath("//playtype")[key].text,
        gameCode:  record.xpath("//gameCode")[key].text,
        val:  record.xpath("//val")[key].nil? ? '' : record.xpath("//val")[key].text,
        transactionType:  record.xpath("//transactionType")[key].text,
        gameResult:  record.xpath("//gameResult")[key].text,
      }
    end
    result
  end

  # @NOTE: 尚無使用
  # 查询注单的派彩状况(電子游戏)
  # return object
  # example:
  # {
  #  "#{transactionID}" => {
  #     :playname => "RNDtest2",
  #     :transactionType => "WITHDRAW",
  #     :transactionID => "000101021318515400022",
  #     ...
  # }
  def check_orders_status_by_egame(bill_no)
    xml = send_request("#{@uatapi_url}/resource/CheckOrdersStatus.ucs?billNo=#{transaction_id}&type=EGame")
    result = {}
    xml.xpath("//Record").each_with_index do |record, key|
      result[record.xpath("//transactionID")[key].text] = {
        playname: record.xpath("//playname")[key].text,
        transactionType: record.xpath("//transactionType")[key].text,
        transactionID:  record.xpath("//transactionID")[key].text,
        currency:  record.xpath("//currency")[key].text,
        amount:  record.xpath("//amount")[key].text,
        gameId:  record.xpath("//gameId")[key].text,
        roundId:  record.xpath("//roundId")[key].text,
        time:  record.xpath("//time")[key].text,
        remark:  record.xpath("//remark")[key].text
      }
    end
    result
  end

  # @NOTE: 尚無使用
  # 查询活动的详情
  # return object
  # example:
  # {
  #  "EV0001" => {
  #    :displayname => "LED Dealer Tips",
  #    :description => "Tips for LED Dealer",
  #    :remark_format => "[VID];[loginname];[Dealer Name];"
  #  }
  # }
  def check_event_info(event_id = '')
    xml = send_request("#{@uatapi_url}/resource/CheckEventInfo.ucs?productid=#{@productid}&eventid=#{event_id}")
    result = {}
    xml.xpath("//event").each_with_index do |event, key|
      result[xml.xpath('//event')[key]['id']] = {
        displayname: event.xpath("//displayname")[key].text,
        description: event.xpath("//description")[key].text,
        remark_format: event.xpath("//remark_format")[key].text
      }
    end
    result
  end

  private

  def create_game_session
    if @game_platform_user.update!(session_id: @game_platform_user.generate_session_id)
      params = {
        productid: @productid,
        username: @username,
        session_token: @game_platform_user.session_id,
        credit: @user.credit_left
      }
      params = params.map do |field, value|
        "#{field}=#{value}"
      end.join('&')
      xml = send_request("#{@uatapi_url}/resource/player-tickets.ucs?#{params}")
      xml.xpath("//ResponseCode").text == 'OK' ? true : false
    else
      false
    end
  end

  def password_encrypt(password, salt)
    Digest::SHA1.hexdigest("#{salt}-#{password}$$").slice(0, 20)
  end

  def send_request(url)
    response = Net::HTTP.get_response(URI.parse(url))
    @code = response.code
    # response.body
    # "<?xml version=\"1.0\" encoding=\"utf-8\"?><result info=\"0\" msg=\"\"/>"
    Nokogiri::XML(response.body)
  end

  def request_url(url, args = {})
    args = args.merge({cagent: @cagent}).map do |field, value|
      "#{field}=#{value}"
    end.join('/\\\\/')
    params = encode(args)
    key = Digest::MD5.hexdigest(params + md5_key)
    url = "#{url}?params=#{params}&key=#{key}"
  end

  def encode(str)
    cipher = ::OpenSSL::Cipher.new('des-ede3')
    cipher.encrypt
    if @des_key.length < cipher.key_len
      @des_key = @des_key.ljust(cipher.key_len, "\0")
    end
    cipher.key = @des_key

    cipher.padding = 1
    update_value = cipher.update(str)
    up_final = update_value + cipher.final

    Base64.encode64(up_final).gsub(/\n/, "")
  end
end