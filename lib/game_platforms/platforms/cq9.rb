class GamePlatforms::Cq9 < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  autoload :BetFormHandler, 'game_platforms/platforms/cq9/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  attr_reader :url, :token, :prefix
  attr_reader :username, :password, :nickname
  attr_accessor :gamecode, :lang

  def initialize(user = nil)
    @url      ||= Setting.game_platform.cq9.url
    @token   ||= Setting.game_platform.cq9.token
    @prefix   ||= Setting.game_platform.cq9.prefix

    p "cq9 prefix:"+Setting.game_platform.cq9.prefix
    if user.present?
      game_platform_user = user.user_game_platformships.cq9
      @username = "#{@prefix}#{user.username}"
      @nickname = user.name
      game_platform_user.update!(username: @username) if game_platform_user.username.blank?
      @password = game_platform_user.password
      # 檢查帳號是否存在對方平台, 若無則建立帳號
      check_and_create_account
    else
      # @TODO 測試帳號
      false
    end
  end

  # 取得設定
  def config
    config = {
      'slots' => {
        'category' => {
          2 => 'slot',
        },
        'games' => {
          2 => games_list(),
        }
      }
    }
  end

  def check_and_create_account
    params = {
      account: username,
      password: password,
      nickname: nickname
    }
    p params
    data = send_post_request('/gameboy/player', params)
    p data
    data['status']['code'] == '0' ? true : false
  end

  def get_balance
    begin
      data = send_get_request('/gameboy/player/balance/'+username)
      p data
      if data['status']['code'] == '0'
        data["data"]["balance"].to_f.round(2)
      else
        0
      end
    rescue Exception => e
      p e.message
      0
    end
  end

  def transfer_credit(credit, type)
    path = (type == 'IN') ? "/gameboy/player/deposit" : "/gameboy/player/withdraw"
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    billno = "#{time_now}#{Random.rand(1000)}"
    params = {
      account: username,
      amount: credit,
      mtcode: billno,
    }
    data = send_post_request(path, params)
    p data
    data['status']['code'] == '0' ? true : false
  end

  def transfer_game
    @gamecode ||= '1'; # 鑽石水果王
    @lang ||= 'en';
    begin
      # 登入
      params = {
        account: username,
        password: password,
      }
      p params
      data = send_post_request('/gameboy/player/login', params)
      p data
      if (data['status']['code'] == '0')
        # 取得遊戲連結
        game_params = {
          usertoken: data['data']['usertoken'],
          gamehall: 'cq9',
          gamecode: @gamecode,
          gameplat: 'web', # or mobile
          lang: @lang.downcase
        }
        game_data = send_post_request('/gameboy/player/gamelink', game_params)
        game_data['data']['url']
      else
        {}
      end
    rescue Exception => e
      p e.message
      {}
    end
  end

  def betting_records
    Rails.logger.info "======GamePlatforms::CQ9 - #{@prefix}======"
    last_updated_at = GamePlatform.cq9.updated_at
    date_from  = ThirdPartyBetForm.cq9.first.nil? ? (Time.zone.now.in_time_zone("Caracas") - 1.days).strftime("%Y-%m-%dT%H:%M:%S-04:00") : (last_updated_at + 1.seconds).in_time_zone("Caracas").strftime("%Y-%m-%dT%H:%M:%S-04:00")
    data_to = Time.zone.now.in_time_zone("Caracas").strftime("%Y-%m-%dT%H:%M:%S-04:00")
    begin
      p '/gameboy/order/view?starttime='+date_from+'&endtime='+data_to+'&page=1&account=&pagesize=20000'
      data = send_get_request('/gameboy/order/view?starttime='+date_from+'&endtime='+data_to+'&page=1&account=&pagesize=20000')
      p data
      if (data['status']['code'] == '0')
        bet_forms_format(data['data']['Data'])
      else
        {}
      end
    rescue Exception => e
      p e.message
      {}
    end
  end

  def games_list
    begin
      data = send_get_request('/gameboy/game/list/cq9')
      p data
      if (data['status']['code'] == '0')
        games = {}
        data['data'].each{|game| games[game['gamename']] = game['gamecode']}
        games
      else
        {}
      end
    rescue Exception => e
      p e.message
      {}
    end
  end

  def send_post_request(path, params)
    begin
      uri = URI.parse(url + "/" + path)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri, {"Authorization" => token, "Content-Type" => "application/x-www-form-urlencoded"})

      request.set_form_data(params)
      response = http.request(request)
      JSON.parse(response.body)
    rescue Exception => e
      p e.message
      {}
    end
  end

  def send_get_request(path)
    begin
      uri = URI.parse(url + "/" + path)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      request.initialize_http_header({"Authorization" => token})

      response = http.request(request)
      JSON.parse(response.body)
    rescue Exception => e
      {}
    end
  end

end