module GamePlatforms::Ag::BetFormHandler

  GAME_PLATFORM_ID = 4
  attr_accessor :bet_forms, :target_date, :xml_file_path

  def get_bet_forms(date = nil)
    self.bet_forms = {}
    last_betform      = ThirdPartyBetForm.from_ag.first
    today             = DateTime.now.utc.new_offset('-04:00')
    # last_betform_date = last_betform.nil? ? date : last_betform.vendor_id.split('_')[0].to_datetime
    last_betform_date = last_betform.nil? ? today : last_betform.vendor_id.split('_')[0].to_datetime
    # last_betform_date = date
    start_date        = Date.new(last_betform_date.year, last_betform_date.month, last_betform_date.day)
    end_date          = Date.new(today.year, today.month, today.day)

    # @todo 處理Net::FTPTempError: 421 There are too many connections from your internet address.問題
    (start_date..end_date).each do |date|
      self.target_date = date.strftime("%Y%m%d")
      ['AGIN', 'XIN', 'MG', 'BBIN'].each do |platform|
        @target_platform = platform
        # @todo 紀錄最後一次拉的xml檔名
        get_last_files_count = platform == 'BBIN' ? 0 : 12
        read_xml(find_forder(platform), get_last_files_count)
        read_xml(find_forder(platform, true), 1)
      end
    end

    bet_forms
  end

  def get_lost_bet_forms(lost_folder_path)
    self.bet_forms = {}
    lost_folder_path = 'public/third_party_bet_forms/prepare'
    Dir.glob("#{lost_folder_path}/*").each do |platform_folder|
      @target_platform = platform_folder.split('/')[-1]
      Dir.glob("#{platform_folder}/*").each do |date|
        Dir.glob("#{date}/*.xml").each do |xml|
          @xml_file_path = xml
          @target_date = date.split('/')[-1]
          unless File.zero?(xml)
            File.readlines(xml).each do |row|
              filter_bet_form(Nokogiri::XML(row))
            end
          end
          move_done_folder(xml)
        end
        Dir.glob("#{platform_folder}/lostAndfound/*").each do |lost_and_found_date|
          Dir.glob("#{lost_and_found_date}/*.xml").each do |xml|
            @xml_file_path = xml
            unless File.zero?(xml)
              File.readlines(xml).each do |row|
                filter_bet_form(Nokogiri::XML(row))
              end
            end
            move_done_folder(xml)
          end
          remove_empty_folder(lost_and_found_date)
        end
        remove_empty_folder(date)
      end
      remove_empty_folder(platform_folder)
    end
    bet_forms
  end

  private

  def move_done_folder(xml)
    new_xml = xml.gsub 'prepare', 'done'
    dir = File.dirname(xml)
    new_dir = dir.gsub 'prepare', 'done'
    FileUtils.mkdir_p(new_dir) unless File.directory?(new_dir)
    FileUtils.mv(xml, new_xml)
  end

  def remove_empty_folder(path)
    if Dir.glob("#{path}/*").empty?
      FileUtils.remove_dir path, true
    end
  end

  def read_xml(target_dir, get_last_files_count)
    # 找當天資料夾 or loseAndfound

    # FTP login
    ftp = Net::FTP.new
    ftp.connect(Setting.game_platform.ag.ftp.host, Setting.game_platform.ag.ftp.port)
    ftp.login(Setting.game_platform.ag.ftp.username, Setting.game_platform.ag.ftp.password)
    begin
      ftp.chdir(target_dir)
    rescue Net::FTPPermError => err
      if ftp.last_response_code == '550'  # No such directory
        return
      end
    end

    ftp.passive = true
    target_files = ftp.nlst("*.xml")
    get_last_files_count = get_last_files_count == 0 ? target_files.length : get_last_files_count
    target_files = target_files.slice((target_files.length - get_last_files_count)..(target_files.length - 1)) if target_files.length >= get_last_files_count
    target_files.each do |file|
      @xml_file_path = file
      content = ftp.getbinaryfile(file, nil)
      rows = StringIO.new(content)
      rows.each do |row|
        # 處理相應動作
        filter_bet_form(Nokogiri::XML(row))
      end
    end
  end

  def find_forder(dir = 'AGIN', is_lose_and_found = false)
    # AG視訊/BBIN/MG : AG電子遊戲
    is_lose_and_found ? "/#{dir}/loseAndfound/#{target_date}" : "/#{dir}/#{target_date}"
  end

  def filter_bet_form(row)
    @game_platform_id = case @target_platform
    when 'AGIN', 'XIN'
      GAME_PLATFORM_ID
    when 'MG'
      5
    when 'BBIN'
      3
    when 'HUNTER'
      GAME_PLATFORM_ID
    end

    @game_type_id = case row.xpath("//@dataType").text
      when 'BR'
        # AG視訊, BBIN所有遊戲, MG所有遊戲
        # @todo 判斷BBIN遊戲分類
        row.xpath("//@platformType").text == 'MG' ? 2 : 1
      when 'EBR'
        # AG電子遊戲下注紀錄
        2
      when 'HSR'
        # AG捕魚王遊戲
        3
      else
        return
      end
    params = bet_forms_format(row)
    # insert data
    if params.present?
      bet_form = ThirdPartyBetForm.find_or_initialize_by(order_number: params[:order_number])
      bet_form.assign_attributes(params)
      bet_form.save
    end
  end

  # xml資料整理
  def bet_forms_format(xml)
    if xml.xpath("//row").present?
      username = if @target_platform == 'MG'
        xml.xpath("//@playerName").text.remove(Setting.game_platform.mg.ag.prefix)
      elsif @target_platform == 'BBIN'
        xml.xpath("//@playerName").text.remove(Setting.game_platform.bbin.ag.prefix)
      else
        xml.xpath("//@playerName").text
      end
      user = User.find_by_username(username)
      if user.present?
        default_bet_form = default_bet_form_params(user, xml.xpath("//row"))
        # 計算代理分成
        final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
      end
    else
      # @todo 看是否做通知處理
      Rails.logger.debug "[AG - Histories Import] Someting Wrong. (result: #{xml})" unless xml.xpath("//result") == 'No_Data'
    end
  end

  def default_bet_form_params(user, data)
    # 捕魚王
    if @game_type_id == 3
      product_id = data.xpath("//@ID").text
      order_number = "#{@target_platform}_#{data.xpath("//@tradeNo").text}"
      table_id = data.xpath("//@sceneId").text
      game_kind = data.xpath("//@type").text
      extra = {
        scene_start_time: data.xpath("//@SceneStartTime").text,
        scene_end_time: data.xpath("//@SceneEndTime").text,
        room_id: data.xpath("//@Roomid").text,
      }.to_json
      # 房間倍率
      compensate_rate = data.xpath("//@Roombet").text
      bet_total_credit = data.xpath("//@Cost").text.to_f
      reward_amount = data.xpath("//@Earn").text.to_f
      user_credit_diff = data.xpath("//@transferAmount").text.to_f
      balance = data.xpath("//@currentAmount").text.to_f
      betting_at = Time.zone.parse(data.xpath("//@SceneStartTime").text).to_datetime + 12.hours
      valid_amount = data.xpath("//@Cost").text
    else
      product_id = data.xpath("//@mainbillno").text
      order_number = "#{@target_platform}_#{data.xpath("//@billNo").text}"
      table_id = data.xpath("//@tableCode").text
      game_kind = data.xpath("//@gameType").text
      extra = data.xpath("//@remark").text
      compensate_rate = data.xpath("//@odds").text
      bet_total_credit = data.xpath("//@betAmount").text.to_f
      reward_amount = data.xpath("//@betAmount").text.to_f + data.xpath("//@netAmount").text.to_f
      user_credit_diff = data.xpath("//@netAmount").text.to_f
      balance = (data.xpath("//@beforeCredit").text.to_f + data.xpath("//@netAmount").text.to_f).round(2)
      betting_at = Time.zone.parse(data.xpath("//@betTime").text).to_datetime + 12.hours
      valid_amount = data.xpath("//@validBetAmount").text
    end
    {
      user_id:            user.id,
      game_platform_id:   @game_platform_id,
      bet_total_credit:   bet_total_credit,
      reward_amount:      reward_amount,
      user_credit_diff:   user_credit_diff,
      game_type_id:       @game_type_id,

      product_id:         product_id,
      game_record_id:     data.xpath("//@gameCode").text == 'null' ? nil : data.xpath("//@gameCode").text,
      order_number:       order_number,
      table_id:           table_id,

      game_name_id:       data.xpath("//@gameType").text,
      game_betting_kind:  data.xpath("//@playType").text,
      compensate_rate:    data.xpath("//@odds").text,
      balance:            balance,
      betting_at:         betting_at,
      vendor_id:          "#{target_date}_#{order_number}_#{Time.zone.now.utc.strftime("%y%m%d%I%M%S")}_#{rand(10000)}",
      valid_amount:       valid_amount,

      game_kind:          game_kind,
      round:              data.xpath("//@round").text,
      result:             data.xpath("//@result").text,
      ip:                 data.xpath("//@loginIP").text,
      extra:              extra,
      state:              data.xpath("//@flag").text,
      currency:           data.xpath("//@currency").text,
      response:           data.to_json,
      from:               'ag',
      xml_file_path:      @xml_file_path.present? ? @xml_file_path : nil
    }
  end

end
