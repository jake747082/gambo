# WM
class GamePlatforms::Wm < GamePlatforms::Base::Interface
    include Game::ProfitableBetFormHandler
    # 處理遊戲歷程
    autoload :BetFormHandler, 'game_platforms/platforms/wm/bet_form_handler'
    [ BetFormHandler ].each { |m| include m }

    attr_reader :dobusiness_url, :forward_game_url, :md5_key, :des_key, :cagent, :prefix
    attr_reader :username, :password
    attr_accessor :game_type, :lang, :line

    def initialize(user = nil)
        @api_domain    ||=  Setting.game_platform.wm.api_domain
        @vendor_id     ||=  Setting.game_platform.wm.vendor_id
        @signature     ||=  Setting.game_platform.wm.signature
        @user_prefix   ||=  Setting.game_platform.wm.user_prefix
        @limit_type    ||=  Setting.game_platform.wm.limit_type
        @chip          ||=  Setting.game_platform.wm.chip
        @return_url    ||=  Setting.game_platform.wm.return_url

        if user.present?
            @game_platform_user = user.user_game_platformships.wm
            @username = "#{@user_prefix}#{user.lobby_id}"
            @game_platform_user.update!(username: @username) if @game_platform_user.username.blank?
            @password = password_encrypt(@game_platform_user.password, @username)
            @user = user

            #確認該會員有無在平台上註冊，若無則建立資料
            check_or_create_platformAccount
        end
    end

    def transfer_game
        logout_game

        case lang
        when "zh-CN"
            lang_code = 0
        when "zh-TW"
            lang_code = 9
        when "vi-VN" 
            lang_code = 3
        else
            lang_code = 1
        end

        cmd = "LoginGame"
        params = {
            user:      @username,
            password:  @password,
            lang:      lang_code,
            returnurl: @return_url,
            syslang:   1
        }
        resp = send_request(cmd, params)
        url = resp["result"]
    end

    def betting_records
        last_update_time    = ThirdPartyBetForm.wm.exists? ? ThirdPartyBetForm.wm.order(:updated_at).first.updated_at.strftime("%Y-%m-%d %H:%M:%S") : 1.day.ago.strftime("%Y-%m-%d %H:%M:%S")
        last_update_time    = convert_timeZone_to_CST(last_update_time)
        now_query_tme       = Time.zone.now.in_time_zone("Beijing").strftime("%Y-%m-%d %H:%M:%S")
        query_interval_arr  = get_query_bettingRecords_interval(last_update_time, now_query_tme)


        Rails.logger.info "-------------wm report query start-----------"
        Rails.logger.info query_interval_arr
        query_interval_arr.each { |interval|
            rsp = query_betting_record(interval[:startTime],interval[:endTime])             
            bet_forms_format(rsp["result"], 1) if rsp["errorCode"] == 0
            if interval != query_interval_arr.last
                if rsp["errorCode"] == 107
                    sleep 10
                else
                    sleep 30
                end
            end     
        }
        Rails.logger.info "-------------wm report query end-------------"
    end

    # 停用帳號，暫無使用
    def suspension
        cmd = "EnableorDisablemem"
        params = {
            user:      @username,
            type:      "login",
            status:    "N",
            syslang:   1  
        }
        send_request(cmd, params)
    end

    # 查詢特定單號紀錄，暫無使用
    def get_dealStatus(dealid, startTime, endTime)
        cmd = "GetDealidStatus"
        params = {
            user:      @username,
            dealid:    dealid,
            startTime: startTime,
            endTime:   endTime,     
            syslang:   1  
        }
        send_request(cmd, params)
    end

    # 修改玩家限額，暫無使用
    def edit_limit(limit)
        cmd = "EditLimit"   
        params = {
            user:      @username,
            maxwin:    limit["maxwin"],
            reset:     limit["reset"],     
            maxlose:   limit["maxlose"],
            limitType: limit["limitType"],
            syslang:   1 
        }
        send_request(cmd, params)
    end

    # 查詢未結算單，暫無使用，使用需間隔10秒
    def undone_records(time)
        cmd = "GetUnsettleReport"   
        params = {
            Time:      time,
            syslang:   1 
        }
        send_request(cmd, params)
    end

    private

    def check_or_create_platformAccount
        cmd = "MemberRegister"
        params = {
            user:      @username,
            password:  @password,
            username:  "#{@user.lobby_id}",
            syslang:   1,
            limitType: @limit_type,
            chip:      @chip   
        }
        send_request(cmd, params)
    end

    def logout_game
        cmd = "LogoutGame"
        params = {
            user:      @username,
            syslang:   1,
        }
        send_request(cmd, params)
    end

    def send_request(cmd, params={})
        if ( params != {} )
            data     = params.map do |key, value| 
                "#{key}=#{value}" 
            end.join("&")
            data = "&"+data
        else
            data = ""
        end
        url      = "#{@api_domain}?cmd=#{cmd}&vendorId=#{@vendor_id}&signature=#{@signature}#{data}"
        response = Net::HTTP.post_form URI(url),{}
        Rails.logger.info "=== wm-req #{cmd} START==="
        Rails.logger.info JSON.parse(response.body)
        Rails.logger.info "=== wm-req #{cmd} END  ==="
        resp = JSON.parse(response.body)
    end

    def query_betting_record(start_time,end_time)
        cmd = "GetDateTimeReport"
        params = {
            startTime: start_time.gsub(/[- :]/,""),
            endTime:   end_time.gsub(/[- :]/,""),
            syslang:   1,
            timetype:  1,
            datatype:  0

        }
        send_request(cmd, params)
    end

    def get_query_bettingRecords_interval(start_time, end_time)
        interval_number = (DateTime.parse(end_time).to_time - DateTime.parse(start_time).to_time)/1.day
        interval_arr    = []
        interval_count  = interval_number.floor
        for index in 1..interval_count
            interval_arr.push({startTime: (DateTime.parse(start_time) + (index-1).day).strftime("%Y-%m-%d %H:%M:%S"), 
                               endTime: (DateTime.parse(start_time) + (index).day).strftime("%Y-%m-%d %H:%M:%S") }) 
        end    
        if (interval_number > interval_count)
            interval_arr.push({startTime: (DateTime.parse(start_time) + interval_count.day).strftime("%Y-%m-%d %H:%M:%S"),
                               endTime:    DateTime.parse(end_time).strftime("%Y-%m-%d %H:%M:%S") }) 
        end
        return interval_arr 
    end

    def convert_timeZone_to_CST(serverTime)
        cstTime           = (DateTime.parse(serverTime)+12.hours).strftime("%Y-%m-%d %H:%M:%S")
    end

    def password_encrypt(password, salt)
        Digest::SHA1.hexdigest("#{salt}-#{password}$$").slice(0, 20)
    end

end
