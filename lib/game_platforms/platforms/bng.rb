class GamePlatforms::Bng < GamePlatforms::Base::Interface

  def initialize(user)
    @url = Setting.game_platform.bng.url
    @project_name = Setting.game_platform.bng.project_name
    @wl = Setting.game_platform.bng.wl

  end
  
  def games_list
    
  end
  
  def transfer_single_game(game_id, lang)
    # 建立user gametoken
    token = ""
    tz = "-240" #美東-4
    platform = "desktop" #desktop / mobile
    "#{@url}/#{@project_name}/game.html?token=#{token}&game=#{game_id}&ts=#{Time.now.to_i}&wl=#{@wl}&lang=#{bng_lang(lang)}&tz=#{tz}&platform=#{platform}"
  end

  private

   # en /zh / zh-hant / vi
  def bng_lang(lang)
    langs = {
      "zh-CN" => "zh",
      "zh-TW" => "zh-hant",
      "vi-VN" => "vi",
    }
    langs.include?(lang) ? langs[lang] : "en"
  end
end