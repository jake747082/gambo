
class GamePlatforms::East < GamePlatforms::Base::Interface
  require 'nokogiri'
  require 'open-uri'
  include Game::ProfitableBetFormHandler
  # 處理遊戲歷程
  autoload :BetFormHandler, 'game_platforms/platforms/east/bet_form_handler'
  autoload :GameResultHandler, 'game_platforms/platforms/east/game_result_handler'

  [ BetFormHandler, GameResultHandler ].each { |m| include m }

  attr_reader :getdata_url, :dobusiness_url, :pay_url, :agent, :user_key
  attr_reader :username, :password

  def initialize(user = nil)

    @getdata_url    = Setting.game_platform.east.data_url
    @dobusiness_url = Setting.game_platform.east.url
    @pay_url        = Setting.game_platform.east.pay_url
    @prefix        ||= Setting.game_platform.east.prefix

    if user.present?
      @agent          = Setting.game_platform.east.agent
      @user_key       = Setting.game_platform.east.user_key

      game_platform_user = user.user_game_platformships.east
      @username          = "#{@prefix}#{user.lobby_id}"
      game_platform_user.update!(username: @username) if game_platform_user.username.blank?
      @password = password_encrypt(game_platform_user.password, @username)
    else
      @agent          = Setting.game_platform.east.demo.agent
      @user_key       = Setting.game_platform.east.demo.user_key

      @username       = "sfdemo_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{SecureRandom.hex(3)}"
      @password       = SecureRandom.hex(6)
    end

    # 檢查帳號是否存在對方平台, 若無則建立帳號
    check_and_create_account unless account_exist?
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'east/config.yml')
    YAML::load(File.open(abs_path))
  end

  # 投注紀錄
  def betting_records
    @agent          = Setting.game_platform.east.agent
    @user_key       = Setting.game_platform.east.user_key
    vendorid  = ThirdPartyBetForm.east.last.nil? ? 0 : ThirdPartyBetForm.east.last.vendor_id
    # vendorid  = ThirdPartyBetForm.east.last.nil? ? '1422841889782' : ThirdPartyBetForm.east.last.vendor_id
    xml = send_request(request_url(getdata_url, 'gbrbv', { vendorid: vendorid }))
    bet_forms_format(xml.xpath("//result"), 1) # 牌桌 game_type_id : 1
  end

  # 取得遊戲結果
  def game_result(bet_form)
    betting_at = bet_form.betting_at
    params = {
      tableid: bet_form.table_id,
      stage: bet_form.stage,
      inning: bet_form.inning,
      datestart: betting_at.strftime("%Y-%m-%d"),
      dateend: (betting_at + 1.day).strftime("%Y-%m-%d")
    }
    xml = send_request(request_url(getdata_url, 'ggr', params))
    game_result_format(xml.xpath("//result"), bet_form.game_record_id, bet_form.game_kind.to_s)
  end

  # 進入遊戲
  def transfer_game
    params = {
      username: username,
      truename: username,
      password: password,
      domain: Setting.game_platform.domain,
      gametype: '1',
      gamekind: '0',
      dataid: '1',
      platformname: Setting.title,
      iframe: '0',
    }
    request_url(dobusiness_url, 'tg', params)
  end

  # 修改限紅及遊戲權限
  #
  # note:
  #   limittype = 1 : video限紅 ; limit代入限紅id
  #   limittype = 2 : 輪盤限紅 ; limit代入限紅id
  #   limittype = 3 : 遊戲權限 ; limit代入14個 0 or 1 字串, ex: 1,1,1,1,1,1,1,1,1,1,1,1,1,1
  #
  def update_limit(limit, limittype)
    params = {
      username: username,
      limit: limit,
      limittype: limittype
    }
    xml = send_request(request_url(dobusiness_url, 'ul', params))
    xml.xpath("//result").text == '1' ? true : false
  end

  # 轉點數
  # type: 'OUT' or 'IN'
  def transfer_credit(credit, type)
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    billno = "#{time_now}#{Random.rand(1000)}"
    params = {
      username: username,
      password: password,
      billno: billno,
      type: type,
      credit: credit
    }

    xml = send_request(request_url(pay_url, 'ptc', params))
    if @code.to_i < 400
      xml.xpath("//result").text == '1' ? true : false
    else
      # 確認轉帳是否成功
      confirm_transfer_credit(credit, type, billno)
    end
  rescue ApiError => e
    return e
  end

  private

  # 檢查帳號是否存在
  def account_exist?
    xml = send_request(request_url(dobusiness_url, 'caie', { username: username }))
    xml.xpath("//result").text == '1' ? true : false
  end

  # 建立帳號
  def check_and_create_account
    params = {
      username: username,
      password: password,
      moneysort: 'RMB',
      limit: 1111111111111,
      limitvideo: 37,
      limitroulette: 1
    }
    xml = send_request(request_url(dobusiness_url, 'caca', params))
    xml.xpath("//result").text == '1' ? true : false
  end

  def confirm_transfer_credit(credit, type, billno)
    params = {
      username: username,
      password: password,
      billno: billno,
      type: type,
      credit: credit,
      flag: "1"
    }

    xml = send_request(request_url(dobusiness_url, 'ctc', params))
    xml.xpath("//result").text == '1' ? true : false
  rescue ApiError => e
    return e
  end

  def send_request(url)
    response = Net::HTTP.get_response(URI.parse(url))
    @code = response.code
    Nokogiri::XML(response.body)
  end

  def request_url(url, method, args = {})
    args = args.merge({ agent: agent, method: method }).map do |field, value|
      "#{field}=#{value}"
    end.join('$')
    params   = encode_base64(args)
    key      = encode_md5(params + user_key)
    "#{url}?params=#{params}&key=#{key}"
  end

  def encode_base64(args)
    Base64.strict_encode64(args)
  end

  def encode_md5(str)
    Digest::MD5.hexdigest(str)
  end

  def password_encrypt(password, salt)
    Digest::SHA1.hexdigest("#{salt}-#{password}--")
  end
end
