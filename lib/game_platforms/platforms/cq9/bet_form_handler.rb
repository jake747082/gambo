module GamePlatforms::Cq9::BetFormHandler
  
  GAME_PLATFORM_ID = 9

 def bet_forms_format(record)
    Rails.logger.info "===========GamePlatforms::Cq9::BetFormHandler========="
    if record.present?
      users = {}
      exist_bet_forms = {}
      bet_forms = record.map do |data|
        # @todo refactor : remove loop query
p data
        username = data['account'].split(Setting.game_platform.cq9.prefix)[1]
        unless username.nil?
          user = users.key?(username) ? users[username] : User.find_by_username(username)
          users[username] = user
Rails.logger.info '---------------'
Rails.logger.info username
Rails.logger.info 'round id========='
Rails.logger.info data['round']
is_exist_bet_forms = exist_bet_forms.key?(data['round']) ? exist_bet_forms[data['round']] : ThirdPartyBetForm.cq9.exists?(round: data['round'])
Rails.logger.info '判斷========='
Rails.logger.info exist_bet_forms.key?(data['round'])
Rails.logger.info '已存在========='
Rails.logger.info exist_bet_forms[data['round']]
Rails.logger.info '找db========='
Rails.logger.info ThirdPartyBetForm.cq9.exists?(round: data['round'])
Rails.logger.info '結果========='
Rails.logger.info is_exist_bet_forms
Rails.logger.info !is_exist_bet_forms.nil?
Rails.logger.info '========='
          if user.present? && !is_exist_bet_forms.nil? && data['status'] == 'complete'
Rails.logger.info '*************'
Rails.logger.info data
            exist_bet_forms[data['round']] = true
            default_bet_form = default_bet_form_params(user, data)
            # User.update_counters user, play_total: default_bet_form[:user_credit_diff]
            # 計算代理分成
            final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
            # insert data
            bet_form = ThirdPartyBetForm.find_or_initialize_by(round: final_params[:round])
            bet_form.assign_attributes(final_params)
            bet_form.save
          end
        end
      end.compact
    end
  end

  private

  def default_bet_form_params(user, data)
    if data['gametype'] == 'slot'
      game_type_id = 2
      prefix = 'SLOT'
    else
      game_type_id = 3
      prefix = 'TABLE'
    end
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   data['bet'].to_f,
      reward_amount:      data['win'].to_f,
      user_credit_diff:   data['win'].to_f - data['bet'].to_f,
      game_type_id:       game_type_id,

      order_number:       "cq9_#{data['round']}",

      game_name_id:       data['gamecode'],
      game_kind:          data['gametype'],
      betting_at:         DateTime.strptime(data['bettime'], '%Y-%m-%dT%H:%M:%S%z').strftime("%Y-%m-%d %H:%M:%S"),
      vendor_id:          "#{prefix}_#{data['round']}",
      valid_amount:       data['bet'].to_f,
      
      round:              data['round'],
      result:             data['win'].to_f > 0 ? 'W' : 'L',
      response:           data.to_json,
      from:               'cq9',
    }
  end
end