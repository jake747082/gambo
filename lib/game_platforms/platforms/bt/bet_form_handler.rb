module GamePlatforms::Bt::BetFormHandler

  GAME_PLATFORM_ID = 7

  def bet_forms_format(record, game_type_id)
    bet_forms = []
    jp_logs = []
    platform_games = []
    if record.present?
      record.map do |data|
        username = if Setting.game_platform.bt.prefix.empty?
          # "#{agent}_#{username}"
          data['username'].split("#{Setting.game_platform.bt.agent}_")[1]
        else
          # "#{agent}_#{prefix}#{username}"
          data['username'].split(Setting.game_platform.bt.prefix)[1]
        end
        unless username.nil?
          Rails.logger.info "------------- #{username}: #{data['id']} -----------"
          # @TODO 底下 sql 拉出 loop
          user_id = User.lobby_id_to_id(username)
          user = User.find_by_id(user_id) if user_id > 0
          if !user.nil? && !ThirdPartyBetForm.bt.exists?(vendor_id: data['id'])
            default_bet_form = default_bet_form_params(user, data, game_type_id)
            # 計算代理分成
            final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff].to_f)
            bet_forms << ThirdPartyBetForm.new(final_params.merge!(order_number: final_params[:order_number]))
          end
        end
      end.compact
      # jp計算
      record.group_by{|d| d["game_code"]}.each do |gamecode,game_datas|
        game_datas.delete_if{|data| ThirdPartyBetForm.bt.exists?(vendor_id: data['id'])} 
        platform_game = PlatformGame.find_by_code(gamecode)
        unless platform_game.nil?
          bet = game_datas.sum{|a|a["bet"]} 
          befor_jp = platform_game.jp_md.dup
          befor_bet_total_credit = platform_game.bet_total_credit.dup
          platform_game.bet_total_credit += bet
          platform_game.jp_md = (platform_game.bet_total_credit - platform_game.game_reset) * platform_game.rate_percent;
          platform_games << platform_game
          jp_logs << JpChangeLog.new({
            :manual => false, 
            :befor_jp_change => befor_jp,
            :after_jp_change => platform_game.jp_md,
            :befor_bet_total_credit => befor_bet_total_credit, 
            :after_bet_total_credit => platform_game.bet_total_credit, 
            :jp_change => platform_game.jp_md - befor_jp,
            :jp_rate => platform_game.jp_rate, 
            :platform_game => platform_game 
            })
        end
      end
    end
    unless platform_games.empty?
      Rails.logger.info platform_games
      ActiveRecord::Base.transaction do
        PlatformGame.import platform_games, on_duplicate_key_update: [:bet_total_credit, :jp_md]
      end
    end
    unless bet_forms.empty?
      Rails.logger.info bet_forms
      # insert data
      ActiveRecord::Base.transaction do
        ThirdPartyBetForm.import bet_forms, on_duplicate_key_update: [:bet_total_credit, :reward_amount, :user_credit_diff, :valid_amount, :result]
      end
    end
    unless jp_logs.empty?
      Rails.logger.info jp_logs
      ActiveRecord::Base.transaction do
        JpChangeLog.import jp_logs
      end
    end
  end

  private

  def default_bet_form_params(user, data, game_type_id)
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   data['bet'],
      reward_amount:      data['win'],
      user_credit_diff:   data['diff'],
      game_type_id:       game_type_id,

      order_number:       "BT_#{data['id']}",

      game_name_id:       data['game_code'],
      start_at:           data['bet_time'],
      end_at:             data['end_time'],
      betting_at:         data['time'],
      vendor_id:          data['id'],
      # valid_amount:       game_type_id == 3 ? 0 : data['bet'], # 捕魚機有效投注為 0
      valid_amount:       data['bet'],
      
      round:              data['round_id'],
      game_kind:          data['game_code'],
      result:             (data['diff'].to_f) > 0 ? 'W': 'L',
      ip:                 data['ip'] || nil,
      response:           data.to_json,
      from:               'bt',
    }
  end

end
