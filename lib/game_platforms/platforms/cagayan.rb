class GamePlatforms::Cagayan < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  autoload :BetFormHandler, 'game_platforms/platforms/cagayan/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  attr_reader :url, :hash_code, :prefix
  attr_reader :username, :password
  attr_accessor :game_type, :slots_game_id

  def initialize(user = nil)
    @url ||= Setting.game_platform.cagayan.url
    @hash_code ||= Setting.game_platform.cagayan.hash_code
    @prefix ||= Setting.game_platform.cagayan.prefix

    if user.present?
      game_platform_user = user.user_game_platformships.cagayan
      @username = "#{prefix}#{user.username}"
      @password = game_platform_user.password
      check_and_create_account
    end
  end

  def check_and_create_account
    login['errorCode'] == 0 ? true : false
  end

  def transfer_credit(credit, type)
    ref = "#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{Random.rand(1000)}"
    return false unless valid_ref?(ref)

    command = type == 'IN' ? 'DEPOSIT': 'WITHDRAW'
    params = {
      hashCode: hash_code,
      command: command,
      params: {
        username: username,
        password: Digest::MD5.hexdigest(password),
        ref: ref,
        desc: '',
        amount: credit.to_s
      }
    }
    response = JSON.parse(send_request(url, params))
    response['errorCode'] == 0 ? true : false
  end

  def get_balance
    params = {
      hashCode: hash_code,
      command: 'GET_BALANCE',
      params: {
        username: username,
        password: Digest::MD5.hexdigest(password),
      }
    }
    response = JSON.parse(send_request(url, params))
    response['errorCode'] == 0 ? response['params']['balance'].to_f : false
  end

  def transfer_game
    # @game_type ||= 'SLOTS_CG'
    @game_type ||= 'LIVE'
    @slots_game_id ||= '1'

    login['params']['link']
  end

  def betting_records
    # get_betting_records('GET_SLOTS_CG_RECORD', 'SLOT')
    get_betting_records('GET_RECORD_BY_SEQUENCENO', 'LIVE')
    get_betting_records('GET_ADJUSTED_RECORD')
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'ag/config.yml')
    YAML::load(File.open(abs_path))
  end

  private

  def get_betting_records(command, prefix = '')
    if prefix == 'SLOT'
      last_bet_form  = ThirdPartyBetForm.slot.cagayan.last
      vendorid = last_bet_form.nil? ? 0 : last_bet_form.vendor_id.split("#{prefix}_").last
    else
      if prefix.present?
        last_bet_form = ThirdPartyBetForm.video.cagayan.last
        vendorid = last_bet_form.nil? ? 0 : last_bet_form.vendor_id.split("#{prefix}_").last
      else
        vendorid = GamePlatform.cagayan.record_id
      end
    end
    params = {
      hashCode: hash_code,
      command: command,
      params: {
        beginId: vendorid.to_s,
        count: '1000',
      }
    }
    response = JSON.parse(send_request(url, params))

    return false if response['errorCode'] != 0
    if command == 'GET_ADJUSTED_RECORD'
      update_bet_forms_format(response['params']['recordList'])
    else
      bet_forms_format(response['params']['recordList'], prefix)
    end
  end

  def login
    slot_params = {
      slotsGameId: @slots_game_id,
      gameType: @game_type
    }
    user_params = {
      username: username,
      password: Digest::MD5.hexdigest(password),
      nickname: username,
      currency: 'CNY',
      language: 'CN',
      line: 1,
    }
    user_params = @game_type == 'SLOTS_CG' ? user_params.merge(slot_params) : user_params
    params = {
      hashCode: hash_code,
      command: 'LOGIN',
      params: user_params
    }
    response = send_request(url, params)
    Rails.logger.info response
    JSON.parse(response)
  end

  # 該單號是否不存在
  def valid_ref?(ref)
    params = {
      hashCode: hash_code,
      command: 'CHECK_REF',
      params: {
        ref: ref
      }
    }
    response = JSON.parse(send_request(url, params))
    response['errorCode'] == 0 ? true : false
  end

  def send_request(url, params)
    uri              = URI.parse(url)
    http             = Net::HTTP.new(uri.host, uri.port)
    request          = Net::HTTP::Post.new(uri.request_uri, 'Content-Type' =>'application/json')
    request.body = params.to_json

    http.request(request).body
  end
end