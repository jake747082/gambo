module GamePlatforms::Bts::BetFormHandler

  GAME_PLATFORM_ID = 11

  def bet_forms_format(record, game_type_id)
    bet_forms = []
    jp_logs = [] 
    platform_game = PlatformGame.find_by_code("BTS")
    if record.present?
      record.map do |data|
        username = if Setting.game_platform.bts.prefix.empty?
          data['account']
        else
          data['account'].split(Setting.game_platform.bts.prefix)[1]
        end
        unless username.nil?
          Rails.logger.info "------------- #{username}: #{data['id']} -----------"
          # @TODO 底下 sql 拉出 loop
          user_id = User.lobby_id_to_id(username)
          user = User.find_by_id(user_id) if user_id > 0
          if !user.nil? && !ThirdPartyBetForm.bts.exists?(vendor_id: data['id'])
            default_bet_form = default_bet_form_params(user, data, game_type_id)
            # 計算代理分成
            final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff].to_f)
            bet_forms << ThirdPartyBetForm.new(final_params.merge!(order_number: final_params[:order_number]))
            # jp計算
            unless platform_game.nil?
              bet = data["wagermoney"].to_i
              befor_jp = platform_game.jp_md.dup
              befor_bet_total_credit = platform_game.bet_total_credit.dup
              platform_game.bet_total_credit += bet
              platform_game.jp_md = (platform_game.bet_total_credit - platform_game.game_reset) * platform_game.rate_percent;
              platform_game.save
              jp_logs << JpChangeLog.new({
                :manual => false, 
                :befor_jp_change => befor_jp,
                :after_jp_change => platform_game.jp_md,
                :befor_bet_total_credit => befor_bet_total_credit, 
                :after_bet_total_credit => platform_game.bet_total_credit, 
                :jp_change => platform_game.jp_md - befor_jp,
                :jp_rate => platform_game.jp_rate, 
                :platform_game => platform_game 
                })
            end
          end
        end
        
      end.compact
    end
    unless bet_forms.empty?
      Rails.logger.info bet_forms
      # insert data
      ActiveRecord::Base.transaction do
        ThirdPartyBetForm.import bet_forms, on_duplicate_key_update: [:bet_total_credit, :reward_amount, :user_credit_diff, :valid_amount, :result]
      end
    end
    unless jp_logs.empty?
      Rails.logger.info jp_logs
      ActiveRecord::Base.transaction do
        JpChangeLog.import jp_logs
      end
    end
  end

  private

  def default_bet_form_params(user, data, game_type_id)
    ip = begin
      game_record = JSON.parse(data['GameRecord'])
      game_record['UserIp']
    rescue JSON::ParserError
      nil
    end
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   data['wagermoney'].to_f,
      reward_amount:      data['changeamount'].to_f + data['wagermoney'].to_f,
      user_credit_diff:   data['changeamount'].to_f,
      game_type_id:       game_type_id,

      order_number:       "BTS_#{data['GameNum']}",

      game_name_id:       data['GameID'],
      # @TODO be better : 原北京時區切換至美東時區
      betting_at:         (DateTime.strptime(data['changetime'], '%m/%d/%Y %I:%M:%S %p') - 12.hours).strftime("%Y-%m-%d %H:%M:%S"),
      vendor_id:          data['GameNum'],
      valid_amount:       data['validWagerMoney'].to_f,
      
      round:              data['GameNum'],
      game_kind:          data['GameID'],
      result:             (data['changeamount'].to_f) > 0 ? 'W': 'L',
      ip:                 ip,
      response:           data.to_json,
      from:               'bts',
    }
  end

end
