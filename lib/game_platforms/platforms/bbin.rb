# @todo logout
# @todo 回傳錯誤處理
class GamePlatforms::Bbin < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler
  # 處理遊戲歷程
  autoload :BetFormHandler, 'game_platforms/platforms/bbin/bet_form_handler'
  autoload :GameResultHandler, 'game_platforms/platforms/bbin/game_result_handler'

  [ BetFormHandler, GameResultHandler].each { |m| include m }

  attr_reader :url, :game_url, :website, :uppername, :keys, :prefix
  attr_reader :username, :password

  def initialize(user)
    @url            = Setting.game_platform.bbin.url
    @game_url       = Setting.game_platform.bbin.game_url
    @website        = Setting.game_platform.bbin.website
    @uppername      = Setting.game_platform.bbin.uppername
    @keys           = Setting.game_platform.bbin.keys
    @prefix         = Setting.game_platform.bbin.prefix

    if user.present?
      game_platform_user = user.user_game_platformships.bbin
      @username = "#{prefix}#{user.username}"
      @password = game_platform_user.password
      # 檢查帳號是否存在對方平台, 若無則建立帳號
      check_and_create_account
    end
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'bbin/config.yml')
    YAML::load(File.open(abs_path))
  end

  def get_balance
    params = {
      website: website,
      username: username,
      uppername: uppername,
      page: 1,
      pagelimit: 10
    }
    response = send_request(request_url(params, 'CheckUsrBalance'))
    response['result'] ? response['data'].select{|value| value['Currency'] == 'RMB'}.first['Balance'] : false
  end

  def transfer_credit(credit, type)
    # @todo 各平台 remitno 統一
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    remitno = "#{time_now}#{Random.rand(1000)}"
    params = {
      website: website,
      username: username,
      uppername: uppername,
      remitno: remitno,
      action: type,
      remit: credit.to_i,
    }
    send_request(request_url(params, 'Transfer'))
    # 再次確認
    confirm_transfer_credit(remitno)
  end

  def betting_records
    # @todo 若是前幾天的
    starttime  = ThirdPartyBetForm.bbin.today.present? ? (ThirdPartyBetForm.bbin.first.betting_at + 1.seconds).strftime("%Y-%m-%d %H:%M:%S").split(' ')[1] : '00:00:00'
    params = {
      website: website,
      uppername: uppername,
      rounddate: DateTime.now.utc.new_offset('-04:00').strftime("%Y-%m-%d"),
      # starttime: starttime,
      # endtime: DateTime.now.utc.new_offset('-04:00').strftime("%H:%M:%S"),
      gamekind: 3,
    }
    response = send_request(request_url(params, 'BetRecord'))
    bet_forms_format(response['data'])
  end

  def transfer_game
    params = {
      website: website,
      username: username,
      uppername: uppername,
      password: password,
      page_site: 'live'
    }
    response = Net::HTTP.get_response(URI.parse(request_url(params, 'Login', game_url)))
    response.body
  end

  def check_and_create_account
    params = {
      website: website,
      username: username,
      uppername: uppername,
      password: password
    }
    response = send_request(request_url(params, 'CreateMember'))
    response['result']
  end

  def game_result(bet_form)
    game_result_format(bet_form)
  end

  private

  def confirm_transfer_credit(remitno)
    params = {
      website: website,
      transid: remitno
    }
    response = send_request(request_url(params, 'CheckTransfer'))
    response['result']
  end

  def send_request(url)
    response = Net::HTTP.get_response(URI.parse(url))
    code = response.code
    response.body.present? ? JSON.parse(response.body) : ''
  end

  def request_url(params, method, api_url = url)
    key_params = params.slice(:website, :username, :remitno).map{|field, value| value}.join
    params = params.map do |field, value|
      "#{field}=#{value}"
    end.join('&')
    "#{api_url}/#{method}?#{params}&key=#{encode_key(key_params, method)}"
  end

  def encode_key(key_params, method)
    key = keys[method]
    key_a = random_string(key['KeyA'].to_i)
    key_c = random_string(key['KeyC'].to_i)
    key_b = Digest::MD5.hexdigest("#{key_params}#{key['KeyB']}#{DateTime.now.utc.in_time_zone("Eastern Time (US & Canada)").strftime("%Y%m%d")}")
    "#{key_a}#{key_b}#{key_c}"
  end

  def random_string(length)
    rand(36**length).to_s(36).rjust(length, '0')
  end
end
