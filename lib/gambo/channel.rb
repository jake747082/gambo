module Gambo
  class Channel

    LOBBY = 'private-lobby-global'
    AGENT = 'private-agent-global'
    CASH  = 'private-cash-global'
    SHOP  = 'private-shop-channel'
    ADMIN = 'private-admin-global'

    GLOBAL = [ LOBBY, CASH, AGENT ]

    PLAYER = [ LOBBY, CASH ]
  end
end