# Forms
module BaseForm
  autoload :Basic, 'app/forms/base_form/basic'
  autoload :Reform, 'app/forms/base_form/reform'
end

module SharedForm
  autoload :Authable, 'app/forms/shared_form/authable'
end

module AgentForm
  autoload :Base, 'app/forms/agent_form/base'
  autoload :Auth, 'app/forms/agent_form/auth'
  autoload :Create, 'app/forms/agent_form/create'
  autoload :Update, 'app/forms/agent_form/update'
  autoload :Apply, 'app/forms/agent_form/apply'

  module SubAccount
    autoload :Base, 'app/forms/agent_form/sub_account/base'
    autoload :Create, 'app/forms/agent_form/sub_account/create'
    autoload :Update, 'app/forms/agent_form/sub_account/update'
  end
end

module BankForm
  autoload :Update, 'app/forms/bank_form/update'
end

module UserForm
  autoload :Base, 'app/forms/user_form/base'
  autoload :Auth, 'app/forms/user_form/auth'
  autoload :Create, 'app/forms/user_form/create'
  autoload :Update, 'app/forms/user_form/update'
  autoload :Deposit, 'app/forms/user_form/deposit'
  autoload :Cashout, 'app/forms/user_form/cashout'
  autoload :TransferCredit, 'app/forms/user_form/transfer_credit'
  autoload :Signup, 'app/forms/user_form/signup'
  autoload :Login, 'app/forms/user_form/login'
  autoload :AccountLogin, 'app/forms/user_form/account_login'
  autoload :Loginable, 'app/forms/user_form/loginable'
  module Password
    autoload :SendValidation, 'app/forms/user_form/password/send_validation'
    autoload :Validation, 'app/forms/user_form/password/validation'
    autoload :Reset, 'app/forms/user_form/password/reset'
    autoload :Alter, 'app/forms/user_form/password/alter'
  end
end

module UnconfirmedUserForm
  autoload :Validation, 'app/forms/unconfirmed_user_form/validation'
end

module WithdrawForm
  autoload :Deposit, 'app/forms/withdraw_form/deposit'
  autoload :Cashout, 'app/forms/withdraw_form/cashout'
end

module AgentWithdrawForm
  autoload :Deposit, 'app/forms/agent_withdraw_form/deposit'
  autoload :Cashout, 'app/forms/agent_withdraw_form/cashout'
end

module MessageForm
  autoload :Create, 'app/forms/message_form/create'
  autoload :Update, 'app/forms/message_form/update'
end
# Services
module Shared
  autoload :Pushable, 'app/services/shared/pushable'
end

# Should mix platform DSL before model load
ActiveRecord::Base.send(:include, Gambo::Platforms)

# Load initializer
Dir.glob("#{File.dirname(__FILE__)}/../../config/initialize/*.rb").each { |file| require file }

# Model Concerns
Dir.glob("#{File.dirname(__FILE__)}/../app/models/concerns/*.rb").each { |file| require file }

# Controller Concerns
Dir.glob("#{File.dirname(__FILE__)}/../app/controller/*.rb").each { |file| require file }

# Models
Dir.glob("#{File.dirname(__FILE__)}/../app/models/*.rb").each { |file| require file }

# Services
require 'app/services/base_service'
Dir.glob("#{File.dirname(__FILE__)}/../app/services/**/*.rb").each { |file| require file }

# Helpers
Dir.glob("#{File.dirname(__FILE__)}/../app/helpers/*.rb").each { |file| require file }

# Mailer
Dir.glob("#{File.dirname(__FILE__)}/../app/mailer/*.rb").each { |file| require file }