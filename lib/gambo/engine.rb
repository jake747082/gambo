module Gambo
  class Engine < ::Rails::Engine
    initializer :prepend_migrations do |app|
      config.paths["db/migrate"].expanded.each do |prepend_path|
        app.config.paths["db/migrate"].unshift(prepend_path)
      end
    end
  end
end
