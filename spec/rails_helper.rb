require "rails/all"
require 'rspec/rails'
require 'bundler/setup'
Bundler.setup

require 'simplecov'
require 'mysql2'
SimpleCov.start
require 'database_cleaner'
require 'support/settings'
require 'support/factory_girl'

FactoryGirl.definition_file_paths = [File.expand_path('../factories', __FILE__)]
FactoryGirl.find_definitions

RSpec.configure do |config|
  config.infer_spec_type_from_file_location!

  config.before(:suite) do
    DatabaseCleaner.strategy = [:truncation, pre_count: true]
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:all) do
    DatabaseCleaner.start
  end

  config.after(:all) do
    DatabaseCleaner.clean
  end
end
