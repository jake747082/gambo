RSpec.describe Racing::Tasks do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  context "when run horses task" do
    pending
    # before {
    #   allow(Racing::Tasks).to receive(:ALL_RACING_SECS) {30}
    #   @tasks = Racing::Tasks.new()
    # }
    # it "get result after run task" do
    #   schedule = @tasks.send(:create_schedule, @racing_horses)
    #   reward_result, reward_amount = @tasks.run!(schedule.id)
    #   expect(reward_result.split(',').size).to eq(3)
    # end
    # context "when schedule not enough" do
    #   before { @tasks.create_schedules! }
    #   it "not start schedules count should eq #NOT_START_SCHEDULE_COUNT" do
    #     expect(RacingSchedule.not_start.size).to eq Racing::Tasks::NOT_START_SCHEDULE_COUNT
    #   end
    # end
    # context "when schedule overdue" do
    #   before {
    #     @racing_horses_schedule.update!(run_racing_time: Time.zone.now - 10)
    #     @racing_eight_dogs_schedule.update!(run_racing_time: Time.zone.now - 10)
    #     @racing_six_dogs_schedule.update!(run_racing_time: Time.zone.now - 10)
    #     @racing_horses_schedule.reload
    #     @racing_eight_dogs_schedule.reload
    #     @racing_six_dogs_schedule.reload
    #     @tasks.overdue_schedule_handler
    #   }
    #   it "overdue schedules should be checkout" do
    #     expect(@racing_horses_schedule.reload.no1).to_not eq nil
    #     expect(@racing_eight_dogs_schedule.reload.no1).to_not eq nil
    #     expect(@racing_six_dogs_schedule.reload.no1).to_not eq nil
    #   end
    # end
  end
end
