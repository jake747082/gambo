RSpec.describe Racing::Cashout::BetFormHandler do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  shared_examples "schedule cashout" do
    subject { player }

    context "but not reward" do
      before(:all) { Racing::Cashout.new(@racing_horses_schedule.id, ['2,3,1', 0]).call! }
      it "bet form reward_amount should be 0" do
        @bet_form = subject.racing_bet_forms.last
        expect(@bet_form.reward_amount).to eq 0
        expect(@bet_form.racing_sub_bet_forms.last.reward_amount).to eq 0
      end
      it "subject credit used should be 10" do
        expect(subject.credit_used).to eq 10
      end
    end

    context "and reward" do
      pending
      # before(:all) { Racing::Cashout.new(@racing_horses_schedule.id, ['1,2,3', 40]).call! }
      # it "bet form reward_amount should be 40" do
      #   @bet_form = subject.racing_bet_forms.last
      #   @sub_bet_form = @bet_form.racing_sub_bet_forms.last
      #   expect(@bet_form.reward_amount).to eq 40
      #   expect(@sub_bet_form.reward_amount).to eq 40
      # end
      # it "subject credit used should be -30" do
      #   expect(subject.credit_used).to eq -30
      # end
    end
    it "total dispatched amount * -1 should == user credit diff" do
      @total_dispatched_profit = @service.bet_form.shareholder_owe_parent +
      @service.bet_form.shareholder_win_amount +
      @service.bet_form.director_win_amount +
      @service.bet_form.agent_win_amount
      expect(@total_dispatched_profit * -1).to eq @service.bet_form.machine_credit_diff
    end
  end

  context "Machine Bet Form" do
    include_context "racing machine bet"

    it_behaves_like 'schedule cashout' do
      let!(:player) { machine }
    end
  end

  context "User Bet Form" do
    include_context "racing user bet"

    it_behaves_like 'schedule cashout' do
      let!(:player) { user }
    end
  end
end