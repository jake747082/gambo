RSpec.describe Racing::Cashout::BetFormHandler do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  context "When Schedule Cashout" do
    before(:all) {
      Racing::Cashout.new(@racing_horses_schedule.id, ['2,3,1', 0]).call!
      @racing_horses_schedule.reload
    }

    it "schedule rank should be save" do
      expect(@racing_horses_schedule.no1).to eq 2
      expect(@racing_horses_schedule.no2).to eq 3
      expect(@racing_horses_schedule.no3).to eq 1
    end
  end
end