shared_examples "reward combinations calculator" do
  it "#reward_combinations_calculator should calculator max reward credit" do
    expect(@reward_combinations_calculator.reward_combinations['1,2,3']['credit']).to eq 40
    expect(@reward_combinations_calculator.reward_combinations['4,2,3']['credit']).to eq 22
    expect(@reward_combinations_calculator.reward_combinations['5,6,1']['credit']).to eq 960
  end

  it "#find_winnable_combinations should include reward less then #max_response_credit" do
    combinations = @reward_combinations_calculator.find_winnable_combinations(40)
    all_rank = combinations.map{|rank, reward| rank}
    expect(all_rank).to include('1,2,3')
    expect(all_rank).to include('4,2,3')
    expect(all_rank).to include('1,5,6')
    expect(all_rank).to_not include('6,7,1')
    expect(all_rank).to_not include('8,2,1')
    expect(all_rank).to_not include('1,3,4')
  end
  it "#find_empty_combinations should include reward empty" do
    combinations = @reward_combinations_calculator.find_empty_combinations
    all_rank = combinations.map{|rank, reward| rank}
    expect(all_rank).to include('6,7,1')
    expect(all_rank).to include('8,2,1')
    expect(all_rank).to include('5,7,2')
    expect(all_rank).to_not include('1,2,3')
    expect(all_rank).to_not include('1,4,6')
    expect(all_rank).to_not include('5,6,8')
  end
  it "#find_max_percent_of_winnable_combinations should include 0% ~ #max_percent_of_winnable% range reward" do
    combinations = @reward_combinations_calculator.find_max_percent_of_winnable_combinations
    all_rank = combinations.map{|rank, reward| rank}
    expect(all_rank).to include('6,7,1')
    expect(all_rank).to include('1,2,3')
    expect(all_rank).to include('5,4,2')
    expect(all_rank).to_not include('1,4,3')
    expect(all_rank).to_not include('1,4,6')
    expect(all_rank).to_not include('5,6,8')
  end
end

describe Racing::Result::RewardCombinationsCalculator do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  context "When Machine Bet" do
    pending
    # before {
    #   # machine bet
    #   @machine.update!(credit_max: 2000, credit_used: 0)
    #   @machine.reload
    #   bet_info = {'sub_bets' => [{'bet_credit' => 10, 'bet_number' => 1, 'bet_type' => 'champion'},
    #                             {'bet_credit' => 10, 'bet_number' => 4, 'bet_type' => 'place'},
    #                             {'bet_credit' => 10, 'bet_number' => '5,6', 'bet_type' => 'quinella'}
    #     ]}
    #   Racing::Bet.new(@racing_horses_schedule.id, @machine).bet!(bet_info)
    #   @reward_combinations_calculator = Racing::Result::RewardCombinationsCalculator.new(@racing_horses.game_model)
    #
    #   bet_form = @machine.reload.racing_bet_forms.first
    #   bet_form.racing_sub_bet_forms.each do |sub_bet_form|
    #     @reward_combinations_calculator << sub_bet_form
    #   end
    # }
    # it_behaves_like "reward combinations calculator"
  end

  context "When User Bet" do
    pending
    # before {
    #   # user bet
    #   @user.update!(credit: 2000, credit_used: 0)
    #   @user.reload
    #   bet_info = {'sub_bets' => [{'bet_credit' => 10, 'bet_number' => 1, 'bet_type' => 'champion'},
    #                             {'bet_credit' => 10, 'bet_number' => 4, 'bet_type' => 'place'},
    #                             {'bet_credit' => 10, 'bet_number' => '5,6', 'bet_type' => 'quinella'}
    #     ]}
    #   Racing::Bet.new(@racing_horses_schedule.id, @user, true).bet!(bet_info)
    #   @reward_combinations_calculator = Racing::Result::RewardCombinationsCalculator.new(@racing_horses.game_model)
    #
    #   bet_form = @user.reload.racing_bet_forms.first
    #   bet_form.racing_sub_bet_forms.each do |sub_bet_form|
    #     @reward_combinations_calculator << sub_bet_form
    #   end
    # }
    # it_behaves_like "reward combinations calculator"
  end
end
