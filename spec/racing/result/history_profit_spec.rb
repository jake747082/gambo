describe Racing::Result::HistoryProfit do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  context "When Racing Get Max Response Credit" do
    before { @history_profit = Racing::Result::HistoryProfit.new(@racing_horses_schedule) }
    it "current lose cycle should be 'xs'" do
      expect(@history_profit.send(:current_lose_cycle)).to eq 'xs'
    end
    it "max can dispatched credit should be 0" do
      expect(@history_profit.send(:max_can_dispatched_credit)).to eq 0
    end
    it "racing win speed rate should be 0.2" do
      expect(@history_profit.send(:racing_win_speed_rate)).to eq 0.2
    end
    it "user lose speed rate should be 0.8" do
      expect(@history_profit.send(:user_lose_speed_rate)).to eq 0.8
    end
    it "max response credit should be <= 8" do
      expect(@history_profit.max_response_credit).to be <= 8
    end
  end
end