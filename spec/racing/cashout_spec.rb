RSpec.describe Racing::Cashout do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"
  include_context "racing machine bet"
  include_context "racing user bet"

  context "when checkout horses schedule" do
    before {
      @result = Racing::Result.new(@racing_horses_schedule.id).reward!
      Racing::Cashout.new(@racing_horses_schedule.id, @result).call!
      @racing_horses_schedule.reload
    }
    it "bet forms's agent should not empty" do
      bet_forms = @racing_horses_schedule.racing_bet_forms
      expect(bet_forms.count).to eq 2
      expect(bet_forms.first!.agent_id).to be_present
    end
  end
end