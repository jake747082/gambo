RSpec.describe Racing::Result do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  context "when get horses schedule reward result" do
    before {
      @result = Racing::Result.new(@racing_horses_schedule.id).reward!
      @reward_result, @reward_amount = @result
    }
    it "get result after run task" do
      expect(@reward_result.split(',').size).to eq(3)
    end
    it "result reward amount is the same as checkout reward amount" do
      Racing::Cashout.new(@racing_horses_schedule.id, @result).call!
      checkout_reward_amount = @racing_horses_schedule.racing_bet_forms.sum(:reward_amount)
      expect(@reward_amount).to eq(checkout_reward_amount)
    end
  end
end