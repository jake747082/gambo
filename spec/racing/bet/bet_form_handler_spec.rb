RSpec.describe Racing::Bet::BetFormHandler do
  include_context "site config"
  include_context "agent tree and machine"

  shared_examples "racing bet" do
    before(:all) { @sub_bet_form = @service.bet_form.racing_sub_bet_forms.last }
    subject { player }
    it "bet form should be created" do
      expect(@service.bet_form.id).to_not be_nil
      expect(@sub_bet_form.id).to_not be_nil
    end
    it "bet total amount should be 10" do
      expect(@service.bet_form.bet_total_credit).to eq 10
    end
    it "sub bet credit should be 10" do
      expect(@sub_bet_form.bet_credit).to eq 10
    end
    it "sub bet credit should be 10 * (1 + #odds)" do
      odds = @racing_horses.game_model.collections[@bet_type][@bet_number.to_s]
      expect(@sub_bet_form.max_reward_amount).to eq 10 * (1 + odds)
    end
    it "schedule bet total amount should be 10" do
      expect(@racing_horses_schedule.reload.bet_total_credit).to eq 10
    end
    it "schedule bet total count should be 1" do
      expect(@racing_horses_schedule.reload.total_bet_count).to eq 1
    end
    it "game bet total count should be 1" do
      expect(@racing_horses.reload.total_bet_count).to eq 1
    end
    it "player credit used 10 credit" do
      subject.reload
      expect(subject.credit_used).to eq 10
    end
  end

  context "When Machine Bet Spent 10 credit" do
    include_context "racing game"
    include_context "racing schedule"
    include_context "racing machine bet"

    it_behaves_like 'racing bet' do
      let!(:player) { machine }
    end

    it "bet form should belongs_to @player" do
      expect(@service.bet_form.machine).to eq machine
    end
  end

  context "When User Bet Spent 10 credit" do
    include_context "racing game"
    include_context "racing schedule"
    include_context "racing user bet"

    it_behaves_like 'racing bet' do
      let!(:player) { user }
    end

    it "bet form should belongs_to @player" do
      expect(@service.bet_form.user).to eq user
    end
  end
end