RSpec.describe Racing::Tasks::CreateSchedule do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"

  before { allow_any_instance_of(Racing::Tasks::CreateSchedule).to receive(:push_event) {true} }

  context "When Create Schedules" do

    it "Before create schedules, schedules not enough should be false" do
      expect(Racing::Tasks.new().send(:schedules_enough?)).to eq false
    end

    it "After create schedules, schedules enough should be true" do
      Racing::Tasks.new().create_schedules!
      expect(Racing::Tasks.new().send(:schedules_enough?)).to eq true
    end

    it "last not start schedule should be 3" do
      last_not_start_schedule = Racing::Tasks.new().send(:last_not_start_schedule)
      expect(last_not_start_schedule.id).to eq 4
    end
  end
end
