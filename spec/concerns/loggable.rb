require 'rails_helper'
require 'faker'

shared_examples "loggable" do
  include_context "agent tree and machine"
  let(:model) { described_class }

  before(:all) {
    @person = FactoryGirl.create(described_class.to_s.underscore.to_sym, username: Faker::Name.name, password: 'password', password_confirmation: 'password')
  }

  ### instance method

  it "#create_log" do
    @person.create_log('test', @person)
    expect(PublicActivity::Activity.first.key).to eq "#{model.to_s.underscore}.test"
  end

  it "#create_bank_account_log" do
    @person.create_bank_account_log('test2', @person, '', {}, @person)
    activity = PublicActivity::Activity.first
    expect(activity.key).to eq "#{model.to_s.underscore}.test2"
    expect(activity.recipient_id).to eq @person.id
    expect(activity.recipient_type).to eq model.to_s
  end

  it "#action_logs" do
    @user.create_log('test3', @person)
    log = @person.action_logs.first
    expect(log.key).to eq "user.test3"
    expect(log.owner_id).to eq @person.id
    expect(log.owner_type).to eq model.to_s
  end

  it "#change_logs" do
    @person.create_log('test4', @user)
    log = @person.change_logs.first
    expect(log.key).to eq "#{model.to_s.underscore}.test4"
    expect(log.trackable_id).to eq @person.id
    expect(log.trackable_type).to eq model.to_s
  end
end
