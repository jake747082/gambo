describe Gambo::Channel do

  it { expect(Gambo::Channel::LOBBY).to eq 'private-lobby-global' }
  it { expect(Gambo::Channel::AGENT).to eq 'private-agent-global' }
  it { expect(Gambo::Channel::CASH).to eq 'private-cash-global' }

  it { 
    expect(Gambo::Channel::GLOBAL).to include Gambo::Channel::LOBBY
    expect(Gambo::Channel::GLOBAL).to include Gambo::Channel::AGENT
    expect(Gambo::Channel::GLOBAL).to include Gambo::Channel::CASH
  }

  it { 
    expect(Gambo::Channel::PLAYER).to include Gambo::Channel::LOBBY
    expect(Gambo::Channel::PLAYER).to include Gambo::Channel::CASH
    expect(Gambo::Channel::PLAYER).to_not include Gambo::Channel::AGENT
  }
end