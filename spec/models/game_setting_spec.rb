describe GameSetting do
  let(:game_setting) { GameSetting.new(jackpot_md_range_begin: 10, jackpot_md_range_end: 20, jackpot_md_interval: 5, jackpot_sm_range_begin: 1, jackpot_sm_range_end: 10, jackpot_sm_interval: 1) }
  
  context "#vaild?" do
    it "should true" do
      expect(game_setting).to be_valid
    end
    it "#jackpot_md_range begin value over end value should false" do
      game_setting = GameSetting.new(jackpot_md_range_begin: 20, jackpot_md_range_end: 10, jackpot_md_interval: 5, jackpot_sm_range_begin: 1, jackpot_sm_range_end: 10, jackpot_sm_interval: 1)
      expect(game_setting).to_not be_valid
    end
    it "#jackpot_md interval value over diff value should false" do
      game_setting = GameSetting.new(jackpot_md_range_begin: 10, jackpot_md_range_end: 20, jackpot_md_interval: 15, jackpot_sm_range_begin: 1, jackpot_sm_range_end: 10, jackpot_sm_interval: 1)
      expect(game_setting).to_not be_valid
    end
    it "#jackpot_sm_range begin value over end value should false" do
      game_setting = GameSetting.new(jackpot_md_range_begin: 10, jackpot_md_range_end: 20, jackpot_md_interval: 5, jackpot_sm_range_begin: 10, jackpot_sm_range_end: 1, jackpot_sm_interval: 1)
      expect(game_setting).to_not be_valid
    end
    it "#jackpot_sm interval value over diff value should false" do
      game_setting = GameSetting.new(jackpot_md_range_begin: 10, jackpot_md_range_end: 20, jackpot_md_interval: 5, jackpot_sm_range_begin: 1, jackpot_sm_range_end: 10, jackpot_sm_interval: 10)
      expect(game_setting).to_not be_valid
    end
  end
  it "#jackpot_md_range_diff should be 10 (10~20)" do
    expect(game_setting.jackpot_md_range_diff).to eq 10
  end
  it "#jackpot_sm_range_diff should be 9 (1~10)" do
    expect(game_setting.jackpot_sm_range_diff).to eq 9
  end
  it "#jackpot_md_range should be (10..20)" do
    expect(game_setting.jackpot_md_range).to eq (10..20)
  end
  it "#jackpot_sm_range should be (1..10)" do
    expect(game_setting.jackpot_sm_range).to eq (1..10)
  end
  context "#jackpot_range_changed?" do
    it "#jackpot_md_range_begin change to 9 should true" do
      game_setting.jackpot_md_range_begin = 9
      expect(game_setting).to be_jackpot_range_changed
    end
    it "#jackpot_sm_range_end change to 9 should true" do
      game_setting.jackpot_sm_range_end = 9
      expect(game_setting).to be_jackpot_range_changed
    end
    it "#jackpot_md_interval change to 3 should true" do
      game_setting.jackpot_md_interval = 3
      expect(game_setting).to be_jackpot_range_changed
    end
  end
end