require 'rails_helper'
require 'concerns/loggable'

shared_examples "a agent with" do
  subject { test_agent }

  it "#agent_level should returns :director" do
    expect(subject.agent_level).to eq agent_level
    expect(subject.agent_level_cd).to eq agent_level_id
  end

  it "#agent_level_foreign_key_cond should return the a hash with {\#{agent_level}_id => id}" do
    expect(subject.send(:agent_level_foreign_key_cond)).to be_a_kind_of Hash
    expect(subject.send(:agent_level_foreign_key_cond)).to have_key agent_level.to_s.foreign_key
  end

  context "#is_belongs_to?" do
    it "will returns TRUE if the agent is the children of the pass in agent" do
      belongs_to_agents.each do |parent_agent|
        expect(subject.is_belongs_to?(parent_agent)).to be true
      end
    end

    it "will returns FALSE if the agent IS NOT the children of the pass in agent" do
      not_belongs_to_agents.each do |other_agent|
        expect(subject.is_belongs_to?(other_agent)).to be false
      end
    end
  end
end

describe Agent do
  it_behaves_like "loggable"

  it "#title should be \#{agent_level} - Eddie" do
    agent = Agent.new
    allow(agent).to receive(:human_agent_level) { 'Shareholder' }
    allow(agent).to receive(:nickname) { 'Eddie' }
    expect(agent.title).to eq "Shareholder - Eddie"
  end

  context "#credit_left with credit_max: 1000" do
    before(:all) { @agent = Agent.new(username: 'Eddie', credit_max: 1000) }
    it "should returns 1000 with credit_used: 0" do
      @agent.credit_used = 0
      expect(@agent.credit_left).to eq 1000
    end

    it "should returns 1 when credit_used: 999" do
      @agent.credit_used = 999
      expect(@agent.credit_left).to eq 1
    end
  end

  context "#credit_undispatched with credit_max: 1000" do
    before(:all) { @agent = Agent.new(username: 'Eddie', credit_max: 1000) }

    it "should returns 0 with credit_dispatched: 1000" do
      @agent.credit_dispatched = 1000
      expect(@agent.credit_undispatched).to eq 0
    end

    it "should returns 0 with credit_dispatched: 999" do
      @agent.credit_dispatched = 999
      expect(@agent.credit_undispatched).to eq 1
    end
  end

  context "#maintain_open? depends on maintain_open code present?" do
    it "should return true when maintain_open be present" do
      expect(Agent.new(maintain_code: 'YY293k')).to be_maintain_open
    end

    it "should return false when maintain_open be present" do
      expect(Agent.new(maintain_code: nil)).to_not be_maintain_open
    end
  end

  context "#open_api? depends on open_api secret_key present?" do
    it "should return true when open_api be present" do
      expect(Agent.new(secret_key: 'c8d566ed2387979b8bc1e1e1c8ab1a32')).to be_open_api
    end

    it "should return false when open_api be present" do
      expect(Agent.new(secret_key: nil)).to_not be_open_api
    end
  end

  context "with each level agent" do
    include_context "agent tree and machine"
    context "as a shareholder" do
      subject { shareholder }

      it_behaves_like 'a agent with' do
        let!(:test_agent) { shareholder }
        let!(:agent_level_id) { 0 }
        let!(:agent_level) { :shareholder }
        let!(:belongs_to_agents) { [] }
        let!(:not_belongs_to_agents) { [agent, director] }
      end

      it { expect(subject).to_not be_in_final_level }

      it { expect(subject).to be_in_top_level }
    end

    context "as a director" do
      subject { director }

      it_behaves_like 'a agent with' do
        let!(:test_agent) { director }
        let!(:agent_level_id) { 1 }
        let!(:agent_level) { :director }
        let!(:belongs_to_agents) { [shareholder] }
        let!(:not_belongs_to_agents) { [agent] }
      end

      it { expect(subject).to_not be_in_final_level }

      it { expect(subject).to_not be_in_top_level }
    end

    context "as a agent" do
      subject { agent }

      it_behaves_like 'a agent with' do
        let!(:test_agent) { agent }
        let!(:agent_level_id) { 2 }
        let!(:agent_level) { :agent }
        let!(:belongs_to_agents) { [shareholder, director] }
        let!(:not_belongs_to_agents) { [] }
      end

      it { expect(subject).to be_in_final_level }

      it { expect(subject).to_not be_in_top_level }
    end

    ### instance method

    describe "#user_change_logs" do
      it "should get this agent's users create log" do
        @user.create_log('test', @agent)
        log = @agent.user_change_logs('test').first
        expect(log.key).to eq "user.test"
        expect(log.trackable_id).to eq @user.id
        expect(log.trackable_type).to eq 'User'
      end
    end
  end
end
