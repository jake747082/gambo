describe Bonus do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  context "#should_done?" do
    context "When retry_count: 0" do
      let(:bonus) { Bonus.new(retry_count: 0) }

      it "and bonus_left: 0 should BE DONE" do
        allow(bonus).to receive(:bonus_left) { 0 }
        expect(bonus).to be_should_done
      end

      it "and bonus_left: -10 should BE DONE" do
        allow(bonus).to receive(:bonus_left) { -10 }
        expect(bonus).to be_should_done
      end

      it "and bonus_left: 10 should NOT BE DONE" do
        allow(bonus).to receive(:bonus_left) { 10 }
        expect(bonus).to_not be_should_done
      end
    end

    it "should be done When retry_count >= RETRY_COUNT_MAX" do
      bonus_1 = Bonus.new(bonus: 100, retry_count: Bonus::RETRY_COUNT_MAX + 1)
      bonus_2 = Bonus.new(bonus: 100, retry_count: Bonus::RETRY_COUNT_MAX)
      expect(bonus_1).to be_should_done
      expect(bonus_2).to be_should_done
    end
  end

  # jackpot_md_range_begin: 1000
  # jackpot_md_range_end: 3000
  # jackpot_md_interval: 200

  # jackpot_sm_range_begin: 200
  # jackpot_sm_range_end: 1000
  # jackpot_sm_interval: 50)

  context "When major bonus (1000-3000)" do
    let(:bonus) { machine.bonuses.new(bonus_type: :major) }

    it "should NOT have error on bonus with valid bonus" do
      bonus.bonus = 1500
      expect(bonus).to be_valid
    end

    it "should NOT have error on bonus with valid bonus (2 decimal)" do
      bonus.bonus = 1500.99
      expect(bonus).to be_valid
    end

    it "should have error on bonus with OVER bonus limit 3000" do
      bonus.bonus = 3001
      expect(bonus).to_not be_valid
      expect(bonus.errors).to have_key(:bonus)
    end

    it "should have error on bonus with LESS THAN bonus limit 1000" do
      bonus.bonus = 999
      expect(bonus).to_not be_valid
      expect(bonus.errors).to have_key(:bonus)
    end
  end

  context "When minor bonus (200-1000)" do
    let(:bonus) { machine.bonuses.new(bonus_type: :minor) }

    it "should NOT have error on bonus with valid bonus" do
      bonus.bonus = 500
      expect(bonus).to be_valid
    end

    it "should NOT have error on bonus with valid bonus (2 decimal)" do
      bonus.bonus = 500.99
      expect(bonus).to be_valid
    end

    it "should have error on bonus with OVER bonus limit 1100" do
      bonus.bonus = 1100
      expect(bonus).to_not be_valid
      expect(bonus.errors).to have_key(:bonus)
    end

    it "should have error on bonus with LESS THAN bonus limit 199" do
      bonus.bonus = 199
      expect(bonus).to_not be_valid
      expect(bonus.errors).to have_key(:bonus)
    end
  end

  context "When total jackpot" do
    let(:bonus) { machine.bonuses.new(bonus_type: :total) }

    context "without undispatched bonus" do
      before {
        allow(Bonus).to receive(:undispatched_total_jackpot) { 0 }
        allow(SlotMachine).to receive(:total_jackpot) { 1000 }
      }

      it "should allow when bonus LESS than total jackpot" do
        bonus.bonus = 999
        expect(bonus).to be_valid
      end

      it "should NOT have error on bonus with valid bonus (2 decimal)" do
        bonus.bonus = 999.99
        expect(bonus).to be_valid
      end

      it "should NOT allow when bonus OVER than total jackpot" do
        bonus.bonus = 1200
        expect(bonus).to_not be_valid
      end
    end

    context "with other undispatched bonus" do
      before {
        allow(Bonus).to receive(:undispatched_total_jackpot) { 200 }
        allow(SlotMachine).to receive(:total_jackpot) { 1000 }
      }

      it "should allow bonus LESS then 800" do
        bonus.bonus = 800
        expect(bonus).to be_valid
      end

      it "should NOT allow bonus OVER then 800" do
        bonus.bonus = 801
        expect(bonus).to_not be_valid
      end
    end
  end
end
