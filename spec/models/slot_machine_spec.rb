require 'rails_helper'

describe SlotMachine do
  context "#valid?" do
    it "should true" do
      slot_machine = SlotMachine.new(win_speed: 10)
      expect(slot_machine).to be_valid
    end
    it "win_speed over range (0~100) should false" do
      slot_machine = SlotMachine.new(win_speed: 110)
      expect(slot_machine).to_not be_valid
    end
  end
  it "#current_jackpot should return value (not over 2 decimal)" do
    slot_machine = SlotMachine.new(win_speed: 10, jackpot: 1.999)
    expect(slot_machine.current_jackpot).to eq 1.99
  end
end