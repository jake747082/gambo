require 'rails_helper'

describe AgentSubAccount do
	include_context "agent tree and machine"

  it "#system_id should be \#{username} - (\#{nickname})" do
    sub_agent = AgentSubAccount.new(username: 'sub_agent_is_me', nickname: 'SubAgentIsMe')
    expect(sub_agent.system_id).to eq "sub_agent_is_me (SubAgentIsMe)"
  end

  it "sub agent belongs to agent " do
    sub_agent = @shareholder.accounts.new
    expect(sub_agent.agent_id).to eq 1
    expect(sub_agent.human_agent_level).to eq 'Shareholder'
  end
end