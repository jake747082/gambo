require 'rails_helper'
require 'concerns/loggable'

describe Admin do
  it_behaves_like "loggable"

  ### create validate

  describe "Create a admin" do
    describe "When data is correct" do
      it "should be create successful" do
        attrs = attributes_for(:new_manager)
        admin = Admin.create(attrs)
        expect(admin.errors.messages).to be_empty
        expect(Admin.last.username).to eq "manager"
      end
    end
    describe "When password is empty" do
      it "should be create fail" do
        attrs = attributes_for(:incorrect_manager)
        admin = Admin.create(attrs)
        expect(admin.errors.messages).not_to be_empty
        expect(Admin.last.username).not_to eq "incorrect_manager"
      end
    end
    describe "When role is nil" do
      it "should be create fail" do
        attrs = attributes_for(:incorrect_manager2)
        admin = Admin.create(attrs)
        expect(admin.errors.messages).not_to be_empty
        expect(Admin.last.username).not_to eq "incorrect_manager"
      end
    end
  end

  ### instance method

  describe "#system_id" do
    it "should be '[Admin] \#{nickname}'" do
      admin = build(:admin)
      expect(admin.system_id).to eq "[Admin] Manager"
    end
  end

  describe "#title" do
    it "should be \#{username} (\#{nickname})" do
      admin = build(:admin)
      expect(admin.title).to eq "manager (Manager)"
    end
  end

  describe "#human_role" do
    describe "when role == 0" do
      it "should be 'Admin Manager'" do
        admin = build(:admin, role: 0)
        expect(admin.human_role).to eq "Admin Manager"
      end
    end
    describe "when role == 1" do
      it "should be 'Report Manager'" do
        admin = build(:admin, role: 1)
        expect(admin.human_role).to eq "Report Manager"
      end
    end
    describe "when role == 2" do
      it "should be 'Game Manager'" do
        admin = build(:admin, role: 2)
        expect(admin.human_role).to eq "Game Manager"
      end
    end
  end

  describe "#active_for_authentication?" do
    describe "when admin is unlocked" do
      it "should be true" do
        admin = build(:admin, lock: false)
        expect(admin.active_for_authentication?).to be true
      end
    end

    describe "when admin is locked" do
      it "should be false" do
        admin = build(:admin, lock: true)
        expect(admin.active_for_authentication?).to be false
      end
    end
  end
end
