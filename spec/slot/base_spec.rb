require "games/test_slot"

RSpec.describe TestSlot do

  it { expect(TestSlot.width).to eq(5) }
  it { expect(TestSlot.height).to eq(3) }

  context "Icons Check" do
    it { expect(TestSlot.icon_all.length).to eq(10) }
    it { expect(TestSlot.icon_payouts.length).to eq(3) }
    it { expect(TestSlot.icon_wildcard.length).to eq(1) }
    it { expect(TestSlot.icon_bonus.length).to eq(2) }
    it { expect(TestSlot.icon_event.length).to eq(2) }
    it { expect(TestSlot.icon_scatter.length).to eq(2) }
  end

  context "Position Check" do
    it { expect(TestSlot.position_to_x_y(1)).to eq(x: 0, y: 0) }
    it { expect(TestSlot.position_to_x_y(2)).to eq(x: 1, y: 0) }
    it { expect(TestSlot.position_to_x_y(3)).to eq(x: 2, y: 0) }
    it { expect(TestSlot.position_to_x_y(4)).to eq(x: 3, y: 0) }
    it { expect(TestSlot.position_to_x_y(5)).to eq(x: 4, y: 0) }
    it { expect(TestSlot.position_to_x_y(6)).to eq(x: 0, y: 1) }
    it { expect(TestSlot.position_to_x_y(7)).to eq(x: 1, y: 1) }
    it { expect(TestSlot.position_to_x_y(8)).to eq(x: 2, y: 1) }
    it { expect(TestSlot.position_to_x_y(9)).to eq(x: 3, y: 1) }
    it { expect(TestSlot.position_to_x_y(10)).to eq(x: 4, y: 1) }
    it { expect(TestSlot.position_to_x_y(11)).to eq(x: 0, y: 2) }
    it { expect(TestSlot.position_to_x_y(12)).to eq(x: 1, y: 2) }
    it { expect(TestSlot.position_to_x_y(13)).to eq(x: 2, y: 2) }
    it { expect(TestSlot.position_to_x_y(14)).to eq(x: 3, y: 2) }
    it { expect(TestSlot.position_to_x_y(15)).to eq(x: 4, y: 2) }
  end

  context "Lines Check" do
    # it { expect(TestSlot.lines.count).to eq 10 }
    it "#6 mutex #7, #8, but include self" do
      expect(TestSlot.line_mutex(6)).to include 7, 8
      expect(TestSlot.line_mutex(7)).to include 6
      expect(TestSlot.line_mutex(8)).to include 6
      expect(TestSlot.line_mutex(6)).not_to include 6
    end

    it "#7 NOT mutex #8 and self" do
      expect(TestSlot.line_mutex(7)).to_not include 7, 8
      expect(TestSlot.line_mutex(8)).to_not include 7
    end

    it "#2 NOT mutex #5 and self" do
      expect(TestSlot.line_mutex(2)).to be_empty
      expect(TestSlot.line_mutex(5)).to be_empty
    end

    it "#1 mutex #4, but not self" do
      expect(TestSlot.line_mutex(1)).to include 4
      expect(TestSlot.line_mutex(4)).to include 1
      expect(TestSlot.line_mutex(1)).not_to include 1
      expect(TestSlot.line_mutex(4)).not_to include 4
    end
  end
end