RSpec.describe Slot::Spin::BetFormHandler do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  context "When Machine Spin Spent 250 credit (10 * 25)" do
    pending
    # include_context "slot spin"
    # before(:all) {
    #   @total_dispatched_profit = @service.bet_form.shareholder_owe_parent +
    #   @service.bet_form.shareholder_win_amount +
    #   @service.bet_form.director_win_amount +
    #   @service.bet_form.agent_win_amount
    # }
    #
    # it "bet form should be created" do
    #   expect(@service.bet_form.id).to_not be_nil
    # end
    # it "bet line should be 25" do
    #   expect(@service.bet_form.bet_line).to eq 25
    # end
    # it "bet credit should be 10" do
    #   expect(@service.bet_form.bet_credit).to eq 10
    # end
    # it "bet total amount should be 250" do
    #   expect(@service.bet_form.bet_total_credit).to eq 250
    # end
    # it "bet form should belongs_to @machine" do
    #   expect(@service.bet_form.machine).to eq @machine
    # end
    # it "bet machine belongs to agent" do
    #   expect(@service.bet_form.agent).to eq @machine.agent
    # end
    # it "bet machine belongs to director" do
    #   expect(@service.bet_form.director).to eq @machine.agent.parent
    # end
    # it "bet machine belongs to shareholder" do
    #   expect(@service.bet_form.shareholder).to eq @machine.agent.parent.parent
    # end
    # it "total dispatched amount * -1 should == user credit diff" do
    #   expect(@total_dispatched_profit * -1).to eq @service.bet_form.machine_credit_diff
    # end
  end

  context "When Machine Spin Get Free Spin" do
    pending
    # let(:service) {
    #   service = Slot::Spin.new(SlotMachine.find(2), Machine.first!)
    #   allow(service).to receive(:free_spin?) { true }
    #   service.spin!(10, 25)
    #   service
    # }
    # let(:bet_form) { service.bet_form }
    #
    # it { expect(service).to be_free_spin }
    #
    # it { expect(bet_form).to be_persisted }
    # it { expect(bet_form.free_spin_count).to be > 0 }
    # it { expect(bet_form.free_spin_used).to eq 0 }
    # it { expect(bet_form.free_spin_token).to_not be_nil }
  end
end
