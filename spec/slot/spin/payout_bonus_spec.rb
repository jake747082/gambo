shared_context "spin service with payout bonus" do
  before(:all) {
    @machine.update!(credit_max: 1000, credit_used: 0)
    @bonus = @machine.bonuses.create!(bonus_type: :payout, bonus: 300)
    @service = Slot::Spin.new(@slot_mayan, @machine)
  }
end

describe Slot::Spin do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  let(:spin_service) { @service }
  let(:bonus) { @bonus.reload }
  let(:bet_form) { spin_service.bet_form }

  context "when Machine Spin with PAYOUT bonus" do
    pending
    # include_context "spin service with payout bonus"
    #
    # it { expect(spin_service.spin!(0.1, 25)).to eq true }
    #
    # it { expect(spin_service.jackpot_bonus).to be_nil }
    # it { expect(spin_service.jackpot_open?).to eq false }
    #
    # # TODO: 待補上 bonus done & bet form checker
    #
    # context "History" do
    #   let(:history) { spin_service.history.reload }
    #   let(:company_lose_amount) { spin_service.reward_amount - 2.5 }
    #
    #   it "preparing_CYCLE should increase" do
    #     expect(history.preparing_xs).to eq 0
    #     expect(history.preparing_sm).to eq 0
    #     expect(history.preparing_md).to eq 0
    #     expect(history.preparing_lg).to eq 0
    #     expect(history.preparing_xl).to eq 0
    #   end
    #
    #   it "dispatching_CYCLE should keep 0" do
    #     expect(history.dispatching_xs).to eq 0
    #     expect(history.dispatching_sm).to eq 0
    #     expect(history.dispatching_md).to eq 0
    #     expect(history.dispatching_lg).to eq 0
    #     expect(history.dispatching_xl).to eq 0
    #   end
    # end
  end

  context "When spin with bonus mode but user first time losing money" do
    pending
    # before { allow_any_instance_of(Slot::Spin).to receive(:user_win_credit) { 0 } }
    #
    # context "when in_special_event? = false" do
    #   include_context "spin service with payout bonus"
    #   it "should increase payout_bonus retry counter" do
    #     allow_any_instance_of(Slot::Spin).to receive(:in_special_event?) { false }
    #     expect(spin_service.spin!(0.1, 25)).to be true
    #     expect(bonus.retry_count).to eq 1
    #   end
    # end
    #
    # context "when in_special_event? = true" do
    #   include_context "spin service with payout bonus"
    #   it "should NOT increase payout_bonus retry counter" do
    #     allow_any_instance_of(Slot::Spin).to receive(:in_special_event?) { true }
    #     expect(spin_service.spin!(0.1, 25)).to be true
    #     expect(bonus.retry_count).to eq 0
    #   end
    # end
  end

  context "When bonus should done" do
    pending
    # include_context "spin service with payout bonus"
    #
    # it "should setup bonus to done" do
    #   allow_any_instance_of(Bonus).to receive(:should_done?) { true }
    #
    #   expect(spin_service.spin!(0.1, 25)).to be true
    #   expect(bonus).to be_done
    # end
  end
end
