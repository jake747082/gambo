RSpec.describe Slot::Spin do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  context "when Machine Spin with PRIMARY JACKPOT bonus" do
    pending
    # before(:all) {
    #   @slot_mayan.update!(jackpot: 10000)
    #   @agent.update!(credit_used: 0, credit_max: 20000)
    #   @machine.update!(credit_used: 0, credit_max: 250)
    #
    #   @bonus = @machine.bonuses.create!(bonus_type: :primary, bonus: 10000)
    #   @spin_service = Slot::Spin.new(@slot_mayan, @machine)
    # }
    #
    # let(:bonus) { @bonus.reload }
    # let(:spin_service) { @spin_service }
    # let(:dispatched_jackpot_amount) { 10000 }
    #
    # context "Spin Service" do
    #
    #   it "should spin successful" do
    #     allow(spin_service).to receive(:dispatched_spin_amount) { 0 }
    #     expect(spin_service.spin!(10, 25)).to eq true
    #   end
    #
    #   it "#jackpot_open? should be true" do
    #     expect(spin_service.jackpot_open?).to eq true
    #   end
    #
    #   it "#jackpot_bonus should be assigned bonus" do
    #     expect(spin_service.jackpot_bonus).to eq bonus
    #   end
    #
    # end
    #
    # context "Finance" do
    #   let(:bet_cost) { 250 }
    #   let(:company_win_amount) { (dispatched_jackpot_amount - bet_cost) * -1 }
    #
    #   context "Agent" do
    #     let(:shop_agent) { spin_service.bet_form.agent.reload }
    #     let(:shop_agent_win_amount) { company_win_amount * 0.5 }
    #
    #     it "#credit_used should eq 50% of company_lose_amount" do
    #       expect(shop_agent.credit_used).to eq shop_agent_win_amount
    #     end
    #
    #     it "#credit_left should eq (20000 - 50% of company_lose_amount)" do
    #       expect(shop_agent.credit_left).to eq (20000 - shop_agent_win_amount).to_f
    #     end
    #   end
    #
    #   context "Bet Form" do
    #     let(:bet_form) { spin_service.bet_form.reload }
    #
    #     it "should be assigned bonus has_many associations" do
    #       expect(bet_form).to eq bonus.slot_bet_form
    #     end
    #
    #     it "#jackpot_type should be :primary" do
    #       expect(bet_form.jackpot_type).to eq :primary
    #     end
    #
    #     it "#agent_win_amount should eq 50% of company_win_amount" do
    #       expect(bet_form.agent_win_amount.round(2).to_f).to eq (company_win_amount * 0.5).round(2).to_f
    #     end
    #
    #     it "#agent_owe_parent should eq 50% of company_win_amount" do
    #       expect(bet_form.agent_owe_parent.round(2).to_f).to eq (company_win_amount * 0.5).round(2).to_f
    #     end
    #
    #     it "#director_win_amount should eq 30% of company_win_amount" do
    #       expect(bet_form.director_win_amount.round(2).to_f).to eq (company_win_amount * 0.3).round(2).to_f
    #     end
    #
    #     it "#director_owe_parent should eq 20% of company_win_amount" do
    #       expect(bet_form.director_owe_parent.round(2).to_f).to eq (company_win_amount * 0.2).round(2).to_f
    #     end
    #
    #     it "#shareholder_win_amount should eq 15% of company_win_amount" do
    #       expect(bet_form.shareholder_win_amount.round(2).to_f).to eq (company_win_amount * 0.15).round(2).to_f
    #     end
    #
    #     it "#shareholder_owe_parent should eq 5% of company_win_amount" do
    #       expect(bet_form.shareholder_owe_parent.round(2).to_f).to eq (company_win_amount * 0.05).round(2).to_f
    #     end
    #   end
    # end
    #
    # context "Bonus" do
    #   it "#done? should be true" do
    #     expect(bonus).to be_done
    #   end
    # end
    #
    # context "Machine" do
    #   let(:machine_credit_left) { @machine.reload.credit_left }
    #
    #   it "should equal #dispatched_jackpot_amount" do
    #     expect(machine_credit_left).to eq dispatched_jackpot_amount
    #   end
    # end
    #
    # context "Slot Machine" do
    #   let(:mayan_jackpot) { @slot_mayan.reload.jackpot }
    #   let(:expect_jackpot) {
    #     jackpot_cal = (dispatched_jackpot_amount - spin_service.dispatched_jackpot_amount + spin_service.incremented_jackpot).round(2).to_f
    #   }
    #   it "should be decrument slot machine jackpot" do
    #     expect(mayan_jackpot).to eq expect_jackpot
    #   end
    # end
  end

  context "When Machine Spin and dispatched bonus > jackpot amount" do
    pending
    # before(:all) {
    #   @slot_mayan.update!(jackpot: 100)
    #   @bonus = @machine.bonuses.create!(bonus_type: :primary, bonus: 1000)
    #   @service = Slot::Spin.new(@slot_mayan, @machine)
    # }
    # let(:spin_service) { @service }
    # let(:bonus) { @bonus.reload }
    # let(:slot_mayan) { @slot_mayan.reload }
    #
    # it { expect(spin_service.spin!(10, 25)).to eq true }
    #
    # it { expect(bonus).to be_done }
    #
    # it { expect(bonus.bonus_dispatched).to eq 100 }
    #
    # it { expect(spin_service.dispatched_jackpot_amount).to eq 100 }
    #
    # it { expect(slot_mayan.jackpot.to_f).to eq spin_service.incremented_jackpot }
  end

  (1..20).each do |times|
    context "When Machine Spin with a total jackpot #{times}" do
      pending
      # before(:all) {
      #   Bonus.delete_all
      #   SlotMachine.all.each do |slot_machine|
      #     slot_machine.update!(jackpot: rand(1.01..1500).round(5))
      #   end
      #   @before_total_jackpot = SlotMachine.sum(:jackpot).round(5)
      #   @service = Slot::Spin.new(@slot_mayan, @machine)
      #   @dispatched_jackpot = rand(1..@before_total_jackpot).to_i
      #   @bonus = @machine.bonuses.create!(bonus_type: :total, bonus: @dispatched_jackpot)
      # }
      #
      # let(:bonus) { @bonus.reload }
      #
      # it "should distribute reduce jackpot in each slot machine" do
      #   expect(@service.spin!(10, 25)).to be true
      #   after_total_jackpot = SlotMachine.sum(:jackpot).round(5)
      #
      #   expect(after_total_jackpot).to eq (@before_total_jackpot - @dispatched_jackpot + @service.incremented_jackpot).round(5)
      # end
      #
      # it { expect(bonus).to be_done }
    end
  end
end
