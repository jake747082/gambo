RSpec.describe Slot::Spin::BetFormHandler do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"
  include_context "slot spin"

  before(:all) {
    @service.bet_form.assign_free_spin(3, 2) # count, mutilple
    @service.bet_form.save!
    @free_spin_params = { free_id: @service.bet_form.id, free_token: @service.bet_form.free_spin_token }
  }

  let(:free_spin_service) { @service }
  let(:free_spin_base_form) { free_spin_service.bet_form }

  context "When provide correct free spin params" do
    pending
    # before(:all) { @machine.update!(credit_max: 2000, credit_used: 0) }
    #
    #
    # (1..3).each do |spin_time|
    #   context "and free spin not done => ##{spin_time}/3" do
    #     before(:all) { @avaliable_spin_service = Slot::Spin.new(@slot_mayan, @machine) }
    #     let(:spin_service) { @avaliable_spin_service }
    #
    #     it "should allow free spin and return true" do
    #       allow(spin_service).to receive(:dispatched_spin_amount) { 0 }
    #       expect(spin_service.free_spin!(@free_spin_params)).to be true
    #     end
    #
    #     it "should not change credit (equal 2000) in machine" do
    #       expect(spin_service.player.credit_left.to_f).to be 2000.to_f
    #     end
    #
    #     it "should be free spin mode" do
    #       expect(spin_service).to be_free_spin_mode
    #     end
    #
    #     context "#bet_form" do
    #       let(:bet_form) { spin_service.bet_form }
    #       let(:free_spin_base_form) { spin_service.free_spin_base_form }
    #
    #       it "#free_spin_base relation should eq free_spin_base_form" do
    #         expect(bet_form.free_spin_base.id).to eq free_spin_base_form.id
    #       end
    #
    #       %w(bet_credit bet_line).each do |column|
    #         it "##{column} should eq to free_spin_base_form##{column}" do
    #           expect(bet_form.send(column)).to eq free_spin_base_form.send(column)
    #         end
    #       end
    #
    #       it "#bet_total_credit should eq 0" do
    #         expect(bet_form.bet_total_credit).to eq 0
    #       end
    #     end
    #
    #     context "#free_spin_base_form" do
    #       let(:free_spin_base_form) { spin_service.free_spin_base_form }
    #
    #       it "#free_spin_used should incrument FREE_SPIN_USED to #{spin_time}" do
    #         free_spin_base_form.reload
    #         expect(free_spin_base_form.free_spin_used).to be spin_time
    #       end
    #
    #       it "#id should equal params free_id" do
    #         expect(free_spin_base_form.id).to be @free_spin_params[:free_id]
    #       end
    #     end
    #
    #     context "#history" do
    #       let(:before_history) { free_spin_service.history }
    #       let(:history) { spin_service.history.reload }
    #       it "should not increase for preparing_CYCLE" do
    #         expect(history.preparing_xs).to eq before_history.preparing_xs
    #         expect(history.preparing_sm).to eq before_history.preparing_sm
    #         expect(history.preparing_md).to eq before_history.preparing_md
    #         expect(history.preparing_lg).to eq before_history.preparing_lg
    #         expect(history.preparing_xl).to eq before_history.preparing_xl
    #       end
    #     end
    #   end
    # end
    #
    # context "but free spin already done" do
    #   before(:all) { @unavaliable_spin_service = Slot::Spin.new(@slot_mayan, @machine) }
    #   let(:spin_service) { @unavaliable_spin_service }
    #
    #   it "should raise error" do
    #     expect {
    #       free_spin_base_form.update!(free_spin_count: free_spin_base_form.free_spin_used)
    #       spin_service.free_spin!(@free_spin_params)
    #     }.to raise_error Slot::Spin::EventFree::NotFoundFreeSpinBetForm
    #   end
    #
    #   it "#free_spin_count should keep same as #free_spin_used" do
    #     expect(free_spin_base_form.free_spin_count).to eq free_spin_base_form.free_spin_used
    #   end
    # end
    #
    # context "Get a free spin chance" do
    #   before(:all) {
    #     @machine.update!(credit_max: 2000, credit_used: 0)
    #     @service = Slot::Spin.new(@slot_mayan, @machine)
    #   }
    #
    #   let(:spin_service) { @service }
    #
    #   it "should be response true" do
    #     allow(Integer).to receive(:in) { true }
    #     expect(spin_service.spin!(10, 25)).to be true
    #   end
    #
    #   context "#events" do
    #     let(:events) { service.events }
    #     let(:free_spin) { service.events[:free_spin] }
    #
    #     it "should be hash" do
    #       expect(events).to be_kind_of Hash
    #     end
    #
    #     it "#free_spin should be hash and has keys (:id, :token, :total, :count, :used, :positions)" do
    #       expect(free_spin).to be_kind_of Hash
    #       expect(free_spin).to have_key :id
    #       expect(free_spin).to have_key :token
    #       expect(free_spin).to have_key :total
    #       expect(free_spin).to have_key :count
    #       expect(free_spin).to have_key :used
    #       expect(free_spin).to have_key :positions
    #     end
    #   end
    # end
  end
end
