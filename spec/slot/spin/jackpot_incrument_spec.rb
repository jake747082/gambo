RSpec.describe Slot::Spin do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"
  include_context "slot spin"

  let(:slot_mayan) { @slot_mayan.reload }

  context "When Machine Spin Spent 250 credit (10 * 25)" do
    pending
    #
    # let(:spin_service) { @service }
    # let(:expect_incremented_jackpot) { (250 * @game_setting.jackpot_accumulate_rate).round(2) }
    # let(:expect_jackpot) { expect_incremented_jackpot }
    #
    # it "slot_mayan_jackpot should be 1000 + (250 * jackpot_accumulate_rate)" do
    #   expect(slot_mayan.jackpot).to eq expect_jackpot
    # end
    # it "service increment jackpot should eq (250 * jackpot_accumulate_rate)" do
    #   expect(spin_service.incremented_jackpot).to eq expect_incremented_jackpot
    # end
  end

  context "When Free Spin Spent 0 credit (10 * 25)" do
    pending
    # before(:all) {
    #   @slot_mayan.update(jackpot: 1000)
    #   @service.bet_form.assign_free_spin(3, 2) # count, mutilple
    #   @service.bet_form.save!
    #   @spin_service = Slot::Spin.new(@slot_mayan, @machine)
    #   @spin_service.free_spin!(free_id: @service.bet_form.id, free_token: @service.bet_form.free_spin_token)
    # }
    #
    # context "Spin Service" do
    #   let(:spin_service) { @spin_service }
    #
    #   it "#incremented_jackpot should eq 0" do
    #     expect(spin_service.incremented_jackpot).to eq 0
    #   end
    # end
    #
    # context "Slot Mayan" do
    #   it "#jackpot should NOT be incremented" do
    #     expect(slot_mayan.jackpot).to eq 1000
    #   end
    # end
  end
end
