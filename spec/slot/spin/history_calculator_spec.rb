shared_context "slot history calculator" do
  before(:all) {
    SlotMachineHistory.delete_all
    @calculator = Slot::Spin::HistoryCalculator.new(@slot_mayan, @machine, 1)
  }
  let(:calculator) { @calculator }
  let(:history) { calculator.history.reload }
end

describe Slot::Spin::HistoryCalculator do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  context "#update_machine_history!" do
    let(:reward_amount) { 0 }
    context "When lose 1 credit" do
      include_context "slot history calculator"

      it "should return true" do
        expect(calculator.update_machine_history!(reward_amount)).to be true
        expect(history.save!).to be true
      end

      it "#total_bet_count should eq 1" do
        expect(history.total_bet_count).to eq 1
      end

      it "#history should increase preparing credit for each cycle's ratio" do
        expect(history.preparing_xs).to eq 0.65
        expect(history.preparing_sm).to eq 0.17
        expect(history.preparing_md).to eq 0.08
        expect(history.preparing_lg).to eq 0.04
        expect(history.preparing_xl).to eq 0.01
      end

      it "#history should NOT deduct from dispatching" do
        expect(history.dispatching_xs).to eq 0
        expect(history.dispatching_sm).to eq 0
        expect(history.dispatching_md).to eq 0
        expect(history.dispatching_lg).to eq 0
        expect(history.dispatching_xl).to eq 0
      end
    end

    context "when #preparing_CYCLE_SIZE >= slot_machine#lose_trigger_amount_CYCLE_SIZE" do
      include_context "slot history calculator"
      before(:all) {
        prepare_history_params = {
          preparing_xs: @slot_mayan.lose_trigger_amount_xs,
          preparing_sm: @slot_mayan.lose_trigger_amount_sm - 0.18,
          preparing_md: @slot_mayan.lose_trigger_amount_md - 0.09,
          preparing_lg: @slot_mayan.lose_trigger_amount_lg - 0.04,
          preparing_xl: @slot_mayan.lose_trigger_amount_xl - 0.02
        }
        @calculator.history.update!(prepare_history_params)
      }

      it "should return true" do
        expect(calculator.update_machine_history!(reward_amount)).to be true
        expect(history.save!).to be true
      end

      it { expect(history.total_bet_count).to eq 1 }

      it "#history should increase preparing credit for each cycle's ratio" do
        expect(history.preparing_xs.to_f).to eq 0
        expect(history.preparing_sm.to_f).to eq (@slot_mayan.lose_trigger_amount_sm - 0.01)
        expect(history.preparing_md.to_f).to eq (@slot_mayan.lose_trigger_amount_md - 0.01)
        expect(history.preparing_lg.to_f).to eq 0
        expect(history.preparing_xl.to_f).to eq (@slot_mayan.lose_trigger_amount_xl - 0.01)
      end

      it "#history should increase win amount" do
        expect(history.dispatching_xs).to eq @slot_mayan.lose_trigger_amount_xs + 0.65
        expect(history.dispatching_sm).to eq 0
        expect(history.dispatching_md).to eq 0
        expect(history.dispatching_lg).to eq @slot_mayan.lose_trigger_amount_lg
        expect(history.dispatching_xl).to eq 0
      end
    end
  end
end