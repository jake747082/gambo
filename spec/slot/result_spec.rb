RSpec.describe Slot::Result do
  context "when assign a grids with wildcard" do
    before(:all) {
      @result = Slot::Result.new(Slot::MayanEclipse).setup_coin(10, (1..25).to_a)
      @result.instance_eval {
        @grids = [ nil,
                   2, 2, 3, 3, 3,
                   1, 4, 1, 4, 9,
                   5, 5, 6, 2, 8 ]
      }
    }
    it "line #6 should count: 4, icon: 2" do
      line_counter = @result.line_linked_icon(6)
      expect(line_counter.icon.id).to eq(2)
      expect(line_counter.count).to eq(4)
    end

    it "line #17 should count: 3, icon: 5" do
      line_counter = @result.line_linked_icon(17)
      expect(line_counter.icon.id).to eq(5)
      expect(line_counter.count).to eq(3)
    end

    it "line #1 should count: 4, icon: 4" do
      line_counter = @result.line_linked_icon(1)
      expect(line_counter.icon.id).to eq(4)
      expect(line_counter.count).to eq(4)
    end
  end

  context "when assign a line with event card" do
    before(:all) {
      @result = Slot::Result.new(Slot::AmazingSafari).setup_coin(10, (1..25).to_a)
      @result.instance_eval {
        @grids = [ nil,
                   1, 1, 1, 1, 1,
                   2, 3, 2, 1, 3,
                   1, 3, 3, 3, 3 ]
      }
    }
    it "line #1 should count: 3, icon: 3" do
      line_counter = @result.line_linked_icon(1)
      expect(line_counter.icon.id).to eq(3)
      expect(line_counter.count).to eq(3)
    end

    it "line #2 should be nil" do
      line_counter = @result.line_linked_icon(2)
      expect(line_counter).to be_nil
    end

    it "line #3 should be nil" do
      line_counter = @result.line_linked_icon(3)
      expect(line_counter).to be_nil
    end
  end

  context "when assign a line but not bet this line" do
    before(:all) {
      @result = Slot::Result.new(Slot::MayanEclipse).setup_coin(10, [1])
      @result.instance_eval {
        @grids = [ nil,
                   2, 2, 2, 2, 2,
                   3, 3, 3, 3, 3,
                   4, 4, 4, 4, 4 ]
      }
      @result.reward

      @win_line_ids = @result.win_lines.map { |l| l.line_id }
    }

    it "should get line #1" do
      expect(@win_line_ids).to include 1
    end

    it "should NOT get line #2" do
      expect(@win_line_ids).to_not include 2
    end

    it "should NOT get line #3" do
      expect(@win_line_ids).to_not include 3
    end

    it "should NOT get line #20" do
      expect(@win_line_ids).to_not include 20
    end

    it "should NOT get line #21" do
      expect(@win_line_ids).to_not include 21
    end
  end

  context "when assign a line with bonus and wildcard" do
    before(:all) {
      @result = Slot::Result.new(Slot::AmazingSafari).setup_coin(10, (1..25).to_a)
      @result.instance_eval {
        @grids = [nil,
                  1, 2, 3, 3, 3,
                  2, 1, 3, 3, 3,
                  1, 1, 1, 1, 1 ]
      }
      @result.reward
    }

    it "line #1 should be nil" do
      line_counter = @result.line_linked_icon(1)
      expect(line_counter).to be_nil
    end

    it "line #2 should be nil" do
      line_counter = @result.line_linked_icon(2)
      puts line_counter.inspect
      expect(line_counter).to be_nil
    end
  end
end
