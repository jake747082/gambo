describe Slot::ZhaoCiaJinBao do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  it_behaves_like 'slot machine with predefine strategy'
  it_behaves_like 'slot machine with lines'

  context "When Assigned dispatched_spin_amount: 8, and got bonus (2 linked)" do
    it_behaves_like 'a spin with response bonus, machines, bet_form' do
      let(:dispatched_amount) { 8 }
      let(:linked_bonus_count) { 2 }
      let(:bonus_amount) { 5 }

      it_behaves_like 'a spin without free spin'
      it_behaves_like 'a spin with icons in each reels'
    end
  end

  context "When Assigned dispatched_spin_amount: 57, and got bonus (4 linked)" do
    it_behaves_like 'a spin with response bonus, machines, bet_form' do
      let(:dispatched_amount) { 57 }
      let(:linked_bonus_count) { 4 }
      let(:bonus_amount) { 50 }
      
      it_behaves_like 'a spin with free spin'
      it_behaves_like 'a spin with icons in each reels'
    end
  end
end