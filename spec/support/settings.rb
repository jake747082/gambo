# database
require 'active_record'
config = {
  host: 'localhost',
  username: 'root',
  password: '',
  pool: 10,
  adapter: 'mysql2',
  encoding: 'utf8'
}
database = "gambo_#{ENV['TEST_ENV_NUMBER']}"
config = config.merge(username: 'testing_runner', password: 'testing_runner') if ENV['CI']
ActiveRecord::Base.establish_connection(config)
ActiveRecord::Base.connection.drop_database(database)
ActiveRecord::Base.connection.create_database(database)
ActiveRecord::Base.establish_connection(config.merge(database: database))
ActiveRecord::Migration.verbose = true

require 'schema' # test databse schema

# Rails Libraries & settings
I18n.enforce_available_locales = false
Time.zone = 'Taipei'

# Extend libraries
require 'action_controller'
require 'devise'
require 'awesome_nested_set' # mix nested set DSL for activerecord
require 'devise/orm/active_record' # mix devise DSL for activerecord
require 'simple_enum'
require 'public_activity'

# mix simple_enum DSL for activerecord
ActiveRecord::Base.send :extend, SimpleEnum::Attribute
ActiveRecord::Base.send :extend, SimpleEnum::Translation

# Load all of shared files
Dir["#{File.dirname(__FILE__)}/../shared/**/*.rb"].each { |f| require f }

# Main Libraries
require 'gambo' # gambo library

Gambo::Platforms.load_platform :api

I18n.load_path += Dir[Pathname.new(File.dirname(__FILE__)).join('../../', 'config', 'locales', '**', '*.{rb,yml}')]