RSpec.shared_context "site config" do
  before(:all) { @site_config = SiteConfig.create! }

  let(:site_config) { @site_config }
end
