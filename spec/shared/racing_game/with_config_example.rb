shared_examples "racing game with config" do
  let(:racing_game_model) { described_class }
  let(:competitors_count) { racing_game_model.competitors_count }
  let(:set_place_position) { racing_game_model.place_position }

  context "When get human game name" do
    pending
    # it "should be 'Horses'" do
    #   expect(racing_game_model.human_game_name).to eq human_game_name
    # end
  end

  context "When get rand competitors" do
    pending
    # it "should have 8 competitors" do
    #   expect(racing_game_model.rand_competitors.size).to eq competitors_count
    # end
  end

  context "When get odds collection" do
    pending
    # it "should have 3 odds collection" do
    #   expect(racing_game_model.collections.size).to eq 3
    # end
    # it "champion collection should have right odds count" do
    #   expect(racing_game_model.collections["champion"].size).to eq competitors_count
    # end
    # it "place collection should have right odds count" do
    #   expect(racing_game_model.collections["place"].size).to eq competitors_count
    # end
    # it "quinella collection should have right odds count" do
    #   expect(racing_game_model.collections["quinella"].size).to eq competitors_count * (competitors_count - 1)
    # end
    # it "#bet_type = 'champion' and #bet_number = 5 should be exist" do
    #   expect(racing_game_model.exist?('champion', 5)).to eq true
    # end
    # it "#bet_type = 'champion' and #bet_number = 9 should be not exist" do
    #   expect(racing_game_model.exist?('champion', 9)).to eq false
    # end
    # it "#bet_type = 'place' and #bet_number = 5 should be exist" do
    #   expect(racing_game_model.exist?('place', 5)).to eq true
    # end
    # it "#bet_type = 'place' and #bet_number = 9 should be not exist" do
    #   expect(racing_game_model.exist?('place', 9)).to eq false
    # end
    # it "#bet_type = 'quinella' and #bet_number = '5,3' should be exist" do
    #   expect(racing_game_model.exist?('quinella', '5,3')).to eq true
    # end
    # it "#bet_type = 'quinella' and #bet_number = 5 should be not exist" do
    #   expect(racing_game_model.exist?('quinella', '5')).to eq false
    # end
    # it "#bet_type = 'champion' and #bet_number = 1 should be right" do
    #   expect(racing_game_model.collections['champion']['1']).to eq champion_odds_for_bet_number_1
    # end
  end

  context "When get odds uncounted collections" do
    pending
    # it "should have 3 odds uncounted collections" do
    #   expect(racing_game_model.uncounted_collections.size).to eq 3
    # end
    # it "champion uncounted collection should have right odds count" do
    #   expect(racing_game_model.uncounted_collections["champion"].size).to eq competitors_count
    # end
    # it "place uncounted collection should have right odds count" do
    #   expect(racing_game_model.uncounted_collections["place"].size).to eq competitors_count
    # end
    # it "quinella uncounted collection should have right odds count" do
    #   expect(racing_game_model.uncounted_collections["quinella"].size).to eq competitors_count * (competitors_count - 1)
    # end
    # it "#bet_type = 'champion' and #bet_number = 1 should be right" do
    #   expect(racing_game_model.uncounted_collections['champion']['1']).to eq champion_uncounted_odds_for_bet_number_1
    # end
  end

  context "when get reward combinations hash" do
    pending
    # it "should have right combinations count" do
    #   expect(racing_game_model.reward_combinations_hash.size).to eq competitors_count * (competitors_count - 1) * (competitors_count - 2)
    # end
    # it "should have right champion combinations count (type_number : 1)" do
    #   expect(racing_game_model.champion_combinations('1').size).to eq (competitors_count - 1) * (competitors_count - 2)
    # end
    # it "should have right place combinations count (type_number : 1)" do
    #   expect(racing_game_model.place_combinations('1').size).to eq (competitors_count - 1) * (competitors_count - 2) * set_place_position
    # end
    # it "should have right quinella combinations count (type_number : '1,2')" do
    #   expect(racing_game_model.quinella_combinations('1,2').size).to eq competitors_count - 2
    # end
  end
end
