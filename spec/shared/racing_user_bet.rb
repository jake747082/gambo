shared_context "racing user bet" do
  before(:all) {
    @user.update!(credit: 2000, credit_used: 0)
    @service = Racing::Bet.new(@racing_horses_schedule.id, @user, true)
    @bet_types = ['champion', 'place', 'quinella']
    @bet_number = 1
    @bet_type = @bet_types[0]
    @bet_info = {'sub_bets' => [{'bet_credit' => 10, 'bet_number' => @bet_number, 'bet_type' => @bet_type}]}
    @status = @service.bet!(@bet_info)
  }

  let(:service) { @service }
  let(:bet_info) { @bet_info }
  let(:bet_types) { @bet_types }
  let(:bet_number) { @bet_number }
  let(:bet_type) { @bet_type }
end