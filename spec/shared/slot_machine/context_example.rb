shared_examples "a spin with response bonus, machines, bet_form" do
  pending
  # before(:all) {
  #   @slot_machine = SlotMachine.find_by_game_model_name!(described_class)
  #   @slot_machine_lines_count = @slot_machine.game_model.lines.count
  #   @machine.update(credit_max: @slot_machine.game_model.lines.count, credit_used: 0)
  #   @spin_service = Slot::Spin.new(@slot_machine, @machine)
  # }
  # let(:slot_model_lines) { @slot_machine_lines_count }
  # let(:slot_model) { described_class }
  # let(:slot_machine) { @slot_machine }
  # let(:bet_amount) { @slot_machine.game_model.lines.count }
  # let(:service) { @spin_service }
  #
  # it "should be reseponse successful" do
  #   allow(slot_model).to receive(:random_pick_bonus_payout) { |bonus|
  #     linked, multiple = bonus.max_by { |k, y| k }
  #     { linked_count: linked, multiple: multiple }
  #   }
  #   allow(slot_model).to receive(:get_bonus?) { true }
  #   allow(service).to receive(:dispatched_spin_amount) { dispatched_amount }
  #   expect(service.spin!(1, slot_model_lines)).to be true
  # end
  #
  # context "#event[:bonus]" do
  #   let(:bonuses) { service.events[:bonus] }
  #   let(:bonus) { bonuses.first }
  #
  #   it "should have only 1 bonus" do
  #     expect(bonuses.size).to eq 1
  #   end
  #
  #   it "should have linked position" do
  #     expect(bonus[:scatter].size).to eq linked_bonus_count
  #     bonus[:scatter].each do |position|
  #       expect(position).to have_key :x
  #       expect(position).to have_key :y
  #     end
  #   end
  #
  #   it "should have 1 bonus (multiple * credit)" do
  #     expect(bonus[:amount]).to eq bonus_amount
  #   end
  # end
  #
  # context "#machine" do
  #   it "#credit should eq bonus + win_amount + jackpot" do
  #     # machine always credit_left = 0
  #     expect(machine.credit_left).to eq (bonus_amount + service.win_amount)
  #   end
  # end
  #
  # context "#bet_form" do
  #   let(:bet_form) { service.bet_form }
  #   let(:total_machine_credit_diff) { (bonus_amount + service.win_amount - bet_amount)  }
  #
  #   it "#machine_credit_diff should eq win_amoun + bonus + jackpot" do
  #     expect(bet_form.machine_credit_diff).to eq total_machine_credit_diff
  #   end
  #
  #   it "#agents win_amount total should eq win_amoun + bonus + jackpot" do
  #     total_dispatched_amount = %w(agent director shareholder).map { |agent_level| bet_form["#{agent_level}_win_amount"] }.sum
  #     total_dispatched_amount += bet_form["shareholder_owe_parent"]
  #     expect(total_dispatched_amount).to eq (total_machine_credit_diff * -1)
  #   end
  # end
end

shared_examples "a spin with icons in each reels" do
  pending
  # let(:bonus_icon_id) { described_class.bonus_icon.id }
  # it "should have bonus icon in each reel" do
  #   icons_count = service.dimension_x.map { |reel| reel.include?(bonus_icon_id) ? 1 : 0 }.sum
  #   expect(icons_count).to eq linked_bonus_count
  # end
end

shared_examples "a spin with icons in random positions" do
  pending
  # let(:bonus_icon_id) { described_class.bonus_icon.id  }
  # it "should have bonus icon in grids random positions" do
  #   icons_count = if bonus_icon_id.is_a?(Array)
  #     service.dimension_x.flatten.map { |icon_id| bonus_icon_id.first == icon_id ? 1 : 0 } +
  #     service.dimension_x.flatten.map { |icon_id| bonus_icon_id.last == icon_id ? 1 : 0 }
  #   else
  #     service.dimension_x.flatten.map { |icon_id| icon_id == bonus_icon_id ? 1 : 0 }
  #   end.sum
  #   expect(icons_count).to eq linked_bonus_count
  # end
end

shared_examples "a spin with free spin" do
  context "#event[:free_spin]" do
    pending
    # let(:free_spin) { service.events[:free_spin] }
    #
    # it "should have free spin" do
    #   expect(free_spin).to be_present
    # end
    #
    # it "should with keys (count, used, token, positions)" do
    #   expect(free_spin).to have_key(:count)
    #   expect(free_spin).to have_key(:used)
    #   expect(free_spin).to have_key(:total)
    #   expect(free_spin).to have_key(:token)
    #   expect(free_spin).to have_key(:positions)
    # end
  end
end

shared_examples "a spin without free spin" do
  pending
  # context "#event[:free_spin]" do
  #   let(:free_spin) { service.events[:free_spin] }
  #
  #   it "should not with free spin" do
  #     expect(free_spin).to be_nil
  #   end
  # end
end
