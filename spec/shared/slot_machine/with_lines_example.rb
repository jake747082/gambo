shared_examples "slot machine with lines" do
  let(:slot_machine_model) { described_class }
  context "#lines", slow: true do
    (1..described_class.lines.count).each do |id|
      routes = described_class.lines[id]
      context "##{id} each position #{routes} in line should in right reel" do
        let(:line_id) { id }
        let(:line_routes) { routes }
        it "each position should in right reel" do
          line_routes.each_with_index do |position, reel_index|
            reel = reel_index + 1
            position_in_reel = (position % slot_machine_model.width)
            position_in_reel = slot_machine_model.width if position_in_reel == 0
            expect(position_in_reel).to eq reel
          end
        end
      end

      context "line compare" do
        ((id + 1)..described_class.lines.count).each do |compare_id|
          compare_routes = described_class.lines[compare_id]
          context "compare with ##{id} to ##{compare_id}" do
            it "should not duplicate" do
              expect(routes).to_not eq compare_routes
            end
          end
        end
      end
    end
  end
end