RSpec.shared_context "slot game" do
  before(:all) {
    # Slot Game
    {
      :money_kindom => 'MoneyKindom',
      :mayan => 'MayanEclipse',
      :forbidden_city => 'ForbiddenCity',
      :valley_kings => 'ValleyKings',
      :ninja => 'SamuraiVsNinja',
      :paradise => 'WingsOfTheParadise',
      :treasure => 'TreasureOfTheTitans',
      :zhao_cia_jin_bao => 'ZhaoCiaJinBao',
      :winning_wonders => 'WinningWonders',
      :vegas_startlight => 'VegasStarlight',
      :bear_cubs => 'BearCubs',
      :buffalo_wild_wins => 'BuffaloWildWins',
      :quest_for_atlantis => 'QuestForAtlantis',
      :viking_plunder => 'VikingPlunder',
      :amazon_safiri => 'AmazingSafari',
      :running_with_the_pack => 'RunningWithThePack',
      :white_river => 'WhiteRiver',
      :desert_rose => 'DesertRose',
      :parise_love => 'ParisLove',
      :bellera => 'BelleraDeEspana',
      :undersea_king => 'UnderseaKing',
      :gold_city => 'TheGoldenCity',
      :unicorn_falls => 'UnicornFalls',
      :jet_set_rollers => 'JetSetRollers',
      :winder_of_the_deep => 'WondersOfTheDeep',
      :shamrockin => 'Shamrockin',
      :dark_vs_light => 'DarkVsLight',
      :puppy_power => 'PuppyPower',
      :jazzy_riches => 'JazzyRiches',
      :immortal_treasure => 'ImmortalTreasure',
      :crests_of_camelot => 'CrestsOfCamelot', 
      :nidhi_of_india => 'NidhiOfIndia', 
      :pixies_of_enchantia => 'PixiesOfEnchantia', 
      :superb_heroes => 'SuperbHeroes', 
      :amazing_blazing => 'AmazingBlazing', 
      :movie_reels => 'MovieReels', 
      :bruce_lee => 'BruceLee'
    }.each do |var_name, model_name|
      instance_variable_set("@slot_#{var_name}", SlotMachine.create(game_model_name: "Slot::#{model_name}",
                                                                 lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                                                 lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                                                 lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                                                 lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                                                 lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                                                 win_speed: 20))
    end

    # Game Setting
    @game_setting = GameSetting.create(jackpot_primary_max: 2000000, jackpot_accumulate_rate: 0.02,
                                       jackpot_md_range_begin: 1000, jackpot_md_range_end: 3000, jackpot_md_interval: 200,
                                       jackpot_sm_range_begin: 200, jackpot_sm_range_end: 1000, jackpot_sm_interval: 50)

  }

  let(:slot_money_kindom) { @slot_money_kindom }
  let(:slot_mayan) { @slot_mayan }
  let(:slot_forbidden_city) { @slot_forbidden_city }
  let(:game_setting) { @game_setting }
end
