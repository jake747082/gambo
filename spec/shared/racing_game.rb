RSpec.shared_context "racing game" do
  before(:all) {
    # Racing Game
    {
      :horses => 'Horses',
      :eight_dogs => 'EightDogs',
      :six_dogs => 'SixDogs'
    }.each do |var_name, model_name|
      instance_variable_set("@racing_#{var_name}", RacingGame.create(game_model_name: "Racing::#{model_name}",
                                                                 lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                                                 lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                                                 lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                                                 lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                                                 lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                                                 win_speed: 20))
    end
  }

  let(:racing_horses) { @racing_horses }
  let(:racing_eight_dogs) { @racing_eight_dogs }
  let(:racing_six_dogs) { @racing_six_dogs }
end
