shared_context "agent tree and machine" do
  before { 
    allow(Pusher).to receive(:trigger_async) { true }
    allow(Pusher).to receive(:trigger) {true}
  }

  before(:all) {
    Agent.delete_all
    Machine.delete_all
    User.delete_all

    # (股東)
    @shareholder = Agent.new(credit_max: 0)
    AgentForm::Create.new(@shareholder).create(username: 'agent1', nickname: '測試股東', password: '888888', password_confirmation: '888888', credit_max: 100000, casino_ratio: 95)

    # (總代)
    @director = Agent.new(credit_max: 0, parent: @shareholder)
    AgentForm::Create.new(@director).create(username: 'agent2', nickname: '測試總代', password: '888888', password_confirmation: '888888', credit_max: 80000, casino_ratio: 80)

    # (代理)
    @agent = Agent.new(credit_max: 0, parent: @director)
    AgentForm::Create.new(@agent).create(username: 'agent3', nickname: '測試代理', password: '888888', password_confirmation: '888888', credit_max: 20000, casino_ratio: 50)

    # 機器
    @machine = Machine.new(credit_max: 10000000, boot: true, agent: @agent)
    @machine_form = MachineForm::Create.new(@machine).tap { |f| f.create(nickname: 'Machine1', mac_address: '3c-15-c2-d4-a6-b0', maintain_code: @agent.generate_maintain_code) }

    # 會員
    @user = User.new(agent: @agent)
    @user_form = UserForm::Create.new(@user).tap { |f| f.create(username: 'test_user', nickname: 'test_user', password: '888888', password_confirmation: '888888', credit: 1000) }

    # 子帳號
    @sub_account_form = AgentForm::SubAccount::Create.new(@agent.accounts.new).tap { |f| f.create(nickname: 'subaccount', username: 'subaccount', password: '111111', password_confirmation: '111111') }
  }

  let(:shareholder) { @shareholder.reload }
  let(:director) { @director.reload }
  let(:agent) { @agent.reload }
  let(:machine) { @machine.reload }
  let(:user) { @user.reload }
  let(:machine_form) { @machine_form.reload }
  let(:sub_account) { @sub_account_form.model.reload }
end