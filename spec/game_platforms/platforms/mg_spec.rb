require 'savon'

describe GamePlatforms::Mg do
  include_context "agent tree and machine"
  before {
    getdata_url       = 'https://entservices200.totalegame.net?wsdl'
    dobusiness_url    = 'https://tegapi200.totalegame.net'
    domain            = 'www.mdb88.net'
    agent             = 'mdbbet198935'
    agent_password    = 'eb4ab7'
    prefix            = 'mdb'
    # 設定Setting測試資料
    allow(Setting).to receive_message_chain(:game_platform, :mg, :getdata_url).and_return(getdata_url)
    allow(Setting).to receive_message_chain(:game_platform, :mg, :dobusiness_url).and_return(dobusiness_url)
    allow(Setting).to receive_message_chain(:game_platform, :domain).and_return(domain)
    allow(Setting).to receive_message_chain(:game_platform, :mg, :agent).and_return(agent)
    allow(Setting).to receive_message_chain(:game_platform, :mg, :agent_password).and_return(agent_password)
    allow(Setting).to receive_message_chain(:game_platform, :mg, :prefix).and_return(prefix)

    # @todo 暫時先將與第三方server的class做覆蓋, 若確認測試帳號啟用, 則將底下註解
    value = { session_guid: '', error_code: '', ip_address: '' }
    allow_any_instance_of(GamePlatforms::Mg).to receive(:is_authenticate) { value }
    allow_any_instance_of(GamePlatforms::Mg).to receive(:is_account_available?) { true }
    allow_any_instance_of(GamePlatforms::Mg).to receive(:check_and_create_account) { true }
    allow_any_instance_of(GamePlatforms::Mg).to receive(:get_balance) { 10 }

    @mg = GamePlatforms::Mg.new(@user)
  }

  ### instance method

  context "#initialize" do

    # @todo 這些參數是否外部不需可以取得
    it { expect(@mg.getdata_url).to eq Setting.game_platform.mg.getdata_url }
    it { expect(@mg.dobusiness_url).to eq Setting.game_platform.mg.dobusiness_url }
    it { expect(@mg.website).to eq "http://#{Setting.game_platform.domain}/mg_electronic_games/retry" }
    it { expect(@mg.agent).to eq Setting.game_platform.mg.agent }
    it { expect(@mg.agent_password).to eq Setting.game_platform.mg.agent_password }
    it { expect(@mg.prefix).to eq Setting.game_platform.mg.prefix }

    it { expect(@mg.username).to eq "#{Setting.game_platform.mg.prefix}#{@user.username}" }
    it { expect(@mg.password).to eq "#{@user.user_game_platformships.mg.password}" }
  end

  context "#get_balance" do
    it { expect(@balance = @mg.get_balance).to be >= 0 }
  end

  context "#transfer_credit" do
    pending
    # @todo 若不連結第三方server會錯誤, @balance 不能互通
    # it { expect(@mg.transfer_credit(100, 'IN')).to eq true }
    # it { expect(@mg.get_balance).to eq @balance = @balance + 100 }
    # it { expect(@mg.transfer_credit(50, 'OUT')).to eq true }
    # it { expect(@mg.get_balance).to eq @balance - 50 }
  end

  context "#transfer_single_game_url" do
    it { expect(@mg.transfer_single_game_url('majorMillions')).to eq "https://mobile22.gameassists.co.uk/mobilewebservices/casino/game/launch/GoldenTree/majorMillions/en/?lobbyURL=#{@mg.website}&bankingURL=#{@mg.website}&username=#{@mg.username}&password=#{@mg.password}&currencyFormat=%23%2C%23%23%23.%23%23&logintype=fullUPE&xmanEndPoints=https://xplay22.gameassists.co.uk/xman/x.x" }
  end

  context "#betting_records" do
    pending
  end

  ### class method

  context ".config" do
    config = GamePlatforms::Mg.config
    it { expect(config).to be_an Hash }
    it { expect(config).to have_key 'html5' }
  end

  context ".games" do
    games = GamePlatforms::Mg.games
    it { expect(games).to be_an Hash }
    it { expect(games.size).to eq 3 }
    it { expect(games[1]).to include 'majorMillions' }
  end

  context ".games_config" do
    games_config = GamePlatforms::Mg.games_config
    it { expect(games_config).to be_an Hash }
    it { expect(games_config.size).to eq 3 }
    it { expect(games_config[1]).to have_key 15005 }
  end

  context ".category" do
    category = GamePlatforms::Mg.category
    it { expect(category).to be_an Hash }
    it { expect(category.size).to eq 3 }
    it { expect(category[1]).to eq 'SLOTS' }
  end

end
