require 'net/http'

describe GamePlatforms::Ag do
  include_context "agent tree and machine"
  before {
    do_business_url    = 'http://gi.sf1788.com:81/doBusiness.do'
    forward_game_url  = 'http://gci.sf1788.com:81/forwardGame.do'
    md5_key           = 'EqM326JFd5ML'
    des_key           = 'apUPNvuJ'
    cagent            = 'L13_AGIN'

    # 設定Setting測試資料
    allow(Setting).to receive_message_chain(:game_platform, :ag, :do_business_url).and_return(do_business_url)
    allow(Setting).to receive_message_chain(:game_platform, :ag, :forward_game_url).and_return(forward_game_url)
    allow(Setting).to receive_message_chain(:game_platform, :ag, :md5_key).and_return(md5_key)
    allow(Setting).to receive_message_chain(:game_platform, :ag, :des_key).and_return(des_key)
    allow(Setting).to receive_message_chain(:game_platform, :ag, :cagent).and_return(cagent)

    # @todo 暫時先將與第三方server的class做覆蓋, 若確認測試帳號啟用, 則將底下註解
    allow_any_instance_of(GamePlatforms::Ag).to receive(:check_and_create_account) { true }

    @mg = GamePlatforms::Ag.new(@user)
  }

  ### instance method
  context "#initialize" do
    # 1. 檢查每個設定可讀參數
    it {expect(@mg.dobusiness_url).to eq Setting.game_platform.ag.do_business_url}
    it {expect(@mg.forward_game_url).to eq Setting.game_platform.ag.forward_game_url}
    it {expect(@mg.md5_key).to eq Setting.game_platform.ag.md5_key}
    it {expect(@mg.des_key).to eq Setting.game_platform.ag.des_key}
    it {expect(@mg.cagent).to eq Setting.game_platform.ag.cagent}

    it {expect(@mg.username).to eq @user.username }
    it {expect(@mg.password).to eq @user.user_game_platformships.ag.password }
    # 檢查會員是否有傳入, 若有建立ag會員, 若無則error
  end

  context "#betting_records" do
    # 取得當日下注紀錄
    pending
  end

  context "#check_and_create_account" do
    pending
  end

  context "#get_balance" do
    pending
  end

  context "#transfer_credit" do
    pending
  end

  context "#query_order_status" do
    pending
  end

  context "#transfer_game" do
    pending
  end
end
