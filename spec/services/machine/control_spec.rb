describe Machine::Control do
  include_context "agent tree and machine"

  before(:all){ @machine_service = Machine::Control.new(@machine) }

  context "when machine boot" do

    it "#boot! should return true" do
      allow(@machine_service).to receive(:push_event!).and_return(true)
      expect(@machine_service.boot!).to be true
    end

    it "machine should be boot" do
      expect(machine).to be_boot
    end
  end

  context "when machine shutdown" do

    it "should shutdown machine" do
      allow(@machine_service).to receive(:push_event!).and_return(true)
      expect(@machine_service.shutdown!).to be true
    end

    it "machine should be shutdown" do
      expect(machine).to_not be_boot
    end
  end
end