describe Finance::ChildrenCashout do
  include_context "agent tree and machine"
  include_context "site config"
  include_context "slot game"
  include_context "slot spin"
  # include_context "racing game"
  # include_context "racing schedule"
  # include_context "racing bet"

  before(:all) { @service = Finance::ChildrenCashout.new }

  context "when generated agent finance" do
    pending
    # before(:all) { @stats = @service.call(Date.today, Date.today, @agent) }
    # it "finance should instance of CashoutStatistics" do
    #   expect(@stats).kind_of?(Finance::ChildrenCashout::CashoutStatistics)
    # end
    #
    # it "finance list should instance of SlotBetForm" do
    #   expect(@stats.list).kind_of?(SlotBetForm)
    # end
    # it "should not have children list" do
    #   expect(@stats.list).to be_empty
    # end
    # it "should have self list and machine list" do
    #   expect(@stats.current_agent).to be_present
    #   expect(@stats.player_cashout_list).to be_present
    # end
  end

  context "when generated admin finance" do
    pending
    # before(:all) { @stats = @service.call(Date::today, Date::today) }
    #
    # it "should have children list" do
    #   expect(@stats.list).to be_present
    # end
    #
    # it "#cashout_query_information should be shareholder and SlotBetForm" do
    #   agent_level_next, total_bet_forms = @service.send(:cashout_query_information)
    #   expect(agent_level_next.to_s).to eq('shareholder')
    #   expect(total_bet_forms).to eq([ SlotBetForm, RacingBetForm ])
    # end
  end

  context "when generated shareholder finance" do
    pending
    # before(:all) { @stats = @service.call(Date::today, Date::today, @shareholder) }
    #
    # it "should have self list and children list" do
    #   expect(@stats.current_agent).to be_present
    #   expect(@stats.list).to be_present
    # end
    #
    # it "#cashout_query_information should be director and shareholder #slot_bet_form" do
    #   agent_level_next, total_bet_forms = @service.send(:cashout_query_information)
    #   expect(agent_level_next.to_s).to eq('director')
    #   expect(total_bet_forms).to eq([ shareholder.all_slot_bet_forms, shareholder.all_racing_bet_forms ])
    # end
  end
end
