describe Bonus::Remove do
  include_context "agent tree and machine"

  context "when pass a valid bonus_id" do
    let(:bonus) {
      @bonus = @machine.bonuses.new(bonus_type: :primary, bonus: 100, done: false, bonus_dispatched: 0)
      @bonus.save(validate: false)
      @bonus
    }

    it "should pass with a INTEGER bonus_id" do
      expect(Bonus::Remove.call(bonus.id)).to be true
      expect { Bonus.find(bonus.id) }.to raise_error ActiveRecord::RecordNotFound
    end

    it "should pass with a STRING bonus_id" do
      expect(Bonus::Remove.call(bonus.id.to_s)).to be true
      expect { Bonus.find(bonus.id) }.to raise_error ActiveRecord::RecordNotFound
    end
  end

  it "should return false when pass invalid bonus id" do
    expect(Bonus::Remove.call(0)).to be false
  end

  context "When Bonus is done" do
    let(:bonus) {
      @bonus = @machine.bonuses.new(bonus_type: :primary, bonus: 100, done: true, bonus_dispatched: 0)
      @bonus.save(validate: false)
      @bonus
    }
    it "cannot be removed" do
      expect(Bonus::Remove.call(bonus.id)).to be false
    end
  end

  context "When Bonus is dispatched already but not done" do
    let(:bonus) {
      @bonus = @machine.bonuses.new(bonus_type: :primary, bonus: 100, done: false, bonus_dispatched: 1)
      @bonus.save(validate: false)
      @bonus
    }
    it "cannot be removed" do
      expect(Bonus::Remove.call(bonus.id)).to be false
    end
  end
end