describe Agent::TreeLock do
  include_context "agent tree and machine"

  context "when whole tree is unlock state with booting machine" do
    before(:all) {
      # init update agents unlock and machine unlock and boot
      @shareholder.self_and_descendants.lock!.each do |agent|
        agent.update!(lock: false)
      end
      @machine.update!(lock: false, boot: true)
    }
    it "should return true when lock shareholder with whole tree" do
      expect(Agent::TreeLock.new(shareholder).lock).to be true
    end

    it "should be locked state with whole agent tree" do
      shareholder.self_and_descendants.each do |agent|
        expect(agent).to be_lock
      end
    end

    it "should be locked state and shutdown with machine" do
      expect(machine).to be_lock
      expect(machine).to_not be_boot
    end
  end

  context "when only unlock shareholder" do
    before(:all) {
      # init update agents lock and machine lock and shutdown
      @shareholder.self_and_descendants.lock!.each do |agent|
        agent.update!(lock: true)
      end
      @machine.update!(lock: true, boot: false)

      @shareholder_service = Agent::TreeLock.new(@shareholder)
      @status = @shareholder_service.unlock
    }

    it "should only unlocked state with shareholder" do
      expect(@status).to be true
      expect(shareholder).to_not be_lock
      expect(director).to be_lock
      expect(agent).to be_lock
    end
    specify { expect { |b| Agent::TreeLock.new(@shareholder).unlock(&b) }.to yield_control }
  end

  context "when unlock agent self" do
    before(:all) {
      # init update agent lock, parents unlock and machine lock and shutdown
      @shareholder.update!(lock: false)
      @director.update!(lock: false)
      @agent.update!(lock: true)
      @machine.update!(lock: true, boot: false)

      @agent_service = Agent::TreeLock.new(@agent)
    }
    it "should unlock agent self and machines" do
      allow(@agent_service).to receive(:push_event!).and_return(true)
      expect(@agent_service.unlock).to be true
      expect(agent).to_not be_lock
      expect(machine).to_not be_lock
    end
    # specify { expect { |b| Agent::TreeLock.new(@agent).unlock(&b) }.to yield_successive_args }
  end

  context "unlock shareholder all children" do
    before(:all) {
      # init update agents lock and machine lock and shutdown
      @shareholder.self_and_descendants.lock!.each do |agent|
        agent.update!(lock: true)
      end
      @machine.update!(lock: true, boot: false)

      @shareholder_service = Agent::TreeLock.new(@shareholder)
    }

    it "should unlock with whole agent tree" do
      allow(@shareholder_service).to receive(:push_event!).and_return(true)
      expect(@shareholder_service.unlock_all).to be true
      shareholder.self_and_descendants.each do |agent|
        expect(agent.reload).to_not be_lock
      end
    end
    it "machines should unlock" do
      expect(machine).to_not be_lock
    end
  end
end
