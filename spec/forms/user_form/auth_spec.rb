describe UserForm::Auth do
  include_context "agent tree and machine"

  let(:form) { UserForm::Auth.new(@user.username) }

  context "when input correct password" do
    it { expect(form.verify('888888')).to be true }

    it "should be run block when pass" do
      expect { form.verify('888888') { raise "pass" }  }.to raise_error
    end
  end

  context "When input incorrect password" do
    it { expect(form.verify('777777')).to be false }

    it "should not be run block when pass" do
      expect { form.verify('777777') { raise "pass" }  }.to_not raise_error
    end
  end
end