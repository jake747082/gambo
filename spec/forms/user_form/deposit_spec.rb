describe UserForm::Deposit do
  include_context "agent tree and machine"
  before(:all) { @user.update!(credit_used: 0, credit: 0) }

  context "when deposit credit with 2 decimal (0.01)" do
    pending
    # before(:all) { @form = UserForm::Deposit.new(@user, deposit_credit: 0.01) }
    # it {expect(@form.deposit).to be true }
    # it { expect(user.credit_left).to eq(0.01) }
  end

  context "when agent have not enough credit" do
    before(:all) { @form = UserForm::Deposit.new(@user, deposit_credit: @agent.credit_left + 10) }
    it { expect(@form.deposit).to be false }
    it { expect(@form.errors[:deposit_credit]).to be_present }
  end

  context "when user deposit credit = 0 " do
    before(:all) { @form = UserForm::Deposit.new(@user, deposit_credit: 0) }
    it { expect(@form.deposit).to be false }
    it { expect(@form.errors[:deposit_credit]).to be_present }
  end
end
