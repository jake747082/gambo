describe UserForm::Create do
  include_context "agent tree and machine"

  context "when created user" do
    before(:all) {
      @origin_agent_total_users_count = @agent.reload.total_users_count
      @new_user = User.new(agent: @agent)
      @user_form = UserForm::Create.new(@new_user)
      @user_params = { username: 'test_user2', nickname: 'test_user2', password: '888888', passowrd_confirmation: '888888' }
    }
    it "create user! return true" do
      expect(@user_form.create(@user_params)).to be true
    end
    it "user's agent should be agent3" do
      expect(@new_user.agent).to eq(agent)
    end
    it "user's director should be agent2" do
      expect(@new_user.director).to eq(director)
    end
    it "user's shareholder should be agent1" do
      expect(@new_user.shareholder).to eq(shareholder)
    end
    it "user's agent increase total_users_count" do
      expect(@new_user.agent.total_users_count).to eq(@origin_agent_total_users_count + 1)
    end
  end
end