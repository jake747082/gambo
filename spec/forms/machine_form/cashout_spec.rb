describe MachineForm::Cashout do
  include_context "agent tree and machine"

  context "when cashout credit with 2 decimal (0.01)" do
    before(:all) {
      @machine.update!(credit_max: 100, credit_used: 0)
      @form = MachineForm::Cashout.new(@machine, cashout_credit: 0.01)
    }
    it { expect{@form.cashout}.to change{@machine.credit_left}.from(100).to(99.99)}
    it { expect(@form.cashout).to be true}
  end

  context "when cashout credit over credit left" do
    before(:all) { @form = MachineForm::Cashout.new(@machine, cashout_credit: 200) }
    it { expect(@form.cashout).to be false}
    it { expect(@form.errors[:cashout_credit].first).to include "Cashout credit cannot exceed" }
  end

  context "when machine cashout credit = 0" do
    before(:all) { @form = MachineForm::Cashout.new(@machine, cashout_credit: 0) }
    it { expect(@form.cashout).to be false}
    it { expect(@form.errors[:cashout_credit]).to be_present }
  end

  context "when cashout all credit left" do
    before(:all) {
      @machine.update!(credit_max: 100, credit_used: 0)
      @form = MachineForm::Cashout.new(@machine, cashout_credit: 100)
    }
    it { expect(@form.cashout).to be true}
    it { expect(machine.credit_max).to eq(0)}
    it { expect(machine.credit_used).to eq(0)}
  end

  context "when withdraw credit (10)" do
    before(:all) {
      @machine.update!(credit_max: 100, credit_used: 0)
      @form = MachineForm::Cashout.new(@machine, cashout_credit: 10)
    }
    it { expect(@form.cashout).to be true}
    it { expect(machine.credit_max).to eq(100)}
    it { expect(machine.credit_used).to eq(10)}
  end
end