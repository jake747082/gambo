describe MachineForm::Delete do
  include_context "agent tree and machine"

  before(:all) {
    @machine.update(boot: false, credit_max: 0, credit_used: 0)
    @form = MachineForm::Delete.new(@machine)
  }

  context "when machine delete" do
    it { expect(@form.delete).to be true }
    it { expect(@machine).to be_deleted }
  end

  context "when machine boot" do
    before(:all) { @machine.update(boot: true) }
    it { expect(@form.delete).to be false }
    it { expect(@form.errors[:id]).to include "Please turn cashout and turn off machine first before removing machine" }
  end

  context "when machine credit left != 0" do
    before(:all) { @machine.update(credit_max: 10, credit_used: 1) }
    it { expect(@form.delete).to be false }
    it { expect(@form.errors[:id]).to include "Please turn cashout and turn off machine first before removing machine" }
  end
end
