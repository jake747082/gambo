describe MachineForm::Deposit do
  include_context "agent tree and machine"
  before(:all) { @machine.update!(credit_used: 0, credit_max: 0) }

  context "when deposit credit with 2 decimal (0.01)" do
    before(:all) { @form = MachineForm::Deposit.new(@machine, deposit_credit: 0.01) }
    it { expect(@form.deposit).to be true }
    it { expect(machine.credit_left).to eq(0.01) }
  end

  context "when agent have not enough credit" do
    before(:all) { @form = MachineForm::Deposit.new(@machine, deposit_credit: @agent.credit_left + 10) }
    it { expect(@form.deposit).to be false }
    it { expect(@form.errors[:deposit_credit].first).to include "Deposit amount cannot exceed" }
  end

  context "when machine deposit credit = 0 " do
    before(:all) { @form = MachineForm::Deposit.new(@machine, deposit_credit: 0) }
    it { expect(@form.deposit).to be false }
    it { expect(@form.errors).to have_key :deposit_credit }
  end
end