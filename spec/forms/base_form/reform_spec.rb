module DemoForm
  class Update < BaseForm::Reform
    model :user
    property :nickname
    validates :nickname, presence: { message: :blank_nickname }

    # 定義 i18n scope
    def self.i18n_scope
      :xxx
    end
  end
end

describe BaseForm::Reform do
  context "when new a demo form then save" do
    before(:all) { @form = DemoForm::Update.new(User.new) }

    it { expect(@form).to be_invalid }

    it { expect(@form.errors).to have_key :nickname }

    it "should get activerecord i18n" do
      expect(@form.errors).to have_key :nickname
    end
  end
end