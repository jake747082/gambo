module DemoForm
  class Create < BaseForm::Basic
    form_name :demo

    attribute :title

    validates :title, presence: { message: :abc }

    # 定義 i18n scope
    def self.i18n_scope
      :xxx
    end
  end
end

describe BaseForm::Basic do

  context "when new a demo form then save" do
    before(:all) { @form = DemoForm::Create.new }

    it { expect(@form).to be_invalid }

    it { expect(@form.errors).to have_key :title }

    it "should get activerecord i18n" do
      expect(@form.errors).to have_key :title
    end
  end
end
