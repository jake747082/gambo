describe WithdrawForm::Cashout do
  include_context "agent tree and machine"

  context "when cashout credit with 2 decimal (10.01)" do
    before(:all) {
      @user.update!(credit: 100, credit_used: 0)
      @params = {username: @user.username, credit: 10.01, credit_diff: 0.01, type: :deposit}
      @form = WithdrawForm::Cashout.new(@params)
    }
    it { expect(@form.cashout).to be true }
    it { expect(@user.reload.credit_left.round(2)).to eq(90) }
    it { expect(@form.credit_actual.round(2)).to eq(10) }
  end

  context "when cashout actual credit less 10" do
    before(:all) {
      @user.update!(credit: 100, credit_used: 0)
      @params = {username: @user.username, credit: 0.01, credit_diff: 0, type: :deposit}
      @form = WithdrawForm::Cashout.new(@params)
    }
    it { expect(@form.cashout).to be false }
  end

  context "when cashout credit over credit left" do
    before(:all) { 
      @user.update!(credit: 100, credit_used: 0)
      @params = {username: @user.username, credit: 200, credit_diff: 0, type: :deposit}
      @form = WithdrawForm::Cashout.new(@params)
    }
    it { expect(@form.cashout).to be false}
    it { expect(@form.errors[:credit]).to be_present }
  end

  context "when user cashout credit = 0" do
    before(:all) {
      @user.update!(credit: 100, credit_used: 0)
      @params = {username: @user.username, credit: 0, credit_diff: 0, type: :deposit}
      @form = WithdrawForm::Cashout.new(@params)
    }
    it { expect(@form.cashout).to be false}
    it { expect(@form.errors[:credit]).to be_present }
  end

  context "when cashout all credit left" do
    before(:all) {
      @user.update!(credit: 100, credit_used: 0)
      @params = {username: @user.username, credit: 100, credit_diff: 0, type: :deposit}
      @form = WithdrawForm::Cashout.new(@params)
    }
    it { expect(@form.cashout).to be true}
    it { expect(@user.reload.credit).to eq(0)}
    it { expect(@user.reload.credit_used).to eq(0)}
  end

  context "when withdraw credit (10)" do
    before(:all) {
      @user.update!(credit: 100, credit_used: 0)
      @params = {username: @user.username, credit: 10, credit_diff: 0, type: :deposit}
      @form = WithdrawForm::Cashout.new(@params)
    }
    it { expect(@form.cashout).to be true}
    it { expect(@user.reload.credit).to eq(100)}
    it { expect(@user.reload.credit_used).to eq(10)}
  end
end