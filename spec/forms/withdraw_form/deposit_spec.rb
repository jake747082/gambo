describe WithdrawForm::Deposit do
  include_context "agent tree and machine"
  before(:all) { @user.update!(credit_used: 0, credit: 0) }

  context "when deposit credit with 2 decimal (0.01)" do
    pending
    # before(:all) {
    #   @params = {username: @user.username, credit: 10.01, credit_diff: 0.01, cashout_limit: 100, type: :cashout}
    #   @form = WithdrawForm::Deposit.new(@params)
    # }
    # it { expect(@form.deposit).to be true }
    # it { expect(user.credit_left).to eq(10.02) }
    # it { expect(user.cashout_limit).to eq(100) }
  end

  context "when agent have not enough credit" do
    before(:all) {
      @params = {username: @user.username, credit: @agent.credit_left + 10, credit_diff: 0, cashout_limit: 0, type: :cashout}
      @form = WithdrawForm::Deposit.new(@params)
    }
    it { expect(@form.deposit).to be false }
    it { expect(@form.errors[:credit]).to be_present }
  end

  context "when user deposit credit = 0 " do
    before(:all) {
      @params = {username: @user.username, credit: 0, credit_diff: 0, cashout_limit: 0, type: :cashout}
      @form = WithdrawForm::Deposit.new(@params)
    }
    it { expect(@form.deposit).to be false }
    it { expect(@form.errors[:credit]).to be_present }
  end
end
