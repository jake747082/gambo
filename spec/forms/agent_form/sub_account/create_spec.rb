describe AgentForm::SubAccount::Create do
  include_context "agent tree and machine"

  context "when create agent sub account" do
    let(:form) { AgentForm::SubAccount::Create.new(@agent.accounts.new) }

    it "should return true, when username, password valid" do
      expect(form.create(nickname: 'subaccount', username: 'subaccount2', password: '111111', password_confirmation: '111111')).to be true
    end

    it "should return false, when username, password unvalid" do
      expect(form.create(nickname: '', username: 'short', password: 'short', password_confirmation: 'short')).to be false
      expect(form.errors).to have_key :nickname
      expect(form.errors).to have_key :username
      expect(form.errors).to have_key :password
    end
  end
end