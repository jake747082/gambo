describe AgentForm::Create do
  include_context "agent tree and machine"

  context "when created shareholder" do
    before(:all) {
      @new_shareholder = Agent.new
      @shareholder_form = AgentForm::Create.new(@new_shareholder)
      @shareholder_params = { username: 'shareholder888', nickname: 'shareholder888', password: '888888', passowrd_confirmation: '888888' }
    }
    it "create shareholder! return true" do
      expect(@shareholder_form.create(@shareholder_params)).to be true
    end
    it "shareholder's level should be 0" do
      expect(@new_shareholder.level).to eq(0)
    end
    it "shareholder should not have parent" do
      expect(@new_shareholder.parent_id).to be_nil
    end
  end

  context "When created a agent under director" do
    before(:all) { @agent_form = AgentForm::Create.new(@director.children.new) }

    let(:new_agent) { @agent_form.model.reload }

    it "form should return true" do
      params = { username: 'agent888', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end

    it "uplevel agents #total_agents_count should increase" do
      expect(shareholder.total_agents_count).to eq 3
      expect(director.total_agents_count).to eq 2
    end

    it "agent should be a agent (shop)" do
      expect(new_agent).to be_agent
    end

    it "agent'parent should be director" do
      expect(new_agent.parent).to eq director
    end

    it "agent should belongs to shareholder and director" do
      expect(director.descendants.ids).to include new_agent.id
      expect(shareholder.descendants.ids).to include new_agent.id
    end
  end

  context "when parent's #credit_left and #casino_ratio not enough" do
    before(:all) {
      @director.update!(credit_max: 100, casino_ratio: 90, credit_dispatched: 0, credit_used: 0)
      @agent_form = AgentForm::Create.new(@director.children.new)
      @agent_form.credit_max = 200
      @agent_form.casino_ratio = 95
    }

    it "should return false" do
      expect(@agent_form).to_not be_valid
      expect(@agent_form.errors[:credit_max]).to be_present
      expect(@agent_form.errors[:casino_ratio]).to be_present
    end
  end

  context "when input #credit_max" do
    before(:all) {
      @director.update!(credit_max: 1)
      @agent_form = AgentForm::Create.new(@director.children.new)
    }

    it "2 decimal should return true" do
      params = { username: 'agent999', nickname: 'agent999', password: '888888', passowrd_confirmation: '888888', credit_max: 0.01 }
      expect(@agent_form.create(params)).to be true
    end
  end

  context "when input username" do
    before(:all) { @agent_form = AgentForm::Create.new(@director.children.new) }

    it "'網咖端的帳號' should return false" do
      params = { username: '網咖端的帳號', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be false
    end
    it "'網咖端帳號ABC' should return false" do
      params = { username: '網咖端帳號ABC', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be false
    end
    it "'ABC網咖端帳號' should return false" do
      params = { username: 'ABC網咖端帳號', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be false
    end
    it "'網咖ABC端帳號' should return false" do
      params = { username: '網咖ABC端帳號', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be false
    end

    it "'abcdef' should return true" do
      params = { username: 'abcdef', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end
    it "'ABCDEF' should return true" do
      params = { username: 'ABCDEF', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end
    it "'123456' should return true" do
      params = { username: '123456', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end
    it "'______' should return true" do
      params = { username: '______', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end
    it "'......' should return true" do
      params = { username: '......', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end
    it "'123_.ABC' should return true" do
      params = { username: '123_.ABC', nickname: 'agent888', password: '888888', passowrd_confirmation: '888888' }
      expect(@agent_form.create(params)).to be true
    end
  end
end