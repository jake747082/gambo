describe MessageForm::Create do
  include_context "agent tree and machine"

  context "when to target" do
    let(:message_form) {MessageForm::Create.new(Message.new) }
    let(:params) { {title: 'This is a message.', content: 'This is a content.'} }
    it "only to a target agent should success" do
      params[:agent_username] = @agent.username
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
    it "only to a target user should success" do
      params[:username] = @user.username
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
    it "only to agent port should success" do
      params[:agent_port] = 1
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
    it "only to user port should success" do
      params[:www_port] = 1
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
    it "only to a target user and agent port should success" do
      params[:username] = @user.username
      params[:agent_port] = 1
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
    it "only to a target agent and user port should success" do
      params[:agent_username] = @agent.username
      params[:www_port] = 1
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
    it "to all target should success" do
      params[:username] = @user.username
      params[:agent_username] = @agent.username
      params[:www_port] = 1
      params[:agent_port] = 1
      expect(message_form.create(params)).to be true
      expect(message_form.model.id).not_to be_nil
    end
  end

  context "when to not exist target" do
    let(:message_form) {MessageForm::Create.new(Message.new) }
    let(:params) { {title: 'This is a message.', content: 'This is a content.'} }
    it "to a not exist user should fail" do
      params[:username] = 'nobody'
      expect(message_form.create(params)).to be false
      expect(message_form.errors[:username]).to be_present
    end
    it "to a not exist agent should fail" do
      params[:agent_username] = 'nobody'
      expect(message_form.create(params)).to be false
      expect(message_form.errors[:agent_username]).to be_present
    end
    it "no target should fail" do
      expect(message_form.create(params)).to be false
      expect(message_form.errors[:username]).to be_present
    end
  end

  context "when don't have" do
    let(:message_form) {MessageForm::Create.new(Message.new) }
    let(:params) { {username: @user.username} }
    it "title should fail" do
      params[:content] = 'This is a content.'
      expect(message_form.create(params)).to be false
      expect(message_form.errors[:title]).to be_present
    end
    it "content should fail" do
      params[:title] = 'This is a message.'
      expect(message_form.create(params)).to be false
      expect(message_form.errors[:content]).to be_present
    end
    it "title and content should fail" do
      expect(message_form.create(params)).to be false
      expect(message_form.errors[:title]).to be_present
      expect(message_form.errors[:content]).to be_present
    end
  end
end