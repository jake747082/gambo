# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gambo/version'

rails_version = '~> 4.1.7'

Gem::Specification.new do |spec|
  spec.name          = "gambo"
  spec.version       = Gambo::VERSION
  spec.authors       = ["eddie.li"]
  spec.email         = ["eddie@visionbundles.com"]
  spec.summary       = %q{It's a gambling system shared business logic.}
  spec.description   = %q{including shared concerns of agents, slot bet form, users, game setting ... etc }
  spec.homepage      = "http://gitlab.dev.visionbundles.com/gameteam/gambo"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec-rails", '~> 3.5'
  spec.add_development_dependency 'database_cleaner'
  spec.add_development_dependency 'sqlite3'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency "parallel_tests"
  spec.add_runtime_dependency "mysql2", '~> 0.3.18'

  spec.add_dependency "factory_girl_rails"
  spec.add_dependency "faker"
  spec.add_dependency "simple_enum"
  spec.add_dependency 'colorize'
  spec.add_dependency 'pusher', '~> 0.14.2'
  spec.add_dependency 'public_activity', '1.5.0'
  spec.add_dependency 'activerecord', rails_version
  spec.add_dependency "activesupport", rails_version
  spec.add_dependency "rails", rails_version
  spec.add_dependency "devise", '~> 3.4.1'
  spec.add_dependency "awesome_nested_set", '~> 3.0.1'
  spec.add_dependency "settingslogic", '~> 2.0.9'
  spec.add_dependency "reform", '~> 1.1.1'
  spec.add_dependency "virtus", '~> 1.0.3'
  spec.add_dependency 'activerecord-mysql-unsigned', '~> 0.2.0'
  spec.add_dependency 'memoist'
  spec.add_dependency 'probability'
  spec.add_dependency 'rails_autolink', '~> 1.1.6'
  spec.add_dependency 'activerecord-mysql-index-hint', '0.0.2'
  spec.add_dependency 'aasm', '~> 4.9.0'
  spec.add_dependency 'activerecord-import', '~> 1.0.0'
  spec.add_dependency 'savon', '~> 2.11.0'
  spec.add_dependency 'rest-client'
  spec.add_dependency 'digest-sha3', '1.0.2'
  spec.add_dependency 'sms_mitake'
end
