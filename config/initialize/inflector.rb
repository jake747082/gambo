ActiveSupport::Inflector.inflections do |inflect|
  # Fix bonuses plural problem
  inflect.clear :uncountables
  inflect.irregular 'bonus', 'bonuses'
end