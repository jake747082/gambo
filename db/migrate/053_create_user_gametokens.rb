class CreateUserGametokens < ActiveRecord::Migration
  def change
    create_table :user_gametokens, force: true do |t|
      t.references :user, unsigned: true
      t.references :game_platform, unsigned: true, index: true
      t.references :platform_game, unsigned: true

      t.text :extra
      t.string :token

      t.datetime :expired_at
      t.timestamps
    end

    add_index :user_gametokens, [:user_id, :platform_game_id], unique: true
  end
end