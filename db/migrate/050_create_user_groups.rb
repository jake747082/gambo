class CreateUserGroups < ActiveRecord::Migration
  def change
    create_table :user_groups, force: true do |t|
      t.string :name, null: false
      t.timestamps
    end
    add_index :user_groups, :name, unique: true
  end
end
