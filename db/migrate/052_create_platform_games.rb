class CreatePlatformGames < ActiveRecord::Migration
  def change
    create_table :platform_games, force:true do |t|
      t.string :code, null: false
      t.string :game_type, null: false
      t.references :game_platform, index: true, unsigned: true
      t.string :tech
      t.string :plat
      t.string :lang
      t.boolean :status, default: false, null: false
      t.boolean :maintain, default: false, null: false
      t.boolean :enable, default: false, null: false
      t.string :nameset
      t.timestamps
    end
    add_index :platform_games, :code, unique: true
  end
end