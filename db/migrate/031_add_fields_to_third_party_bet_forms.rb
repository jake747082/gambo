class AddFieldsToThirdPartyBetForms < ActiveRecord::Migration
  def change
    # 遊戲類型(1: video, 2: 電子遊戲)
    add_column :third_party_bet_forms, :game_type_id, :integer, default: 1, null: false, unsigned: true, after: :game_name_id
    # AG round 平台内的大厅类型
    add_column :third_party_bet_forms, :round, :text, after: :game_type_id
    # 其他資訊
    # MG: session_id, progressive_wage, iso_code, game_platform
    # AG: remark
    add_column :third_party_bet_forms, :extra, :text, after: :ip
    # 所有回傳資訊
    add_column :third_party_bet_forms, :response, :text, after: :ip
    add_column :third_party_bet_forms, :state, :string, null: true, default: 1
    add_column :third_party_bet_forms, :from, :text, after: :state
    add_column :third_party_bet_forms, :currency, :string, null: true, default: 'CNY'

    # 遊戲類型id
    change_column :third_party_bet_forms, :game_name_id, :string, null: true
    change_column :third_party_bet_forms, :vendor_id, :string, null: true
    change_column :third_party_bet_forms, :table_id, :integer, null: true
    change_column :third_party_bet_forms, :game_kind, :string, null: true

    add_index :third_party_bet_forms, :game_type_id
  end
end
