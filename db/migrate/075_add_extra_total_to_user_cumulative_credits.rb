class AddExtraTotalToUserCumulativeCredits < ActiveRecord::Migration
  def change
    # 贈金總額
    add_column :user_cumulative_credits, :extra_total, :decimal, precision: 19, scale: 8, null: false, default: 0, unsigned: true, after: :total
  end
end