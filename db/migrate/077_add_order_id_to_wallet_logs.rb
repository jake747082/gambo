class AddOrderIdToWalletLogs < ActiveRecord::Migration
  def change
    # 1: 充值; 2: 提幣
    add_column :wallet_logs, :type_cd, :integer, limit: 1, default: 0, unsigned: true, after: :id
    add_column :wallet_logs, :order_id, :string, null: false, unique: true, after: :type_cd
    add_index :wallet_logs, [:order_id, :type_cd]
    remove_index :wallet_logs, :transaction_hash
  end
end
