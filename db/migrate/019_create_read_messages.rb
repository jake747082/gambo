class CreateReadMessages < ActiveRecord::Migration
  def change
    create_table :read_messages, force: true do |t|
      t.integer :message_id, default: 0, null: false, unsigned: true
      t.integer :user_id, default: 0, null: false, unsigned: true
      t.datetime :read_at
      t.timestamps
    end
  end
end
