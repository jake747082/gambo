class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table(:currencies, force: true) do |t|
      t.string :name, limit: 10, null: false, default: '' #名稱
      t.string :blockchain, limit: 10, null: false, default: '' #幣別
      t.decimal :in_rate, precision: 11, scale: 4, default: 0.0, null: false #換算成遊戲幣
      t.decimal :in_extra_percent, precision: 11, scale: 4, default: 0.0, null: false # 贈金送%
      t.decimal :in_limit, precision: 11, scale: 4, default: 0.0, null: false #最小儲值額
      t.decimal :out_limit, precision: 11, scale: 4, default: 0.0, null: false #最小提幣額
      t.boolean :in_enable, default: false, null: false #可儲值使用(0:否, 1:是)
      t.boolean :out_enable, default: false, null: false #可提幣使用(0:否, 1:是)
      t.timestamps
    end

    add_index :currencies, [:name, :blockchain], unique: true
  end
end
