# 用來儲存會員存款/提款/打碼累計總額
class CreateUserCumulativeCredits < ActiveRecord::Migration
  def change
    create_table :user_cumulative_credits, force: true do |t|
      # 類型(deposit: 0, cashout: 1, dc_deposit: 2, dc_cashout: 3)
      t.integer :type_cd, null: false, unsigned: true
      # 玩家
      t.integer :user_id, null: false, unsigned: true
      # 筆數
      t.integer :count, default: 0, null: false, unsigned: true
      # 總額
      t.decimal :total, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.timestamps
    end
    add_index :user_cumulative_credits, :user_id
    add_index :user_cumulative_credits, [:type_cd, :user_id], unique: true
  end
end
