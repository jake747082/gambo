class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages, force: true do |t|
      t.string :page_id, limit: 50, null: false
      t.string :title, limit: 255, null: false, default: ''
      t.text :content
      t.integer :views, default: 0, null: false
      t.timestamps
    end

    add_index :pages, :page_id, unique: true
  end
end
