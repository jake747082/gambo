class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.string :bank_title, limit: 255, default: ''
      t.string :bank_code, limit: 255, default: ''
      t.string :title, limit: 255, default: ''
      t.string :ssn, limit: 255, default: ''
      t.string :mobile, limit: 20, default: ''
      t.string :account, limit: 255, default: ''
      t.string :security_code, limit: 255, default: ''
      t.integer :accountable_id, unsigned: true, null: false
      t.string :accountable_type, null: false
      t.timestamps
    end

    add_index :bank_accounts, :accountable_id
  end
end