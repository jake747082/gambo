class AddRecommendCodeToAgents < ActiveRecord::Migration
  def change
    # 推薦碼
    add_column :agents, :recommend_code, :string, unique: true, limit: 255, after: :commission_ratio
  end
end
