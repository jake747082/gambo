# AG單一錢包尚未完成的注單
class CreateAgSwUndoneBetForms < ActiveRecord::Migration
 def change
    create_table :ag_sw_undone_bet_forms, force: true do |t|

      # 股東贏
      t.integer :shareholder_id, default: 0, null: false, unsigned: true
      t.decimal :shareholder_win_amount, default: 0, null: false, precision: 14, scale: 4
      t.decimal :shareholder_owe_parent, default: 0, null: false, precision: 14, scale: 4

      # 總代贏
      t.integer :director_id, default: 0, null: false, unsigned: true
      t.decimal :director_win_amount, default: 0, null: false, precision: 14, scale: 4
      t.decimal :director_owe_parent, default: 0, null: false, precision: 14, scale: 4

      # 代理贏
      t.integer :agent_id, default: 0, null: false, unsigned: true
      t.decimal :agent_win_amount, default: 0, null: false, precision: 14, scale: 4
      t.decimal :agent_owe_parent, default: 0, null: false, precision: 14, scale: 4

      # 玩家
      t.integer :user_id, null: false, unsigned: true

      # 平台 id
      t.integer :game_platform_id, null: false, unsigned: true

      # 玩家 result
      t.decimal :bet_total_credit, null: false, precision: 14, scale: 4, unsigned: true

      # 結果
      t.decimal :reward_amount, default: 0, null: false, precision: 14, scale: 4
      # 輸贏金額
      t.decimal :user_credit_diff, default: 0, null: false, precision: 14, scale: 4

      ### 東方會

      # 平台紀錄流水號(AG: mainbillno 主订单号)
      t.string :product_id, null: true
      # 遊戲結果id / 場次(AG: 遊戲局號)
      t.string :game_record_id, null: true
      # 單號(平台簡寫_流水注單號)
      t.string :order_number, null: false, unique: true
      # 桌號
      t.integer :table_id, null: true, unsigned: true
      # 局號
      t.integer :stage, null: true, unsigned: true
      # 靴數
      t.integer :inning, null: true, unsigned: true
      # 遊戲id
      t.string :game_name_id, null: true, unsigned: true
      # 遊戲類型(1: video, 2: 電子遊戲)
      t.integer :game_type_id, default: 1, null: false, unsigned: true
      # AG round 平台内的大厅类型
      t.text :round

      # 投注類型(AG: playType)
      t.integer :game_betting_kind, null: true, unsigned: true
      # 投注內容
      t.string :game_betting_content
      # 遊戲結果類型
      t.integer :result_type, null: true, unsigned: true
      # 賠率(AG: odds)
      t.decimal :compensate_rate, default: 0, null: true, precision: 11, scale: 2
      # 餘額
      t.decimal :balance, default: 0, null: true, precision: 14, scale: 4
      t.datetime :start_at
      t.datetime :end_at
      # 投注時間
      t.datetime :betting_at
      # 順序號(AG: YYYYMMDD)
      t.string :vendor_id, null: true, unsigned: true, limit: 255
      # 有效投注
      t.decimal :valid_amount, default: 0, null: false, precision: 14, scale: 4
      # 遊戲類別
      t.string :game_kind, null: true
      # 開牌結果(AG: result)
      t.text :result
      # 結果牌
      t.string :card

      t.string :ip
      t.text :response
      # 其他資訊
      # MG: session_id, progressive_wage, iso_code, game_platform
      # AG: remark
      t.text :extra

      t.timestamps
      t.string :state, null: true, default: 1
      t.text :from
      t.string :currency, null: true, default: 'CNY'
      t.string :xml_file_path, null: true
    end

    add_index :ag_sw_undone_bet_forms, :user_id
    add_index :ag_sw_undone_bet_forms, :betting_at
    add_index :ag_sw_undone_bet_forms, :game_name_id
    add_index :ag_sw_undone_bet_forms, :game_type_id
    add_index :ag_sw_undone_bet_forms, :vendor_id, unique: true
    add_index :ag_sw_undone_bet_forms, :order_number, unique: true
  end
end
