class AddAccountIdAndSecretKeyToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :account_id, :string, unique: true, limit: 255, after: :maintain_code
    add_column :agents, :secret_key, :string, unique: true, limit: 255, after: :account_id
  end
end
