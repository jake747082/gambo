class AddStatusToGamePlatforms < ActiveRecord::Migration
  def change
    # status : 0 => 關閉 ; 1 => 正常 ; 2 => 維護中
    add_column :game_platforms, :status_cd, :integer, default: 1, after: :name
  end
end