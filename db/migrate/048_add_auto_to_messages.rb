class AddAutoToMessages < ActiveRecord::Migration
  def change
    # 是否系統自動發送
    add_column :messages, :auto, :boolean, null: false, default: false, after: :deleted
    add_column :messages, :group_names, :string, after: :auto
  end
end