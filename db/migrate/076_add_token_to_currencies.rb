class AddTokenToCurrencies < ActiveRecord::Migration
  def change
    # 代幣種類(BTC/ETH)
    add_column :currencies, :token_type, :string, limit: 10, null: false, default: '', after: :blockchain #BTC/ETH
    # 代幣名稱
    add_column :currencies, :token_coin, :string, limit: 10, null: false, default: '', after: :token_type #幣別
  end
end