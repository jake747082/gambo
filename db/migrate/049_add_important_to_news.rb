class AddImportantToNews < ActiveRecord::Migration
  def change
    add_column :news, :important, :boolean, null: false, default: false, after: :deleted
  end
end