class CreateUserWallets < ActiveRecord::Migration
  # 會員錢包管理
  def change
    create_table(:user_wallets, force: true) do |t|
      t.references :user, unsigned: true
      t.string :address, limit: 255, default: '', unique: true
      # 錢包廠商 1: ACE
      t.integer :type_cd, default: 1, null: false, unsigned: true
      # 幣值 1: ETH, 2: BTC, 3: USDT_ERC20, 4: USDT_OMNI
      t.integer :currency_cd, default: 0, null: false, unsigned: true
      t.timestamps
    end

    add_index :user_wallets, [:user_id, :type_cd, :currency_cd], unique: true
    add_index :user_wallets, :address
  end
end
