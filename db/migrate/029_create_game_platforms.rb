class CreateGamePlatforms < ActiveRecord::Migration
  def change
    create_table :game_platforms, force: true do |t|
      t.string :name, limit: 255, null: false, default: ''
      t.timestamps
    end
  end
end
