class AddLanguageToMarquee < ActiveRecord::Migration
  def change
    change_table(:marquees, force: true) do |t|
      t.string :language
      t.integer :order, null: false, default: 0
    end
  end
end