class RenameCurrencyCdToCurrencyId < ActiveRecord::Migration
  def change
    remove_index :user_wallets, [:user_id, :type_cd, :currency_cd]
    remove_index :user_cumulative_credits, name: :index_on_type_n_currency_n_user
    remove_index :user_daily_credit_logs, name: :index_on_date_n_type_n_currency_n_user
    rename_column :user_cumulative_credits, :currency_cd, :currency_id
    rename_column :user_daily_credit_logs, :currency_cd, :currency_id
    rename_column :user_wallets, :currency_cd, :currency_id
    rename_column :withdraws, :currency_cd, :currency_id
    add_index :user_wallets, [:user_id, :type_cd, :currency_id], unique: true
    add_index :user_cumulative_credits, [:type_cd, :currency_id, :user_id], unique: true, name: :index_on_type_n_currency_n_user
    add_index :user_daily_credit_logs, [:date, :type_cd, :currency_id, :user_id], unique: true, name: :index_on_date_n_type_n_currency_n_user
  end
end