class DeviseCreateAdmins < ActiveRecord::Migration
  def change
    create_table(:admins, force: true) do |t|
      ## Database authenticatable
      t.string :username,           null: false, default: "" #帳號
      t.string :encrypted_password, null: false, default: "" #加密密碼

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false, unsigned: true #登入次數
      t.datetime :current_sign_in_at #最近登入
      t.datetime :last_sign_in_at #最近登入	
      t.string   :current_sign_in_ip #最近登入IP	
      t.string   :last_sign_in_ip #最近登入IP	

      t.timestamps
    end

    add_column :admins, :role_cd, :integer, default: 0,  null: false, limit: 1 #權限(0:系統管理員, 3:主管, 5:客服)
    add_column :admins, :nickname, :string, default: '', null: false, limit: 32 #名稱
    add_column :admins, :lock,    :boolean, default: false, null: false #啟用是否(0:是,1:否)

    add_index :admins, :username, unique: true
  end
end
