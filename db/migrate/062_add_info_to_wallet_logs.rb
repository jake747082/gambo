class AddInfoToWalletLogs < ActiveRecord::Migration
  def up
    add_column :wallet_logs, :info, :text, after: :withdraw_id
  end
end
