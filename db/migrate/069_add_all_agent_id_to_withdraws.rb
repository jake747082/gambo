class AddAllAgentIdToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :director_id, :integer, unsigned: true, after: :agent_id
    add_column :withdraws, :shareholder_id, :integer, unsigned: true, after: :director_id
  end
end