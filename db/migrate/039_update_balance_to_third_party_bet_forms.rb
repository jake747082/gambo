class UpdateBalanceToThirdPartyBetForms < ActiveRecord::Migration
  def change
    change_column :third_party_bet_forms, :balance, :decimal, default: 0, null: true, precision: 11, scale: 2
  end
end
