class AddJpToPlatformGames < ActiveRecord::Migration
  def change
    change_table(:platform_games, force: true) do |t|
      t.boolean :is_jp,:default => true,:null => false
      t.decimal :jp_rate, precision: 5, scale: 2, default: 0.0
      t.decimal :jp_md, precision: 10, scale: 4, default: 0.0
      t.decimal :game_reset, precision: 15, scale: 4, default: 0.0 #jp重置值
      t.decimal :base_jp, precision: 15, scale: 4, default: 0.0 #jp固定值
      t.decimal :bet_total_credit, precision: 15, scale: 4, default: 0.0 #下注總額度
    end
  end
end