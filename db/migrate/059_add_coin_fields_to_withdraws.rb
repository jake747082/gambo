class AddCoinFieldsToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :from_address, :string, default: "", after: :agent_id
    add_column :withdraws, :to_address, :string, default: "", after: :from_address
    add_column :withdraws, :currency_cd, :integer, null: false, default: 0, unsigned: true, after: :to_address
    # 儲值或提款的虛擬幣額度
    add_column :withdraws, :amount, :decimal, precision: 19, scale: 8, null: false, default: 0, unsigned: true, after: :currency_cd
  end
end