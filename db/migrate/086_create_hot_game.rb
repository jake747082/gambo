class CreateHotGame < ActiveRecord::Migration
  def change
    create_table(:hot_games, force: true) do |t|
      t.references :game1,null: true
      t.references :game2,null: true
      t.references :game3,null: true
      t.references :game4,null: true
      t.references :game5,null: true
      t.references :game6,null: true
      t.references :game7,null: true
      t.references :game8,null: true
      t.references :game9,null: true
      t.references :game10,null: true
      t.timestamps
    end
  end
end