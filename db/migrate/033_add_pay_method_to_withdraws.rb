class AddPayMethodToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :pay_method, :string, null: false, default: '', after: :type_cd
    add_column :withdraws, :bank_card_type, :string, null: false, default: '', after: :pay_method
    add_column :withdraws, :bank_code, :string, null: false, default: '', after: :bank_card_type
    add_column :withdraws, :ip, :string, null: false, default: ''
  end
end
