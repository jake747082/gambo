class CreateSubAccount < ActiveRecord::Migration
  def change
    create_table :agent_sub_accounts, force: true do |t|
      t.integer :agent_id, unsigned: true
      ## Database authenticatable
      t.string :username, null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false, unsigned: true
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.string :nickname, default: '', null: false, limit: 32
      t.boolean :lock, default: false, null: false

      t.boolean :machine_read_permission, default: false, null: false
      t.boolean :machine_write_permission, default: false, null: false
      t.boolean :finances_permission, default: false, null: false
      t.boolean :agent_read_permission, default: false, null: false
      t.boolean :agent_write_permission, default: false, null: false

      t.timestamps
    end

    add_index :agent_sub_accounts, :username, unique: true
  end
end
