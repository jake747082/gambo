class ChangeUserCreditUsedType < ActiveRecord::Migration
  def change
    change_column :users, :credit_used, :decimal, default: 0, null: false, precision: 15, scale: 2
  end
end
