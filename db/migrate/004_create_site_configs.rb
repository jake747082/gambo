class CreateSiteConfigs < ActiveRecord::Migration
  def change
    create_table :site_configs, force: true do |t|
      t.boolean :cash_maintenance,  default: false, null: false
      t.boolean :lobby_maintenance, default: false, null: false
      t.boolean :agent_maintenance, default: false, null: false

      t.string :cash_marquee,  default: '', null: false
      t.string :lobby_marquee, default: '', null: false
      t.string :agent_marquee, default: '', null: false

      t.timestamps
    end
  end
end
