# AG單一錢包 Request Transfer Logs
# 全部欄位參照AG單一錢包API文件 6.0
class CreateAgSwTransferLogs < ActiveRecord::Migration
  def change
    create_table(:ag_sw_transfer_logs, force: true) do |t|
      t.string :transactionType, limit: 10, null: false
      t.string :transactionID, limit: 50, default: '', unique: true
      t.string :billNo, limit: 255, default: ''
      t.string :playtype, limit: 10, null: false
      t.string :finish, limit: 10, default: ''
      t.string :sessionToken, limit: 100, null: false
      t.string :playname, limit: 100, default: ''
      t.string :agentCode, limit: 10, default: ''
      t.string :betTime, limit: 100, default: ''
      t.string :platformType, limit: 10, default: ''
      t.string :round, limit: 10, default: ''
      t.string :gametype, limit: 10, default: ''
      t.string :gameCode, limit: 100, default: ''
      t.string :tableCode, limit: 100, default: ''
      t.string :transactionCode, limit: 10, default: ''
      t.string :ticketStatus, limit: 10, default: ''
      t.string :gameResult, limit: 255, default: ''
      t.string :deviceType, limit: 10, default: ''
      t.string :netAmount, limit: 255, default: ''
      t.string :validBetAmount, limit: 255, default: ''
      t.string :currency, limit: 10, default: ''
      t.string :value, limit: 255, default: ''
      t.string :betResponse, limit: 10, default: ''
      t.string :val, limit: 255, default: ''
      t.string :settletime, limit: 100, default: ''
      t.string :time, limit: 255, default: ''
      t.string :gameId, limit: 10, default: ''
      t.string :roundId, limit: 100, default: ''
      t.string :amount, limit: 255, default: ''
      t.string :remark, limit: 255, default: ''
      t.string :eventID, limit: 10, default: ''
      t.timestamps
    end

    add_index :ag_sw_transfer_logs, :transactionID
  end
end
