class AddXmlFilePathToThirdPartyBetForms < ActiveRecord::Migration
  def change
    add_column :third_party_bet_forms, :xml_file_path, :string, null: true

    change_column :third_party_bet_forms, :table_id, :string, null: true
  end
end
