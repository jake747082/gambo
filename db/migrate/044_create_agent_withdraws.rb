class CreateAgentWithdraws < ActiveRecord::Migration
  def change
    # 買分紀錄
    create_table :agent_withdraws, force: true do |t|
      t.string :order_id, null: false, default: ''
      t.integer :cash_type_cd, unsigned: true, limit: 2
      t.integer :agent_id, default: 0, null: false, unsigned: true
      t.decimal :points, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.text :bank_account_info
      t.text :note
      t.text :admin_note
      t.string :ip,  default: '', null: false

      t.timestamps
    end
  end
end
