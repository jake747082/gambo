class AddCurrencyToUserDailyCreditLogs < ActiveRecord::Migration
  def change
    add_column :user_daily_credit_logs, :currency_cd, :integer, default: 0, unsigned: true, after: :type_cd
    # 虛擬幣總計(顆)
    add_column :user_daily_credit_logs, :dc_total, :decimal, precision: 19, scale: 8, null: false, default: 0, unsigned: true, after: :total
    remove_index :user_daily_credit_logs, [:date, :type_cd, :user_id]
    add_index :user_daily_credit_logs, [:date, :type_cd, :currency_cd, :user_id], unique: true, name: :index_on_date_n_type_n_currency_n_user
  end
end