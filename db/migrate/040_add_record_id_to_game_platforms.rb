class AddRecordIdToGamePlatforms < ActiveRecord::Migration
  def change
    # 卡卡灣 GET_ADJUSTED_RECORD beginId
    add_column :game_platforms, :record_id, :integer, default: 0
  end
end
