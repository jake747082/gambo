class AddCashoutLimitToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cashout_limit, :decimal, default: 0, null: false, precision: 15, scale: 2, unsigned: true, after: :credit_used
    add_column :users, :cashout_limit_at, :datetime, after: :cashout_limit
  end
end
