class AddWithdrawIdToWalletLogs < ActiveRecord::Migration
  def change
    add_column :wallet_logs, :withdraw_id, :integer, unsigned: true, after: :status
    add_index :wallet_logs, :withdraw_id
  end
end
