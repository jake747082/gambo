class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users, force: true) do |t|
      ## Database authenticatable
      t.string :username,           null: false, default: ""
      t.string :encrypted_password, null: false, default: ""
      t.string :nickname,           null: false, default: "", limit: 32
      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false, unsigned: true
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      
      # Nested tree relationship
      t.integer :shareholder_id, default: 0, null: false, unsigned: true
      t.integer :director_id, default: 0, null: false, unsigned: true
      t.integer :agent_id, default: 0, null: false, unsigned: true
      
      # Credit
      t.decimal :credit, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.decimal :credit_used, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.boolean :lock, default: 0, null: false, unsigned: true
      t.timestamps
    end

    add_index :users, :username, unique: true

    add_index :users, :shareholder_id
    add_index :users, :director_id
    add_index :users, :agent_id
  end
end
