# 增加代理欄位(控制是否有會員管理權限)
class AddUserPermissionToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :user_permission, :boolean, default: true, after: :agent_level_cd
  end
end
