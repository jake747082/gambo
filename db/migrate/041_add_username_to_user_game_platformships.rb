class AddUsernameToUserGamePlatformships < ActiveRecord::Migration
  def change
    add_column :user_game_platformships, :username, :string, null: false, default: "", after: :game_platform_id
  end
end
