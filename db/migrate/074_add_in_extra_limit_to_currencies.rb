class AddInExtraLimitToCurrencies < ActiveRecord::Migration
  def change
    add_column :currencies, :in_extra_limit, :decimal, precision: 11, scale: 4, default: 0.0, null: false, after: :in_extra_percent #贈金的最小儲值額
  end
end