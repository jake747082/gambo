class ChangeDecimalToThirdPartyBetForms < ActiveRecord::Migration
  def change
    change_column :third_party_bet_forms, :shareholder_win_amount, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :shareholder_owe_parent, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :director_win_amount, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :director_owe_parent, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :agent_win_amount, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :agent_owe_parent, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :bet_total_credit, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :reward_amount, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :user_credit_diff, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :balance, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :valid_amount, :decimal, default: 0, null: false, precision: 14, scale: 4
    change_column :third_party_bet_forms, :product_id, :string, null: true
  end
end