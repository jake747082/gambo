# 用來儲存代理佣金報表 每日每項目算一筆
class CreateAgentCommissionReports < ActiveRecord::Migration
  def change
    create_table :agent_commission_reports, force: true do |t|
      t.date :date
      # 類型 ex: AG/OG/xxx/彩票/行政費用）
      t.integer :type_cd, null: false, unsigned: true
      # 代理
      t.integer :agent_id, null: false, unsigned: true
      # 筆數
      t.integer :count, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      # 總額
      t.decimal :total, default: 0, null: false, precision: 15, scale: 2
      t.timestamps
    end
    add_index :agent_commission_reports, :agent_id
    add_index :agent_commission_reports, :date
    add_index :agent_commission_reports, [:date, :type_cd, :agent_id], unique: true
  end
end
