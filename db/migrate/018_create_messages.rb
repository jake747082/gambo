class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages, force: true do |t|
      t.string :title, limit: 255, null: false, default: ''
      t.text :content
      # t.boolean :agent_port, default: false, null: false
      # t.boolean :www_port, default: false, null: false
      t.integer :agent_id, unsigned: true
      t.integer :user_id, unsigned: true
      t.boolean :deleted, default: false, null: false
      t.timestamps
    end
  end
end
