class AddMultipleToPlatformGame < ActiveRecord::Migration
  def change
    change_table(:platform_games, force: true) do |t|
      t.integer :multiple, null: false, default: 0 #遊戲倍數
      t.boolean :multiple_display, null: false, default: false
    end
  end
end