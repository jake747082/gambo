class AddVerificationToUsers < ActiveRecord::Migration
  def up
    add_column :users, :verification_code, :string, limit: 8, default: '', after: :user_group_id
    add_column :users, :verification_token, :string, default: '', after: :verification_code
    add_column :users, :verification_at, :datetime, after: :verification_token
    add_column :users, :verification_send_count, :integer, null: false, default: 0, unsigned: true, after: :verification_at
    add_column :users, :verification_send_at, :datetime, after: :verification_send_count
  end
end
