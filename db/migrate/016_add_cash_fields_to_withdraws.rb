class AddCashFieldsToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :cash_type_cd, :integer, unsigned: true, limit: 2, after: :id
    add_column :withdraws, :type_cd, :integer, unsigned: true, limit: 2, after: :cash_type_cd
    add_column :withdraws, :credit_diff, :decimal, default: 0, null: false, precision: 15, scale: 2, unsigned: true, after: :credit
    add_column :withdraws, :credit_actual, :decimal, unsigned: true, default: 0, null: false, precision: 15, scale: 2, after: :credit_diff
    add_column :withdraws, :cashout_limit, :decimal, default: 0, null: false, precision: 15, scale: 2, unsigned: true, after: :credit_actual

    add_index :withdraws, :type_cd
    add_index :withdraws, :cash_type_cd
  end
end
