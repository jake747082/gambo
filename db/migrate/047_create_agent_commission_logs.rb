# 用來儲存代理佣金結算紀錄
class CreateAgentCommissionLogs < ActiveRecord::Migration
  def change
    create_table :agent_commission_logs, force: true do |t|
      # 代理
      t.integer :agent_id, null: false, unsigned: true
      t.date :begin_date
      t.date :end_date
      # 總額
      t.decimal :amount, default: 0, null: false, precision: 15, scale: 2
      t.timestamps
    end
    add_index :agent_commission_logs, :agent_id
  end
end
