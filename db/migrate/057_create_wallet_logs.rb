class CreateWalletLogs < ActiveRecord::Migration
  # 錢包通知log
  def change
    create_table(:wallet_logs, force: true) do |t|
      t.string :transaction_hash, limit: 255, default: ''
      t.string :from_address, limit: 255, default: ''
      t.string :to_address, limit: 255, default: ''
      t.string :amount, limit: 255, default: ''
      t.string :coin_decimals, limit: 255, default: ''
      t.string :coin_abbr, limit: 255, default: ''
      t.string :status, limit: 255, default: ''
      t.timestamps
    end

    add_index :wallet_logs, :to_address
    add_index :wallet_logs, :transaction_hash, unique: true
  end
end
