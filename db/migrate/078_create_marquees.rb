class CreateMarquees < ActiveRecord::Migration
  def change
    create_table(:marquees, force: true) do |t|
      t.string :title, null: false, default: ''
      t.text :content, null: false
      #顯示
      t.boolean :active, null: false, default: true
      
      # 時間型
      t.boolean :by_time , default: false, null: false
      t.datetime :produced_datetime
      t.datetime :expire_datetime
      # 唯一顯示
      t.boolean :only, default: false

      #每個禮拜的固定時段
      t.boolean :eveny_week, default: false, null: false
      t.string :days
      t.time :begin_time
      t.time :stop_time

      t.timestamps
    end

  end
end